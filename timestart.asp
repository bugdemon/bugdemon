<%@ Language=VBScript %>
<!-- #include file="include.asp" -->

<%
'********************************************************************************************
'*   Copyright 2003-2008, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

%>
   <!-- #include file="inc_checklogin.asp" -->
<%
id = Request.Item("id")
uid = Request.Cookies("UID")
timestamp = now()

if dbType="MySQL" then
   timestamp = MySQLDate(timestamp)
end if

sql = "INSERT INTO timetracking(UserID,TaskID,BeginTime)VALUES(" & uid & "," & id & "," & delim & timestamp & delim & ")"

set oConn = Server.CreateObject("ADODB.Connection")
oConn.Open strConnect

oConn.Execute(sql)

oConn.Close

Response.Redirect("newrequest.asp?id=" & id & "&action=1")
%>