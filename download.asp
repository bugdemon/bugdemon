<%@ Language=VBScript %>
<!-- #include file="include.asp" -->

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/header.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/header.html" -->
<% end if %>

<div style="padding:10px;">
    <h1>Downloads</h1>
    BugDemon was Open Sourced in 2012 and as such is available via Bit Bucket.<p>
    <h3>Download Classic ASP</h3>
    We are currently at version 2.60. &nbsp;&nbsp; <a href="https://bitbucket.org/bugdemon/bugdemon/overview">Download Now</a>
    <p>
    <h3>Download ASP.NET</h3>
    <i>Coming Soon</i>
    <p>
    <h3>Download PHP</h3>
    <i>Coming Sooner</i>
    <p>
    All versions are compatible with the following databases:<br />
    Microsoft SQL Server v7.0+<br />
    MySQL v5.0+<br />
    <u>Note: The Classic ASP version also supports Microsoft Access</u>
    
</div>

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/footer.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/footer.html" -->
<% end if %>