<%@ Language=VBScript %>
<!-- #include file="include.asp" -->

<%
'********************************************************************************************
'*   Copyright 2003-2008, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

%>
   <!-- #include file="inc_checklogin.asp" -->
<%
id = Request.Item("id")
uid = Request.Cookies("UID")
timestamp = now()

if dbType="MySQL" then
   timestamp = MySQLDate(timestamp)
end if

sql = "SELECT * FROM timetracking WHERE UserID=" & uid & " AND TaskID=" & id & " AND EndTime IS NULL"

set oConn = Server.CreateObject("ADODB.Connection")
set oRS = Server.CreateObject("ADODB.Recordset")
oConn.Open strConnect
oRS.Open sql,oConn,1,1

if oRS.EOF then
   Response.Write("You do not have any open time transactions for this Task")
   oRS.Close
   set oRS = nothing
   oConn.Close
   set oConn = nothing
   Response.End   
else
   begin = oRS("BeginTime")
   endtime = now()
   diff = datediff("n",begin,endtime)
   if dbType="MySQL" then
      endtime = MySQLDate(endtime)
   end if
   sql = "UPDATE timetracking SET EndTime= " & delim & endtime & delim & ", TotalMins=" & diff & " WHERE UserID=" & uid & " AND TaskID=" & id & " AND EndTime IS NULL"
   oConn.Execute(sql)
end if

oRS.Close
set oRS = nothing
oConn.Close
set oConn = nothing

Response.Redirect("newrequest.asp?id=" & id & "&action=2")
%>