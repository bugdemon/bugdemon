<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003 - Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one licese per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
userid = Request.Cookies("UserID")
if userid = "" then
   Response.Write("<font face=arial>This is only available after logging in.<p>")
   Response.Write("<a href=logon.asp?pagename=workinfo.asp>Log In</a> | <a href=adduser.asp>Create User ID</a></font>")
   Response.End
end if

workdesc = Request.Item("workdesc")
workdesc = replace(workdesc,"'", "''")
workdesc = replace(workdesc,chr(10), "<br>")

product = Request.Item("product")
product = replace(product, "'", "''")

version = Request.Item("version")
version = replace(version,"'", "''")

duration = replace(Request.Item("duration"),"'","")

if dbType = "MySQL" then
   lastupdated = MySQLDate(now())
else
   lastupdated = now()
end if

set oConn = server.CreateObject("ADODB.Connection")

oConn.Open strConnect

sqlStr = "INSERT INTO workinfo VALUES(" & delim & lastupdated & delim & ",'" & userid & "','" & version & "','" & workdesc & "','" & product & "'," & duration & ")"

oConn.Execute(sqlStr)

Response.Redirect("rep_work.asp")
%>