<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2014, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"
%>
<!-- #include file="inc_checklogin.asp" -->
<%

set oConn = Server.CreateObject("ADODB.Connection")
set oRS = Server.CreateObject("ADODB.Recordset")
oConn.Open strConnect

sql = "SELECT * FROM users WHERE ID=" & Request.Cookies("UID")
oRS.Open sql, oConn,1,1

if oRS.EOF then
   oRS.Close
   set oRS = nothing
   oConn.Close
   set oConn = nothing
   Response.Write("The specified user does not exist.")
   Response.End
end if

s = Request.QueryString("s")
pagetitle = "User Options"
%>

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/header.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/header.html" -->
<% end if %>

<script type="text/javascript">
   function Submit() {
      frm = document.form1;
      if(frm.firstname.value == '') {
         alert('Your first name may not be blank.');
         return false;
      }
      if(frm.lastname.value == '') {
         alert('Your last name may not be blank.');
         return false;
      }
      if(frm.email.value == '') {
         alert('Your email address may not be blank.');
         return false;
      }
      if(frm.password.value == '') {
         alert('You must specify your password in order to make changes to your user record.');
         return false;
      }
      if(frm.newpass.value != '' || frm.repnew.value != '') {
         if(frm.newpass.value != frm.repnew.value) {
            alert('Your new passwords do not match.');
            return false;
         }
      }
      frm.submit();
   }
</script>
<% if s = "1" then %>
<div style="color:red;padding-top:15px;">** User data saved successfully **</div>
<% end if %>
<div name="main" style="padding-top:15px;padding-left:10px;">
   <form name="form1" action="optionsave.asp" method="POST">
   <table border="0" cellpadding="0" cellspacing="0" class="reptab">
      <tr><th class="reptab">User ID</td><td class="reptab"><b><%=oRS("ID")%></b></td><tr>
      <tr><th class="reptab">User Type</td><td class="reptab"><b><%=oRS("UserType")%></b></td><tr>
      <tr><th class="reptab">First Name</td><td class="reptab"><input type="text" size="35" style="font-size:11px;" value="<%=oRS("FirstName")%>" name="firstname"></td><tr>
      <tr><th class="reptab">Last Name</td><td class="reptab"><input type="text" size="35" style="font-size:11px;" value="<%=oRS("LastName")%>" name="lastname"></td><tr>
      <tr><td colspan="2">&nbsp;</td></tr>
      <tr><th class="reptab">Email Address</td><td class="reptab"><input type="text" size="35" style="font-size:11px;" value="<%=oRS("UserID")%>" name="email"></td><tr>
      <tr><th class="reptab">Password</td><td class="reptab"><input type="password" size="35" style="font-size:11px;" value="<%=oRS("UserPass")%>" name="password"></td><tr>
      <tr><th class="reptab">New Password</td><td class="reptab"><input type="password" size="35" style="font-size:11px;" value="" name="newpass"></td><tr>
      <tr><th class="reptab">Repeat New Pass</td><td class="reptab"><input type="password" size="35" style="font-size:11px;" value="" name="repnew"></td><tr>
      <tr><td colspan="2">&nbsp;</td></tr>
      <tr><th class="reptab">Start Page</th>
          <td class="reptab"><select name="startpage">
             <option value="trackmain.asp" <%if oRS("StartPage") = "trackmain.asp" then%>selected<%end if%>>All Tasks</option>
             <% if oRS("UserType") <> defUType then %>
                <option value="rep_developer.asp" <%if oRS("StartPage") = "rep_developer.asp" then%>selected<%end if%>>My Assignments</option>
             <% end if %>
             <option value="watches.asp" <%if oRS("StartPage") = "watches.asp" then%>selected<%end if%>>My Watches</option>
             </select>
          </td>
      </tr>
   </table>
   <p>
   <input type="button" onClick="Submit();" class="butt" value="Save Changes">
   </form>
</div>

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/footer.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/footer.html" -->
<% end if %>
<%
oRS.Close
set oRS = nothing
oConn.Close
set oConn = nothing
%>