<%
'********************************************************************************************
'*   Copyright 2004-2014 - Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************

'Test to see if the field descriptions have already been loaded.
arryFields = Session("Fields")

if not isarray(arryFields) then
   sqlStr = "SELECT * FROM fieldnames ORDER BY FieldNo"
   set xConn = Server.CreateObject("ADODB.Connection")
   xConn.Open strConnect

   set oRS = xConn.Execute(sqlStr)
   
   if oRS.EOF then
      'Assign the default values
      fields = "Status^Change Type^Product^Version^Summary^Description^Comments^Assigned To^Weight Factor^Time^Completed By^Fix Release^Internal Comments^Affected Modules^Admin Comments^Due Date^Priority"
   else
      fields = oRS("FieldDesc")
      oRS.MoveNext
   
      while not oRS.EOF
         fields = fields & "^" & oRS("FieldDesc")
         oRS.MoveNext
      wend
   end if
   
   arryFields = split(fields,"^")
   
   Session("Fields") = arryFields
   
   oRS.Close
   set oRS = nothing
   xConn.Close
   set xConn = nothing
end if

%>