<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2014, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

id = Request.Item("id")
auth = Request.Cookies("Auth")
%>
<!-- #include file="inc_checklogin.asp" -->
<!-- #include file="inc_FieldDef.asp"-->
<!-- #include file="include/inc_messages.asp"-->
<%

description = Request.Item("description")
summary = Request.Item("summary")
status = Request.Item("status")
duedate = Request.Item("duedate")
chgtype = Request.Item("chgtype")
product = Request.Item("product")
version = Request.Item("version")
saveonly = request.item("saveonly")
parentid = request.item("parentid")
priority = cint(Request.Item("priority"))

if description = "" or summary = "" or product = "" then
   Response.Write("You did not supply the required information.<p>")
   Response.Write("Description: " & description & "<br>")
   Response.Write("Summary: " & summary & "<br>")
   Response.Write("Product: " & product & "<br>")
   Response.End
end if

if saveonly = "" then
   saveonly = 0
else
   saveonly = 1
end if

set oConn = Server.CreateObject("ADODB.Connection")
set RS = server.CreateObject("ADODB.Recordset")
oConn.Open strConnect

'set RS = oConn.Execute("SELECT * FROM TObject WHERE ID=" & id)

'Response.Write(RS("Comments"))
'Response.End

lastupdatedby = Request.Cookies("UserID")
if lastupdatedby = "" then
    response.write("Your session expired during data entry. Sorry about that, couldn't be helped.")
    response.end
end if

if dbType = "MySQL" then
   lastupdated = MySQLDate(now())
else
   lastupdated = now()
end if

'***** The request is new *******
if id = "" then
    if duedate = "" then
        if dbType = "MySQL" then
            duedate = MySQLDate(DateAdd("d", 15, Now()))
        else
            duedate = DateAdd("d", 15, Now())
        end if
    end if
    
    if parentid = "" then
        parentid = 0
    else 'test to make sure the parent isn't already a child.  Maybe some day, but can't allow it yet
        set oRS = Server.CreateObject("ADODB.Recordset")
        sqlStr = "SELECT ParentID FROM tobject WHERE ID=" & parentid
        oRS.Open sqlStr, oConn, 1, 1
        if clng(oRS("ParentID")) <> clng(parentid) then
            Response.Write("You are attempting to create a sub/child task for a sub/child task.  This is not currently supported.")
            oRS.Close
            set oRS = Nothing
            Response.End
        end if
        oRS.Close
        set oRS = Nothing
    end if

   sqlStr = "INSERT INTO tobject(Summary, Description, DateEntered, Status, ChangeType, Product, EnteredBy, LastUpdated, LastUpdatedBy, Version, DueDate, ParentID, Priority) VALUES ('" & replace(summary,"'","''") & "','" & replace(description,"'","''") & "'," & delim & lastupdated & delim & ",'New','" & chgtype & "','" & product & "','" & lastupdatedby & "'," & delim & lastupdated & delim & ",'" & lastupdatedby & "','" & version & "'," & delim & duedate & delim & "," & parentid & "," & priority & ")"
   oConn.Execute(sqlStr)
   
   'sqlStr = "SELECT ID FROM tobject WHERE LastUpdated=" & delim & lastupdated & delim & " AND Summary='" & summary & "'"
   sqlStr = "SELECT @@IDENTITY AS ID FROM tobject"
   set RS = oConn.Execute(sqlStr)
   if parentid = 0 then
        'if the parent ID was not passed in, assign itself as the parent
        sqlStr = "UPDATE tobject SET ParentID=" & RS("ID") & " WHERE ID=" & RS("ID")
        oConn.Execute(sqlStr)
   end if
   
   if Request.Item("notify") = "ON" then
      if not Request.Cookies("UserID") = "Guest" then
         sqlStr = "INSERT INTO chgnotify(ID, UserID, DateAdded) VALUES(" & RS("ID") & ",'" & Request.Cookies("UserID") & "'," & delim & lastupdated & delim & ")"
         oConn.Execute(sqlStr)
         call writeLog(RS("ID"), 6,"Object Originator added to Watch List")
      end if
   end if
   
   call writeLog(RS("ID"), 3,"Added new object")
   
   'notify the admin of new request
   call sendMail(adminNotify, "A new request has been entered at the " & coName & " " & appName & ":<p>" & summary & "<p>" & description & "<p>" & "You can view the request by clicking here: <a href=" & chr(34) & siteAddy & "/newrequest.asp?id=" & RS("ID") & chr(34) & ">View Request</a>", coName & " " & appName & ":New Entry:" & RS("ID"))
   
   'send notification to the submitter
   call sendMail(request.cookies("userid"), "Your request (" & summary & ") has been entered at the " & coName & " " & appName & ":<p>" & description & "<p>" & "You can view the request by clicking here: <a href=" & chr(34) & siteAddy & "/newrequest.asp?id=" & RS("ID") & chr(34) & ">View Request</a>", coName & " " & appName & ":New Entry:" & RS("ID"))
   
   'insert original requestor into the watches if necessary
   if parentid <> 0 then
        set oRS = Server.CreateObject("ADODB.Recordset")
        sqlStr = "SELECT EnteredBy, Summary FROM tobject WHERE ID=" & parentid
        oRS.Open sqlStr, oConn, 1, 1
        original_requester = oRS("EnteredBy")
        if(original_requester <> lastupdatedby) then
            sqlStr = "INSERT INTO chgnotify(ID, UserID, DateAdded) VALUES(" & RS("ID") & ",'" & original_requester & "'," & delim & lastupdated & delim & ")"
            oConn.Execute(sqlStr)
            call writeLog(RS("ID"), 6,"Parent Originator " & original_requester & " added to Watch List")
            call sendMail(original_requester, "A new child/sub task for your request (" & oRS("Summary") & ") has been entered at the " & coName & " " & appName & ":<p><strong>Title:</strong> " & summary & "<p>" & "You can view the request by clicking here: <a href=" & chr(34) & siteAddy & "/newrequest.asp?id=" & RS("ID") & chr(34) & ">View Request</a>", coName & " " & appName & ":Sub Task Entry:" & RS("ID"))
        end if
        oRS.Close
        set oRS = nothing
   end if
   
   set RS = nothing
   oConn.Close
   set oConn = nothing
   Response.Redirect("trackmain.asp")
else 'it is an update
    if dbType = "MySQL" then
        duedate = MySQLDate(duedate)
    end if
    'Get priority descriptions
    dim arryPriority(100)
    sqlStr = "SELECT prioritydesc,priorityid FROM priorities ORDER BY escalationpath"
    RS.Open sqlStr,oConn,1,1
    while not RS.EOF
        arryPriority(RS("priorityid")) = RS("prioritydesc")
        RS.MoveNext
    wend
    RS.Close
    
   sqlStr = "SELECT * FROM tobject WHERE ID=" & id
      
   'set RS = oConn.Execute("SELECT * FROM tobject WHERE ID=" & id)
   RS.Open sqlStr,oConn,1,1
   if RS.EOF then
        Response.Write("The specified task could not be found.")
        Response.End
   end if
   
   existcomments =  RS("Comments")
   'Response.Write(existcomments)

   dateclosed = Request.Item("dateclosed")
   
   comments = Request.Item("admincomments")
   comments = replace(comments,"'","''")
   
   assignedto = Request.Item("assignedto")
   
   internalcomments = Request.Item("intcomments")
   internalcomments = replace(internalcomments,"'","''")
   
   if RS("InternalComments") <> "" then
      internalcomments = internalcomments & "<p>" & replace(RS("InternalComments"),"'","''")
   end if
   
   'check to see if the status is allowed to be changed.
   if auth = "Standard" then
      status = RS("Status")
   end if
   
   'check to see if this is a parent task or child, make sure any status change is allowed
   'response.write(RS("ID") & " " & RS("ParentID") & "<P>")
   if clng(RS("ID")) = clng(RS("ParentID")) then
        'if trying to close or complete, need to see if it has children
        if status = "Complete" or status = "Closed" then
            'make sure children are closed as well, if one is not, we cannot allow parent to be closed
            set oRS = Server.CreateObject("ADODB.Recordset")
            sqlStr = "SELECT ID,Status FROM tobject WHERE ParentID=" & RS("ID") & " AND ID <> " & RS("ID")
            'response.write(sqlStr & "<p>")
            oRS.Open sqlStr,oConn,1,1
            
            if oRS.EOF <> true then
                arryRows = oRS.GetRows()
                oRS.MoveFirst
            
                'response.write("recordcount: " & ubound(arryRows,2) + 1 & "<P>")
                    
                if ubound(arryRows,2) >= 0 then
                    'has child tasks
                    while oRS.EOF <> true
                        if oRS("Status") <> "Complete" and oRS("Status") <> "Closed" then
                            Response.Write("One or more sub/child tasks are still open. The item " & chr(34) & RS("Summary") & chr(34) &  " cannot be closed at this time.<br />")
                            Response.End
                        end if
                        oRS.MoveNext
                    wend
                end if
            end if	
        end if       
    else
        '***** TODO: make sure parent task set to in process if child is...
    
   end if
      
   weightingfactor = Request.Item("weightfactor")
   if weightingfactor = "" then
      weightingfactor = 0
   end if
   if dbType = "Access" then
      changeduration = ccur(Request.Item("changeduration"))
   end if
   if dbType = "MSSQL" or dbType = "MySQL" then
      changeduration = Request.Item("changeduration")
      if changeduration = "" then
         changeduration = 0
      end if
      if not isnumeric(changeduration) then
         Response.Write(arryFields(9) & " must be a numeric value.")
         Response.End
      end if
   end if
      
   completedby = Request.Item("completedby")
   files = Request.Item("files")
   fixrelease = Request.Item("fixrelease")
   
   'Check dependencies
   'TODO:  change the description of the fields to use arryFields
   if status = "Assigned" and assignedto = "" then
      Response.Write("You didn't specify the Assigned To resource.")
      Response.End 
   end if
   
   if status = "Closed" and comments = "" then
      Response.Write("Status, Closed, requires you to enter public comments.")
      Response.End
   end if
   
   if status = "Complete" and (changeduration = 0 or completedby = "") then
      Response.Write("You must specify the Change Duration and Completed By values.")
      Response.End
   end if
   
   if internalcomments <> "" then
      internalcomments = "<font color=blue>" & now() & " - " & Request.Cookies("First") & " " & Request.Cookies("Last") & "</font><br>" & internalcomments & "<p>"
   end if
   
   sqlStr = "UPDATE tobject SET Summary='" & replace(summary,"'","''") & "', Description='" & replace(description,"'","''") & "', Status='" & status & "', ChangeType='" & chgtype & "', Version='" & version & "', Product='" & product & "', Comments='" & comments & "', AssignedTo='" & assignedto & "', InternalComments='" & internalcomments & "', WeightingFactor=" & weightingfactor & ", ChangeDuration=" & changeduration & ", CompletedBy='" & completedby & "', FixRelease='" & fixrelease & "', LastUpdatedBy='" & Request.Cookies("UserID") & "', LastUpdated=" & delim & lastupdated & delim & ", DueDate=" & delim & duedate & delim & ", Priority=" & priority
   
   if comments <> existcomments then
      if comments <> "" then     
         sqlStr = sqlStr & ", Commented='<font color=blue>" & now() & " - " & Request.Cookies("First") & " " & Request.Cookies("Last") & "</font><br>'"
      else
         sqlStr = sqlStr & ", Commented=NULL"
      end if
   else
      if comments = "" then
         sqlStr = sqlStr & ", Commented=NULL"
      end if
   end if
   if files <> "" then
      sqlStr = sqlStr & ", AffFiles='" & files & "'"
   end if
   sqlStr = sqlStr & " WHERE ID=" & id
   
   'Response.Write(sqlStr)
   oConn.Execute(sqlStr)
   call writeLog(RS("ID"), 2,"Updated: " & Replace(sqlStr,"'","''"))
   
   'Update the WorkInfo
   if status = "Complete" then 'and RS("Status") <> "Complete" then 'Make sure we only enter it once in case of subsequent saves.
      sqlStr = "INSERT INTO workinfo (EnterDate, EnteredBy, Version, DevDesc, Product, TimeSpent) VALUES(" & delim & lastupdated & delim & ",'" & completedby & "','" & fixrelease & "','Completed Request <a href=newrequest.asp?id=" & id & ">" & id & "</a><br>&nbsp;&nbsp;" & replace(summary,"'","''") & "','" & product & "','" & changeduration & "')"
      oConn.Execute(sqlStr)
   end if
      
   'Send Mail notifications?  Proper status, or priority change.
   if (status = "More Info" or status = "Assigned" or status = "In Process" or status = "Duplicate" or status = "Complete" or status = "Closed") or (cint(RS("Priority")) <> priority ) and Request.Item("sendnotifications") = "ON" then
      
      RS.Close
      
      sqlStr = "SELECT * FROM chgnotify WHERE ID=" & id
      'set RS = oConn.Execute(sqlStr)
      RS.Open sqlStr,oConn,1,1
      
      while RS.EOF <> true
         call sendMail(RS("UserID"), "There has been a status change on an entry you wanted to watch at " & coName & " " & appName & ":<br /><strong>Title</strong>: " & summary & "<br /><strong>Current Status</strong>: " & status & "<br /><strong>Priority</strong>: " & arryPriority(priority) & "<br /><strong>Tentative Due Date</strong>: " & duedate & "<p>You can view the full request at: <a href=" & chr(34) & siteAddy & "/newrequest.asp?id=" & id & chr(34) & ">Click Here</a>", "Change Notification for " & id)
         RS.MoveNext
      wend
      
      RS.Close
      
      '** If status is assigned, notify the developer
      if status = "Assigned" then
         'check to see if the developer has already been notified of the assignment
         sql = "SELECT * FROM chgnotify WHERE UserID='" & assignedto & "' and ID=" & id
         RS.Open sql,oConn,1,1
         
         sendNotify = false
         
         if RS.EOF then
            'the developer has not been notified AND is not watching this change
            
            'add the developer to the watchers table
            sql = "INSERT INTO chgnotify (ID,UserID,DateAdded,DevNotify) VALUES (" & id & ",'" & assignedto & "'," & delim & lastupdated & delim & ",1)"
            oConn.Execute(sql)
            
            sendNotify = true
         else
            'the developer is watching, check to see if they've already been notified of the assignment
            if RS("DevNotify") = "0" then 'watching, but not yet notified of the assignment
               
               
               'update watch record
               sql = "UPDATE chgnotify SET DevNotify=1 WHERE UserID='" & assignedto & "' AND ID=" & id
               oConn.Execute(sql)
               
               sendNotify = true
            end if
         end if
         if sendNotify = true then
            call sendMail(assignedto,"You have been assigned the following:<p><i>" & summary & "</i><p>" & description & "<p>To view the task, <a href=" & chr(34) & siteAddy & "/newrequest.asp?id=" & id & chr(34) & ">Click Here</a>",coName & " " & appName & ":New Assignment:" & id)
         end if
         
      end if  
   end if
   
   'RS.Close
   set RS = nothing
   oConn.Close
   set oConn = nothing
   
   if saveonly = 0 then
      Response.Redirect("trackmain.asp")
   else
      Response.Redirect("newrequest.asp?id=" & id & "&action=3")
   end if
end if
 

%>
