<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2014, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

%>
<!-- #include file="inc_checklogin.asp" -->
<%
id = Request.Item("id")
if id = "" then
   Response.Write("You did not specify the ID number.")
   Response.End
end if


set oConn = Server.CreateObject("ADODB.Connection")
oConn.Open strConnect

if Request.item("stateflag") = "1" then
   comments = Request.Item("comment")
   comments = replace(comments,"'","''")
   if dbType = "MySQL" then
      timestamp = MySQLDate(now())
   else
      timestamp = now()
   end if
   sqlStr = "INSERT INTO addcomments VALUES(" & id & ",'" & Request.Cookies("UserID") & "','" & comments & "'," & delim & timestamp & delim & ")"
   
   oConn.Execute(sqlStr)
   call writeLog(id, 4,"Added Std Comment")
   
   if Request.Item("notify") = "ON" then
      on error resume next
      sqlStr = "INSERT INTO chgnotify(ID,UserID,DateAdded)Values(" & id & ",'" & Request.Cookies("UserID") & "'," & delim & timestamp & delim & ")"
      oConn.Execute(sqlStr)
      call writeLog(id, 6,"Added in Note")
   else
      sqlStr = "DELETE FROM chgnotify WHERE ID=" & id & " AND UserID='" & Request.Cookies("UserID") & "'"
      oConn.Execute(sqlStr)
      call writeLog(id, 6,"Deleted in Note")
   end if
   
   'send e-mail notifications.
   sqlStr = "SELECT * FROM chgnotify WHERE ID=" & id
   set RS = oConn.Execute(sqlStr)
   
   while RS.EOF <> true
      call sendMail(RS("UserID"), "There has been a change on a bug/enhancement request you wanted to watch at " & coName & ".  You can view the request at: <a href=" & siteAddy & "/newrequest.asp?id=" & id & ">" & siteAddy & "/newrequest.asp?id=" & id, "Change Notification for " & id)
      RS.MoveNext
   wend
   
   set RS = nothing
   oConn.Close
   set oConn = nothing
   'Response.Redirect("newrequest.asp?id=" & id)
   response.write("<script>window.close();</script>")
else
   sqlStr = "SELECT Summary FROM tobject WHERE ID=" & id
   
   set oRS = oConn.Execute(sqlStr)
   
   if not oRS.EOF then
      reqtitle = "<font color=blue>" & chr(34) & "</font>" & oRS("Summary") & "<font color=blue>" & chr(34) & "</font>"
   end if
   
   sqlStr = "SELECT DateAdded,UserID,Comments as C FROM addcomments WHERE ID=" & id & " ORDER BY DateAdded DESC"
   set oRS = oConn.Execute(sqlStr)
   
   while oRS.EOF <> true
      comments = comments & "<font color=LightSteelBlue>" & oRS("DateAdded") & " - " & oRS("UserID") & "</font><br>" & oRS("C") & "<p>" 
      oRS.MoveNext
   wend
   
   oConn.Close
   set oConn = nothing
end if
pagetitle = "Add Comment"
%>

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/header.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/header.html" -->
<% end if %>

<script language="JavaScript">
   function submitform() {
      if (document.addcomm.comment.value == "") {
         alert("You did not specify a comment.");
         return 1;
      }
      document.addcomm.submit();
   }
</script>
<body onunload="window.opener.document.location.reload();">
<div id="container" style="width:100%;">
    <div id="content" style="margin:0 auto;display:table;">
    <p>
    <form action=addcomment.asp method=POST name=addcomm>
    <table border=0 cellpadding=0 cellspacing=0 width=500>
        <tr><td colspan=2><hr style="color:lightsteelblue;"></td></tr>
        <tr><td><b><%=reqtitle%></b></td></tr>
        <tr><td colspan=2>&nbsp;</td></tr>
        <tr><td><textarea cols=60 rows=4 style="font-family:arial;" name=comment></textarea></td></tr>
        <tr><td align=left><input type="checkbox" name="notify" value = "ON" checked> <small>Notify me of changes to this request.</small></td></tr>
        <tr><td colspan=2>&nbsp;</td></tr>
        <tr><td align=right><input type="button" onclick="submitform();" value="Add Comment" class="butt"></td></tr>
        <tr><td colspan=2><hr style="color:lightsteelblue;"></td></tr>
        <tr><td><%=comments%></td></tr>
    </table>
    <input type=hidden name=id value="<%=id%>">
    <input type=hidden name=stateflag value="1">
    </form>

</div>
</div>
<%if theme = "2003" then %>
    <!-- #include file="themes/2003/footer.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/footer.html" -->
<% end if %>
