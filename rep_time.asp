<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<!-- #include file="inc_FieldDef.asp" -->

<!-- #include file="inc_checklogin.asp" -->
<%
if Request.Cookies("Auth") = defUType or Request.Cookies("Auth") = "" then
   Response.Write("This section is reserved for developers and administrators.")
   Response.End
end if

exec = Request.Item("exec")

sql = "SELECT * FROM users WHERE UserType='Owner' OR UserType='Developer'"
set oConn = Server.CreateObject("ADODB.Connection")
set oRS2 = Server.CreateObject("ADODB.Recordset")
oConn.Open strConnect
oRS2.Open sql,oConn,1,1

'This is used only for the results, not the initial screen.
'Not sure why oRS2.RecordCount doesn't work, so we worked around it - lazy :)
numusers = 0
while not oRS2.EOF
   numusers = numusers + 1
   oRS2.MoveNext
wend
oRS2.MoveFirst

redim arryUsers(numusers,5)

if exec = "1" then
   'get the post values
   begin = Request.Item("year") & "-" & Request.Item("month") & "-" & Request.Item("day") & " " & Request.Item("hour") & ":" & Request.Item("min")
   enddate = Request.Item("eyear") & "-" & Request.Item("emonth") & "-" & Request.Item("eday") & " " & Request.Item("ehour") & ":" & Request.Item("emin")
   developer = Request.Item("developer")
   if developer = "" then
      Response.Write("Error")
      Response.End
   end if
   
   'pull the data
   sql = "SELECT * from timetracking WHERE BeginTime BETWEEN '" & begin & ":00' AND '" & enddate & ":59'"
   if developer <> "all" then
      sql = sql & " AND UserID=" & developer
   end if
   sql = sql & " ORDER BY UserID,TaskID,BeginTime"

   set oConn = Server.CreateObject("ADODB.Connection")
   set oRS = Server.CreateObject("ADODB.Recordset")

   oConn.Open strConnect
   oRS.Open sql,oConn,1,1
   if oRS.EOF then
      Response.Write("There are no records at this time for the selected criteria.")
      Response.End
   end if
   'populate the Users Array - this reduces the number of DB hits later
   x = 0
   while not oRS2.EOF
      arryUsers(x,0) = oRS2("ID")
      arryUsers(x,1) = oRS2("UserID")
      arryUsers(x,2) = oRS2("FirstName")
      arryUsers(x,3) = oRS2("LastName")
      arryUsers(x,4) = oRS2("UserType")
      x = x + 1
      oRS2.MoveNext
   wend
end if


%>  
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="demon.css">
</HEAD>
<BODY>
<table border="0">
   <tr bgcolor=LightSteelBlue valign=middle><th valign=middle>&nbsp;<%=coName%>&nbsp;<%=appName%> &nbsp;&nbsp;</th><th valign=middle>&nbsp;&nbsp;&nbsp;&nbsp;</th><th valign=middle>&nbsp;&nbsp;&nbsp;</th><th valign=middle>&nbsp;&nbsp;</th><th valign=middle>&nbsp;</th><th width=500 align=right bgcolor=white valign=middle><span style="font-size:8px;">Powered By:&nbsp;</span><img src=logo.gif align=absmiddle border=0></th></tr>
</table>
<!-- #include file="inc_nav.asp" -->
<br>
<% if (Request.Cookies("Auth") = "Owner" or Request.Cookies("Auth") = "Developer") and modText <> "" then %>
   <div style="padding-bottom:10px;padding-top:10px;padding-left:5px;font-size:12px;width:900px;border:1px solid red;"><img src="images/exclamation.png" border="0" align="absmiddle"><%=modText%></div>
<% end if %>

<div style="padding-top:10px;">
<% if exec = "" then %>
<span style="font-size:13px;font-weight:900;">Report Selection Criteria</span>
<form action="rep_time.asp" method="POST" name="form1">
<table border="0" cellpadding="0" cellpadding="0">
   <tr><th class="reptab">User: </th><td class="reptab"><select name="developer">
   <%
   while not oRS2.EOF
      Response.Write ("<option value=" & chr(34) & oRS2("ID") & chr(34) & ">" & oRS2("UserID") & "</option>")
   oRS2.MoveNext
   wend
   if Request.Cookies("Auth") = "Owner" then
      Response.Write("<option value=" & chr(34) & "all" & chr(34) & ">All</option>")
   end if
   %>
   </select>
   </td>
   </tr>
   <tr><th class="reptab">Begin Date: </th>
       <td class="reptab">
          <select name="month"><% PrintOptions 1,12,0 %></select> <b>/</b> <select name="day"><% PrintOptions 1,31,0  %></select> <b>/</b> <select name="year"><% PrintOptions 2009,2015,2010 %></select> &nbsp;&nbsp;<select name="hour"><% PrintOptions 00,23,00 %></select><b>:</b><select name="min"><% PrintOptions 00,59,00 %></select>
      </td>
   </tr>
   <tr><th class="reptab">End Date: </th>
       <td class="reptab">
          <select name="emonth"><% PrintOptions 1,12,0 %></select> <b>/</b> <select name="eday"><% PrintOptions 1,31,0  %></select> <b>/</b> <select name="eyear"><% PrintOptions 2009,2015,2010 %></select> &nbsp;&nbsp;<select name="ehour"><% PrintOptions 00,23,23 %></select><b>:</b><select name="emin"><% PrintOptions 00,59,59 %></select>
       </td>
   </tr>
</table>
<p>
<input type="hidden" name="exec" value="1">
<input type="submit" value="Generate Report">
</form>
<% end if %>


<% if exec = "1" then %>
<span style="font-size:13px;font-weight:900;">Report Results</span>
<p>
<table border="0" class="reptab">
   <tr><th class="reptab">UserID</th><th class="reptab">Summary (TaskID)</th><th class="reptab">Begin Time</th><th class="reptab">End Time</th><th class="reptab">Total Minutes</th></tr>
   <%
   lastuser = 0
   lasttask = 0
   while not oRS.EOF
      if lastuser <> clng(oRS("UserID")) and lastuser <> 0 then ''''>>  When the user changes, tally the time
         
         thisuser = FindUser(arryUsers, lastuser)
         
        'print total, add space
         Response.Write("<tr><td colspan=" & chr(34) & "20" & chr(34) & " class=" & chr(34) & "reptab" & chr(34) & ">Total Minutes for " & arryUsers(thisuser,2) & " " & arryUsers(thisuser,3) & ": " & totaltime & "</td></tr>")
         totaltime = cdbl(0)
      end if
      if lasttask <> clng(oRS("TaskID")) then ''''>> Get the Task Description when the task changes
         sql = "SELECT Summary FROM tobject WHERE ID=" & oRS("TaskID")
         set oRS3 = Server.CreateObject("ADODB.Recordset")
         oRS3.Open sql,oConn,1,1
         if oRS3.EOF then
            Response.Write("Error getting the task description")
            Response.End
         end if
         taskdesc = oRS3("Summary")
         oRS3.Close
         set oRS3 = nothing
      end if
      
      Response.Write("<tr><td class=" & chr(34) & "reptab" & chr(34) & ">" & oRS("UserID")) & "</td><td class=" & chr(34) & "reptab" & chr(34) & ">" & taskdesc & " (" & oRS("TaskID") & ")</td><td class=" & chr(34) & "reptab" & chr(34) & ">" & oRS("BeginTime") & "</td><td class=" & chr(34) & "reptab" & chr(34) & ">" & oRS("EndTime") & "</td><td class=" & chr(34) & "reptab" & chr(34) & ">" & oRS("TotalMins") & "</td></tr>"
      totaltime = cdbl(totaltime) + cdbl(oRS("TotalMins"))
      lastuser = clng(oRS("UserID"))
      lasttask = clng(oRS("TaskID"))
      oRS.MoveNext
   wend
   
   'get the info for the last user in the report
   thisuser = FindUser(arryUsers, lastuser)
   Response.Write("<tr><td colspan=" & chr(34) & "20" & chr(34) & " class=" & chr(34) & "reptab" & chr(34) & ">Total Minutes for "  & arryUsers(thisuser,2) & " " & arryUsers(thisuser,3) & ": " & totaltime & "</td></tr>")
   %>
</table>
<% end if %>

</div>
</BODY>
</HTML>
<%
if exec = "1" then
   oRS.Close
   set oRS = nothing
end if
oRS2.Close
set oRS2 = nothing
oConn.Close
set oConn = nothing

function FindUser (arry, lastuser)
   	'figure out User info - might change this to db hit?
    for x = 0 to ubound(arry)
       if clng(arry(x,0)) = clng(lastuser) then
          FindUser = x
          exit for
       end if
    next
end function

function PrintOptions (beg, top, selected)
   for i = beg to top
      if i < 10 then
         pad = "0"
      else
         pad = ""
      end if
      Response.Write("<option value=" & chr(34) & pad & i & chr(34))
      if i = selected then
         Response.Write(" selected")
      end if
      Response.Write(">" & pad & i & "</option>")
   next
end function
%>