<%@ Language=VBScript %>
<!-- #include  file="include.asp" -->
<%
'set objConnex = new AppConnection
if Request.Cookies("UserID") = "" then
   Response.Write("<h2><font color=red>You have not logged in or your session has expired.</font></h2>Please log in again.")
   Response.End
end if

stateflag=Request.item("stateflag")
if stateflag = "" then
   stateflag = "0"
end if
dim fieldnames(100)
dim fieldvals(100)
index = 0
fieldcount = 0
override = Request.Item("t")
sqlStr = Request.Item("sqlStr")
db = Request.Item("db")

if sqlStr <> "" and override = "t" then
   retcode = issueSQL(sqlStr,"","")
end if

if stateflag = "1" then
   Set Conn = Server.CreateObject( "ADODB.Connection" )
   
   '/* Add a test of your authorizations here */
   auth = Request.Cookies("Auth")
      
   if auth <> "Owner" or auth = "" then
      Response.Write("<h2><font face=arial><font color=red>You are not authorized to use this transaction.</font></h2>Logging pertinent data...<br>Exiting...</font>")
      Response.End
   end if      
   
   Conn.Open strConnect
   commtype=Request.item("commtype")
   
   sqlcomm = Request.item("sqlcomm")
   if sqlcomm = "" then
      Response.Write("<h2><font color=red>You did not specify SQL parameters.</font></h2>")
      Response.End
   end if
   
   sqlStr = commtype & " " & sqlcomm
   sqlStr = ltrim(sqlStr)
   sqlStr = rtrim(sqlStr)
   
   if db = "generic" then
      Conn.Close
      Conn.open "DSN=generic; UID=admin; PWD=;"
   end if
   
   select case(commtype)
      
   case "select"
      set RS = server.CreateObject("ADODB.Recordset")
      set RS = Conn.execute(sqlStr)
      
      if RS.eof = true then
         Response.Write("<h2><font color=red>No records returned for query:</font></h2>" & sqlStr)
         Response.End
      end if
      
      Response.Write("<table border=1><tr>")
      
      'Determine field names, output them, and capture them in array
      for each ofield in RS.Fields
         Response.Write("<th>" & ofield.name & "</th>")
         fieldnames(fieldcount) = ofield.name
         fieldcount = fieldcount + 1
         if fieldcount = 100 then
            Response.Write("<h2><font color=red>Error.  Field count has reached maximum allowed size.</font></h2>Exiting...")
            Response.End
         end if
      next
      
      Response.Write("</tr>")
            
      while RS.eof <> true
         index = 0
         Response.Write("<tr>")
         while index <> fieldcount
            fieldvals(index) = RS(fieldnames(index))
            if isnull(fieldvals(index)) then
               fieldvals(index) = "<small><small>*NULL*</small></small>"
            end if
            if fieldvals(index) = "" then
               fieldvals(index) = "<small><small>*BLANK*</small></small>"
            end if
            if fieldvals(index) = " " then
               fieldvals(index) = "<small><small>*SPACE*</small></small>"
            end if
            index = index + 1
         wend
         index = 0
         while index <> fieldcount
            Response.Write("<td>" & fieldvals(index) & "</td>")
            index = index + 1
         wend
         Response.Write("</tr>")
         RS.MoveNext
      wend
      Response.Write("</table><P><hr><hr>")
      stateflag = 0
      
   case "update"
      if instr(ucase(sqlStr),ucase("where")) = 0 then
         retcode = nowhere(sqlStr)
      else
         retcode = issueSQL(sqlStr,commtype,Conn)
      end if
      
   case "insert"
      retcode = issueSQL(sqlStr,commtype,Conn)
            
   case "delete"
      if instr(ucase(sqlStr),ucase("where")) = 0 then
         retcode = nowhere(sqlStr)
      else
         retcode = issueSQL(sqlStr,commtype,Conn)
      end if
   case "special"
      retcode = issueSQL(sqlcomm,commtype,Conn)
      
   case else
      Response.Write("<b>Command Not Currently Supported...</b>")
   end select
   Conn.Close
   set RS = nothing
   set Conn = nothing
end if

private function noWhere(sqlStr)
   Response.Write("<h2><font face=arial><font color=red>You did not supply a WHERE clause for the entered SQL statement.</font></font></h2>")
   Response.Write("<hr>")
   Response.Write("<center><table border=0><tr><td><form action=q_sql.asp method=POST>")
   Response.Write("<input type=hidden name=sqlStr value=" & chr(34) & sqlStr & chr(34) & ">")
   Response.Write("<input name=t value=t type=hidden>")
   Response.Write("<input name=db value=" & chr(34) & db & chr(34) & " type=hidden>")
   Response.Write("<input type=submit value=Proceed>")
   Response.Write("</form></td>")
   Response.Write("<td><form action=q_sql.asp method=POST>")
   Response.Write("<input type=submit value=Cancel>")
   Response.Write("</form></td></tr></table></center>")      
end function

private function issueSQL(sqlStr,command,Conn)
   on error resume next
      
   if Conn = "" then
      set Conn = server.CreateObject("ADODB.Connection")
      if db = "qbs" then
         Conn.Open strConnect
      else
         Conn.open "DSN=generic; UID=admin; PWD=;"
      end if
   end if
   
   Conn.Execute(sqlStr)
      
   if Conn.Errors.Count > 0 then
      set objErr = server.CreateObject("ADODB.Error")
      for each objErr in Conn.Errors
         if objErr.Number <> 0 then
            Response.Write("Number: " & objErr.Number & "<br>")
            Response.Write("Description: " & objErr.Description & "<br>")
         end if
      next
      set objErr = nothing
      Response.Write("<b><font color=red>" & ucase(command) & " Failed.</font></b><p><hr><hr>")
   else
      Response.Write("<b><font color=blue>" & ucase(command) & " Successful.</font></b><p><hr><hr>")
   end if
   stateflag = 0
      
end function   


%>
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script language=javascript>
   function addfavorite(fav) {
      var favtext;
      switch (fav) {
         case "1":
            favtext = "* FROM sysobjects WHERE xtype='U'";
            break;
         case "2":
            favtext = "name, id, xtype, length, xprec FROM syscolumns WHERE id=";
            break;
         default:
            favtext = "";
            break;
      }
      document.sqlcommands.sqlcomm.value = favtext;
   }
   function eventHandler(evnt) {
      if (event.keyCode == 13) {
         document.sqlcommands.submit.focus;
      } 
   }
</script>
</HEAD>
<BODY>
<font face=arial>
<% if stateflag = "0" then %>  
   <h2>Enter your command below.</h2>
   <hr>
   <form action=q_sql.asp method="POST" name="sqlcommands">
   <input type=radio name=commtype value="select" checked><b>SELECT</b>
   <input type=radio name=commtype value="insert"><b>INSERT</b>
   <input type=radio name=commtype value="update"><b>UPDATE</b>
   <input type=radio name=commtype value="delete"><b>DELETE</b>
   <input type=radio name=commtype value="special">
   <p>
   <textarea name=sqlcomm cols=60 rows=1></textarea>
   <input type=hidden name="stateflag" value="1">
   <input type=submit name=submit value="Send SQL">&nbsp;&nbsp&nbsp;<select name="db"><option value="qbs">Critical DL<option value="generic">Generic</select><br>
   <small><small><b>Favorites:</b> <input type=button onclick="addfavorite('1');" name=Test value="Fave 1" style="font-size:7px;">&nbsp;&nbsp;<input type=button onclick="addfavorite('2');" name=test2 value="Fave 2" style="font-size:7px;">&nbsp;&nbsp;<input type=button name=test3 value="Clear" onclick="addfavorite(' ');" style="font-size:7px;"></small></small>
   </form>
   <script>
      document.onkeypress = eventHandler
   </script>
<% end if %>
</font>
</BODY>
</HTML>
