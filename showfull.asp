<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2008, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

id = Request.QueryString("id")
if id = "" then
   Response.Write("The required data was not passed.")
   Response.End
end if
%>
<!-- #include file="inc_checklogin.asp" -->

<%
set oConn = server.CreateObject("ADODB.Connection")
set oRS = server.CreateObject("ADODB.Recordset")
sql = "SELECT Description FROM tobject WHERE ID=" & id

oConn.Open strConnect
oRS.Open sql, oConn, 1, 1
%>
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="demon.css">
</HEAD>
<BODY>

<%=replace(oRS("Description"),chr(13)&chr(10),"<p>")%>

</BODY>
</HTML>
<%
oRS.Close
set oRS = nothing
oConn.Close
set oConn = nothing
%>
