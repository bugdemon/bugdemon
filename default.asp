<%@ Language=VBScript %>
<%
'********************************************************************************************
'*   Copyright 2003-2008 - Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
startpage = Request.Cookies("StartPage")
if startpage = "" then
   startpage = "trackmain.asp"
end if
Response.Redirect(startpage)
%>