<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<!-- #include file="inc_checklogin.asp" -->
<%

firstname = Request.Form("firstname")
lastname = Request.Form("lastname")
email = Request.Form("email")
password = Request.Form("password")
newpass = Request.Form("newpass")
repnew = Request.Form("repnew")
startpage = Request.Form("startpage")

if firstname = "" or lastname = "" or email = "" or password = "" or startpage = "" then
   Response.Write("The required data was not passed.")
   Response.End
end if

sql = "SELECT UserPass FROM users WHERE ID=" & Request.Cookies("UID")

set oConn = server.CreateObject("ADODB.Connection")
set oRS = server.CreateObject("ADODB.Recordset")

oConn.Open strConnect
oRS.Open sql, oConn, 1, 1

if oRS.EOF then
   oRS.Close
   set oRS = nothing
   oConn.Close
   set oConn = nothing
   Response.Write("The specified User ID does not exist.")
   Response.End
end if

if oRS("UserPass") <> password then
   oRS.Close
   set oRS = nothing
   oConn.Close
   set oConn = nothing
   Response.Write("An incorrect password was specified.  Data not updated.")
   Response.End
end if

passupdate = false
if newpass <> "" or repnew <> "" then
   passupdate = true
   if newpass <> repnew then
      oRS.Close
      set oRS = nothing
      oConn.Close
      set oConn = nothing
      Response.Write("New passwords do not match.  Data not updated.")
      Response.End
   end if
end if

'/* Everything is good to save */
timestamp = now()
if dbType = "MySQL" then
   timestamp = MySQLDate(timestamp)
end if
sql = "UPDATE users SET FirstName='" & firstname & "', LastName='" & lastname & "', StartPage='" & startpage & "', LastUpdated=" & delim & timestamp & delim

if passupdate then
   sql = sql & ", UserPass='" & newpass & "'"
end if

sql = sql & " WHERE ID=" & Request.Cookies("UID")

oConn.Execute(sql)
oRS.Close
set oRS = nothing
oConn.Close
set oConn = nothing

'/* Update the new "home" page */
Response.Cookies("StartPage") = startpage
Response.Cookies("StartPage").Expires = date() + 365

Response.Redirect("options.asp?s=1")
%>