<%@ Language=VBScript %>
<!-- #include = file="include.asp"-->
<%
'********************************************************************************************
'*   Copyright 2002 - Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one licese per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************

%>
<!-- #include file="inc_checklogin.asp" -->
<%

set oConn = server.CreateObject("ADODB.Connection")
set oRS = server.CreateObject("ADODB.Recordset")

oConn.Open strConnect

sqlStr = "SELECT productname FROM products ORDER BY SortOrder"

set oRS = oConn.Execute(sqlStr) 
%>

<html>
   <head>
      <script language="JavaScript">
         function submitform() {
         //add checking here
            document.form1.submit();
         }
      </script>
   </head>
   
   <body>
   <font face=arial>
   <table border=0>
      <tr bgcolor=LightSteelBlue valign=middle><th valign=middle><h3><%=coName%>&nbsp;<%=appName%> &nbsp;&nbsp;</h3></th><th>&nbsp;&nbsp;&nbsp;&nbsp;</th><th>&nbsp;&nbsp;&nbsp;</th><th>&nbsp;&nbsp;</th><th>&nbsp;</th></tr>
   </table>
   <!-- #include file="inc_nav.asp" -->
   <form action="save_workinfo.asp" name=form1 method=POST>
   <table border=0 cellpadding=0 cellspacing=0 bordercolor=silver>
      <tr><td colspan=4><small><b>Add Work Information</b></td></tr>
      <tr><td colspan=4><hr style="color:lightsteelblue"></td></tr>
      <tr><td><small><b>Product:&nbsp;&nbsp; </b></td><td><select style="font-size:10px;" name=product size=1>
         <%
            while oRS.EOF <> true
               Response.Write("<option value=" & chr(34) & oRS("ProductName") & chr(34) & ">" & oRS("ProductName"))
               oRS.MoveNext
            wend
         %>
         </select>&nbsp;&nbsp;</td><td><small><b>Version:&nbsp;&nbsp; </b></td><td><input type=text style="font-size:10px;" size=15 name=version></td></tr>
      <tr><td colspan=4><hr style="color:lightsteelblue"></td></tr>
      <tr><td colspan=4 align=center><small><b>Work Description</b></td></tr>
      <tr><td colspan=4 align=center>&nbsp;&nbsp;<textarea cols=70 rows=6 name=workdesc style="font-size:12px;font-face:arial;"></textarea>&nbsp;&nbsp;</td></tr>
      <tr><td colspan=4>&nbsp;</td></tr>
      <tr><td><small><b>Duration:</b>&nbsp;&nbsp;</td><td><input type=text style="font-size:10px;" size=5 name=duration>&nbsp<small>(In Hours)</small></td><td>&nbsp;</td><td>&nbsp;</td></tr>
      <tr><td colspan=4><hr style="color:lightsteelblue"></td></tr>
      <tr><td colspan=4><input type=button onclick="submitform();" style="background-color:lightsteelblue;font-size:10px;color:white;" value="Save Data" id=button1 name=button1></td></tr>
   </table>
   </form>
   </font>
   </body>
</html>

<%
oRS.Close
set oRS = nothing
oConn.Close
set oConn = nothing
%>