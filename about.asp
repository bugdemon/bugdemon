<%@ Language=VBScript %>
<!-- #include file="include.asp" -->

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/header.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/header.html" -->
<% end if %>

<div style="padding:10px;">
<h1>About BugDemon</h1>
BugDemon began in 2000 as an ASP (not considered "Classic" at that time) alternative to Bugzilla.  I wanted the functionality, but I worked in an all Microsoft 
shop at the time.  That was before the big Open Source movement and MySQL, Apache and PERL were out of the question!  So, I began a project in my 
spare time to create a tool that would provide organization, tracking, and communication for the development projects I was working on.  Since the first version 
was released in early 2001, BugDemon (or UglyDemon as it was affectionately known) was used extensively in one of the largest food manufacturing 
companies in the world - which shall remain nameless.  In 2003, BugDemon was given a facelift.  It was still ugly, but was far better looking than 
it was.  You can still see that early theme by setting the theme to '2003'.  *giggles*  In 2004, I made another set of changes to allow configuration 
of the field names as I was forcing BugDemon into service as a Project Management tool - tracking tasks and assignments still, but the field names 
didn't fit well.  I also expanded the functionality to support the MySQL database instead of just MS SQL Server.  About this time, I began the transition 
to utilizing PHP in my daily work as ASP was being "Classic-ized" and .NET was taking over.
<p>
The product lay dormant for 5 years until 2012, when I began working for a small company doing development work again.  Why not use a more robust product? 
Well because I am a busy body and figured, this would be a great time to port it to PHP.  All developers need a "pet project" and this is mine. But instead 
of beginning the PHP port, the company adopted it and began using it extensively.  There were calls for enhancements and bug fixes.  So, I dusted off 
my VBScript hat and put it back on.  Windows Server 2013 still supports Classic ASP so I still have time for that PHP conversion.  Someday...
<p>
<small>For a complete list of changes and timelines, please see the <a href="#">Change Log</a></small>
</div>

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/footer.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/footer.html" -->
<% end if %>