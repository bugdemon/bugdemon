<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2014, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************

%>
<HTML>
<HEAD><TITLE>Add User</TITLE>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script language="JavaScript">
   function submitform() {
      if (document.user.userid.value == '') {
         alert("You must specify a valid E-mail Address");
         return 1;
      }
      if (document.user.password.value == '') {
         alert("You must specify a Password");
         return 1;
      }
      if (document.user.firstname.value == '') {
         alert("You must specify a First Name");
         return 1;
      }
      if (document.user.lastname.value == '') {
         alert("You must specify a Last Name");
         return 1;
      }
      document.user.submit();
      return 0;
   }
</script>
<link rel="stylesheet" type="text/css" href="demon.css">
</HEAD>
<BODY>

<div style="width:900px;">
<!-- #include file="inc_header.asp" -->
<div style="">
<form action="saveuser.asp" name="user" method="POST">
<table border=0>
   <tr><td colspan=5><hr style="color:lightsteelblue;"></td></tr>
   <tr><td><b>E-mail Address:</b></td><td align=left width=200>&nbsp;&nbsp;<input type=text size=30 name=userid style="font-size:10px;"></td><td width=150>&nbsp;</td></tr>
   <tr><td><b>Password:</b></td><td align=left width=200>&nbsp;&nbsp;<input type=text size=30 name=password style="font-size:10px;"></td><td></td></tr>
   <tr><td><b>First Name:</b></td><td align=left width=200>&nbsp;&nbsp;<input type=text size=30 name=firstname style="font-size:10px;"></td><td></td></tr>
   <tr><td><b>Last Name:</b></td><td align=left width=200>&nbsp;&nbsp;<input type=text size=30 name=lastname style="font-size:10px;"></td><td></td></tr>
   <tr><td colspan=2><hr style="color:lightsteelblue;"></td></tr>
   <tr><td colspan=2><input type=button onclick="submitform();" class="butt" value="Save User Information"></td><td></td></tr>
</table>
</form>
</div>
</div>
</BODY>
</HTML>
