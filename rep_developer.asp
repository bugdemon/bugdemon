<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<!-- #include file="inc_FieldDef.asp" -->

<!-- #include file="inc_checklogin.asp" -->
<%
if Request.Cookies("Auth") = defUType then
   Response.Write("This section is reserved for developers and administrators.")
   Response.End
end if
if Request.Cookies("Auth") = "" then
    'TODO: we might want to expire the landing page cookie
    Response.Redirect("logon.asp")
end if

'add the option to run the report for all developers (for admins only)

'pull the data for the user that's logged in
sql = "SELECT * FROM tobject WHERE AssignedTo='" & replace(Request.Cookies("UserID"),"'","''") & "' AND Status <> 'Complete' AND Status <> 'Closed' AND Status <> 'Duplicate' ORDER BY ParentID ASC, ID ASC"

set oConn = Server.CreateObject("ADODB.Connection")
set oRS = Server.CreateObject("ADODB.Recordset")

oConn.Open strConnect
oRS.Open sql,oConn,1,1
%>
   
<%if theme = "2003" then
    table_class = "reptab"
%>
    <!-- #include file="themes/2003/header.html" -->
<% elseif theme = "2013" then
    table_class = "table1"
%>
    <!-- #include file="themes/2013/header.html" -->
<% end if %>

<% if (Request.Cookies("Auth") = "Owner" or Request.Cookies("Auth") = "Developer") and modText <> "" then %>
   <div style="padding-bottom:10px;padding-top:10px;padding-left:5px;font-size:12px;width:900px;border:1px solid red;"><img src="images/exclamation.png" border="0" align="absmiddle"><%=modText%></div>
<% end if %>
<div style="padding-top:10px;width:100%;padding-bottom:20px;">
<span style="font-size:18px;font-weight:900;">&nbsp;My Open Tasks</span>
<p>
<table border="0" class="<%=table_class%>" width="100%">
   <tr><th class="reptab" width="20">ID</th><th class="reptab">Product</th><th class="reptab">Summary</th><th class="reptab">Date Entered</th><th class="reptab">Last Updated</th><th class="reptab">Last Updated By</th></tr>
   <% if oRS.EOF then%>
      <tr><td colspan="4">You do not have any tasks assigned.</td></tr>
   <% end if %>
   
   <%  while not oRS.EOF
       classname = "reptab"
       if clng(oRS("ID")) <> clng(oRS("ParentID")) then
           classname = "reptab_child"
       end if
   %>   
      <tr><td class="<%=classname%>"><a href="newrequest.asp?id=<%=oRS("ID")%>"><%=oRS("ID")%></a></td><td class="reptab"><%=oRS("Product")%></td><td class="reptab"><a href="newrequest.asp?id=<%=oRS("ID")%>"><%=oRS("Summary")%></a></td><td class="reptab"><%=oRS("DateEntered")%></td><td class="reptab"><%=oRS("LastUpdated")%></td><td class="reptab"><%=oRS("LastUpdatedBy")%></td>
   <% 
      oRS.MoveNext
      wend
   %>
</table>
<%
'Pick up the most recently completed
if dbType = "MSSQL" then
   sql = "SELECT TOP 5 * FROM tobject WHERE AssignedTo='" & replace(Request.Cookies("UserID"),"'","''") & "' AND (Status = 'Complete' OR Status = 'Closed' OR Status = 'Duplicate') ORDER BY ParentID ASC, ID ASC, LastUpdated DESC"
else
   sql = "SELECT * FROM tobject WHERE AssignedTo='" & replace(Request.Cookies("UserID"),"'","''") & "' AND (Status = 'Complete' OR Status = 'Closed' OR Status='Duplicate') ORDER BY ParentID ASC, ID ASC, LastUpdated DESC LIMIT 0,5"
end if

oRS.Close

oRS.Open sql,oConn,1,1
%>
<p>
<span style="font-size:18px;font-weight:900;">&nbsp;Last 5 Completed Tasks</span>
<p>
<table border="0" class="<%=table_class%>" width="100%">
   <tr><th class="reptab">ID</th><th class="reptab">Product</th><th class="reptab">Summary</th><th class="reptab">Date Entered</th><th class="reptab">Last Updated</th><th class="reptab">Last Updated By</th></tr>
   <% if oRS.EOF then%>
      <tr><td colspan="4">You do not have any tasks assigned.</td></tr>
   <% end if %>
   
   <% while not oRS.EOF %>
        
      <tr><td class="reptab"><a href="newrequest.asp?id=<%=oRS("ID")%>"><%=oRS("ID")%></a></td><td class="reptab"><%=oRS("Product")%></td><td class="reptab"><a href="newrequest.asp?id=<%=oRS("ID")%>"><%=oRS("Summary")%></a></td><td class="reptab"><%=oRS("DateEntered")%></td><td class="reptab"><%=oRS("LastUpdated")%></td><td class="reptab"><%=oRS("LastUpdatedBy")%></td>
   <% 
      oRS.MoveNext
      wend
   %>
</table>
</div>
<%if theme = "2003" then %>
    <!-- #include file="themes/2003/footer.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/footer.html" -->
<% end if %>
<%
oRS.Close
set oRS = nothing
oConn.Close
set oConn = nothing
%>