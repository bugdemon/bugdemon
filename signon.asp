<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2008, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

'This fix can be removed 1/23/2009 - adding this is easier than getting everyone to clear their cookies.
if Request.Cookies("ID") <> "" then
   Response.Cookies("ID").Expires = date() - 100
end if
'********** End fix

referpage = Request.Item("pagename")
userid = Request.Item("userid")
password = Request.Item("password")
persistit = Request.Item("persistit")

if userid = "" or password = "" then
   Response.Write("Required data not specified.")
   Response.End
end if

set oConn = Server.CreateObject("ADODB.Connection")
set RS = Server.CreateObject("ADODB.Recordset")
oConn.Open strConnect

sqlStr = "SELECT * FROM users WHERE UserID='" & userid & "'"

set RS = oConn.Execute(sqlStr)
if RS.EOF = true then
   Response.Write("<font face=arial>Your User profile could not be found.<P>")
   Response.Write("<a href=adduser.asp>Create User ID</a> | <a href=trackmain.asp>Return to Main</a></font>")
   Response.End
end if

if password <> RS("UserPass") then
   Response.Write("<font face=arial>Log On Failed.<P>")
   Response.Write("<a href=logon.asp>Try Again</a> | <a href=trackmain.asp>Return to Main</a></font>")
   Response.End
end if

'determine if there is already an open session for the user with today's date
set oRS = Server.CreateObject("ADODB.Recordset")
timestamp = now()
if dbType="MySQL" then
   timestamp = MySQLDate(timestamp)
end if

dateonly = date()
if dbType="MySQL" then
   pos = instr(timestamp," ")
   dateonly = trim(left(timestamp,pos))
end if

sql = "SELECT * FROM logins WHERE LogoutDate IS NULL AND USERID=" & RS("ID") & " AND LoginDate > " & delim & dateonly & delim
oRS.Open sql,oConn,1,1

if not oRS.EOF then
   Response.Write("You already have an existing session for today, please <a href=" & chr(34) & "logout.asp?f=1" & chr(34) & ">log out</a> before trying to log in again.")
   
   'in case the userid isn't set, set it
   Response.Cookies("UID") = RS("ID")
   
   'clean up objects
   oRS.Close
   set oRS = nothing
   RS.Close
   set RS = nothing
   oConn.Close
   set oConn = nothing
   
   Response.End
end if


'log the valid login in the databse
sql = "INSERT INTO logins(UserID,LoginDate,UserAgent,IP)VALUES(" & RS("ID") & "," & delim & timestamp & delim & ",'" & replace(Request.ServerVariables("HTTP_USER_AGENT"),"'","''") & "','" & Request.ServerVariables("REMOTE_ADDR") & "')"
oConn.Execute(sql)


'set a browswer session cookie
Response.Cookies("UID") = RS("ID")
Response.Cookies("UserID") = RS("UserID")
Response.Cookies("Auth") = RS("UserType")
Response.Cookies("First") = RS("FirstName")
Response.Cookies("Last") = RS("LastName")
Response.Cookies("StartPage") = RS("StartPage")

'The user's start page should persist regardless
Response.Cookies("StartPage").Expires = date() + 365

if persistit = "ON" then  'set cookie to persist per the configuration
   Response.Cookies("UID").Expires = date() + persistDays
   Response.Cookies("UserID").Expires = date() + persistDays
   Response.Cookies("Auth").Expires = date() + persistDays
   Response.Cookies("First").Expires = date() + persistDays
   Response.Cookies("Last").Expires = date() + persistDays
end if

oRS.Close
set oRS = nothing
RS.Close
set RS = nothing
oConn.Close
set oConn = nothing

if referpage <> "" then
   Response.Redirect(referpage)
else
   'Response.Write(Response.Cookies("StartPage"))
   Response.Redirect(Request.Cookies("StartPage"))
end if
%>
