<%@ Language=VBScript %>
<!-- #include file="include.asp"-->
<%
'********************************************************************************************
'*   Copyright 2003-2014 - Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one licese per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************

auth = Request.Cookies("Auth")
if auth = defUType  or Request.Cookies("Auth") = "" then
   Response.Write("This is the administrative section.  Your authorization level indicates you are not an administrator.")
   Response.End
end if

pagetitle = "Report Overview"

%>
<!-- #include file="inc_checklogin.asp" -->

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/header.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/header.html" -->
<% end if %>     
  
   <div style="font-size:11pt;font-weight:900;padding-top:10px;padding-left:10px;">Reporting Menu</div>
   <ul>
     <li><a href="rep_developer.asp">My Open Assignments</a></li>
     <li><a href="trackmain.asp">All Tasks</a></li>
     <li><a href="rep_time.asp">Time Report</a></li>
     <li><a href="rep_work.asp">Work List</a></li>
   </ul>
  
<%if theme = "2003" then %>
    <!-- #include file="themes/2003/footer.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/footer.html" -->
<% end if %>