<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2014, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

referpage = Request.Item("pagename")
id = Request.Item("id")
if id <> "" then
   referpage = referpage & "?id=" & id
end if
%>

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/header.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/header.html" -->
<% end if %>
<script language="JavaScript">
   function submitform() {
      if (document.signon.userid.value == '') {
         alert("You did not specify your User ID");
         return 1;
      }
      if (document.signon.password.value == '') {
         alert("You did not specify your Password");
         return 1;
      }
      document.signon.submit();
   }
</script>
<!--
<div style="float:left;text-align:left;width:900px;display:inline;">
-->
<form action="signon.asp" method="POST" name="signon">
<table border="0">
   <tr><th align="left" colspan="2">Log In Information</th></tr>
   <tr><td colspan="3" width="900"><hr style="color:lightsteelblue;"></td></tr>
   <tr><th align="left" width="100"><small>User ID:</small></th><td align="left"><input type="text" style="font-size:10px;" name="userid" size="30"></td><td width=100>&nbsp</td></tr>
   <tr><th align="left"><small>Password:</small></th><td align="left"><input type="password" style="font-size:10px;" name="password" size="30"></td><td></td></tr>
   <tr><td>&nbsp;</td><td><input type="checkbox" name="persistit" value="ON" checked><small>Keep Me Logged In</small></td></tr>
   <tr><td colspan="2"><hr style="color:lightsteelblue;"></td></tr>
   <tr><td>&nbsp;<td><input type="button" onclick="submitform();" class="butt" value="Log On To BugDemon"></td><td></td></tr>
</table>
<input type="hidden" name="pagename" value="<%=referpage%>">
</form>
<!--
</div>
</div>
-->
<%if theme = "2003" then %>
    <!-- #include file="themes/2003/footer.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/footer.html" -->
<% end if %>
