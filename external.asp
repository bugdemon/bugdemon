<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<!-- #include file="inc_checklogin.asp" -->
<%
'/************************************************************/
'/*** This file is used to link to an external data source ***/
'/*** presumably to display detailed information about     ***/
'/*** your product - this is what the file is currently    ***/
'/*** tied into.  See, exteranLookup in include.asp        ***/
'/************************************************************/

site = Request.QueryString("site")
if site = "" then
   Response.Write("Site cannot be blank")
   Response.End
end if

'variables used to connect to the external data source
extDBServer = "172.16.1.13"
extDBPort = "3306"
extDBName = "QBSSites"
extDBUser = "qbssiteslogin"
extDBPass = "S1tem3"

strConnect = "Driver={MySQL ODBC 3.51 Driver};Server=" & extDBServer & ";Port=" & extDBPort & ";Option=131072;Stmt=;Database=" & extDBName & ";Uid=" & extDBUser & ";Pwd=" & extDBPass &";"
sql = "SELECT IPID FROM sites where Name='" & lcase(site) & "'"

set oConn = Server.CreateObject("ADODB.Connection")
set oRS = Server.CreateObject("ADODB.Recordset")
oConn.Open strConnect
oRS.Open sql, oConn, 1, 1

if oRS.EOF then
   Response.Write("No Matching Record Found")
   oRS.Close
   set oRS = nothing
   oConn.Close
   set oConn = nothing
   Response.End
end if

ipid = oRS("IPID")
oRS.Close

sql = "SELECT * FROM ipaddresses AS I LEFT JOIN servers AS S ON I.ServerID=S.ServerID WHERE ID=" & ipid
oRS.Open sql, oConn, 1, 1

if oRS.EOF then
   Response.Write("No Detailed Data Found")
   oRS.Close
   set oRS = nothing
   oConn.Close
   set oConn = nothing
   Response.End
end if
%>
<HTML>
<HEAD><title>External Data Lookup</title>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="demon.css">
</HEAD>
<BODY>

<div>
<h2><%=site%></h2>
Server: <span style="color:blue;font-weight:900;"><%=oRS("Description")%></span>
<table border="0" class="reptab">
<tr><th class="reptab">External IP</th><th class="reptab">Internal IP</th><th class="reptab">Primary</th>
<%
while not oRS.EOF
   Response.Write("<tr><td>" & oRS("IPAddress") & "</td><td>" & oRS("PrivateIP") & "</td><td>")
   if oRS("Primary") = "1" then
      Response.Write("&nbsp;Yes")
   else
      Response.Write("&nbsp;")
   end if
   Response.Write("</td></tr>")
   oRS.MoveNext
wend
%>
</table>
</div>

</BODY>
</HTML>
<%
oRS.Close
set oRS = nothing
oConn.Close
set oConn = nothing
%>