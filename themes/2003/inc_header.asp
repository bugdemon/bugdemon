<%
'This is the main header

script = Request.ServerVariables("SCRIPT_NAME")

%>

<div id="header">
    <table border="0">
       <% if script = "/newrequest.asp" or script = "/reports.asp" or script = "/options.asp" then %>
            <tr bgcolor="#61A6D0" valign="middle">
                <th valign="middle" class="title"><b>&nbsp;<%=coName%>&nbsp;<%=appName%>&nbsp;&nbsp;</b></th>
                <th class="title2">&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th class="title3">&nbsp;&nbsp;&nbsp;</th>
                <th class="title4">&nbsp;&nbsp;</th>
                <th class="title5">&nbsp;</th>
                <th width="300" align="right" bgcolor="white"><img src="/themes/2003/images/bug.gif" align="absmiddle" border="0"></th>
            </tr>
       <% end if %> 
       <% if script = "/trackmain.asp" or script = "/rep_developer.asp" then %> 
           <tr bgcolor="#61A6D0" valign="middle">
                <th valign="middle" class="title"><%=coName%>&nbsp;<%=appName%> &nbsp;&nbsp;</th>
                <th valign="middle" class="title2">&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th valign="middle" class="title3">&nbsp;&nbsp;&nbsp;</th>
                <th valign="middle" class="title4">&nbsp;&nbsp;</th>
                <th valign="middle" class="title5">&nbsp;</th>
                <th width="500" align="right" bgcolor="white" valign="middle"><span style="font-size:8px;">Powered By:&nbsp;</span><img src="/themes/2003/images/logo.gif" height="25"  align="absmiddle" border="0>"</th>
           </tr>
       <% end if %>
       <% if script = "/watches.asp" or script = "/addcomment.asp" or script = "/logon.asp" or script = "/adduser.asp" then %>
            <tr bgcolor="#61A6D0" valign="middle">
                <th valign="middle" height="40" class="title"><b>&nbsp;<%=coName%>&nbsp;<%=appName%> &nbsp;&nbsp;</b></th>
                <th class="title2">&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th class="title3">&nbsp;&nbsp;&nbsp;</th>
                <th class="title4">&nbsp;&nbsp;</th>
                <th class="title5">&nbsp;</th>
            </tr>
       <% end if %>
    </table>
</div>
