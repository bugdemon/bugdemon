<%
scriptname = replace(Request.ServerVariables("SCRIPT_NAME"),"/","")
startpage = Request.Cookies("StartPage")
if startpage = "" then
   startpage = "trackmain.asp"
end if
%>
<style>.fltr{font-size:11px;}</style>
<div id="container" style="width:100%;">
    <div id="content">
        <% if scriptname <> "addcomment.asp" and scriptname <> "logon.asp" then %>
        <table border="0" cellpadding="0" cellspacing="0" width="950">
            <tr><td width="330" style="border-bottom-style:solid;border-bottom-width:1px;border-bottom-color:lightsteelblue;"><small><a href="newrequest.asp">Submit</a> | Search | <a href=watches.asp>My Watches</a> | <a href=reports.asp>Reports</a> | <a href="options.asp">Options</a> | <a href="<%=startpage%>">HOME</a></small></td><td width=400 align="right" style="border-bottom-style:solid;border-bottom-width:1px;border-bottom-color:lightsteelblue;"><small><%if userid="" then%><a href="logon.asp">Log In</a><%else%>You are logged in as <font color=blue><%=userid%></font>.&nbsp;&nbsp;[<a href="logout.asp">Log Out</a>]<%end if%></small></td></tr>
            <form action="list.asp" name="search" method="POST">
                <tr><td colspan="10" width="900" style="padding-top:5px;padding-bottom:7px;border-bottom-style:solid;border-bottom-width:1px;border-bottom-color:lightsteelblue;"><small>Quick Search:</small>  <input type="text" name="term" size="35" style="font-size:10px;">&nbsp;<input type="button" class="butt" onClick="Search();" value="GO!" id=button1 name=button1><input type="hidden" name="q" value=""></td></tr>
            </form>
        <% end if %>
   <% if scriptname = "trackmain.asp" then %>
   <form name="options">
   <tr>
        <td style="padding-top:10px;font-size:12px;"><input type=checkbox name=hidecomp onclick="DoComp()" <%if comp = 1 then%>checked<%end if%>> Hide Completed Requests </td>
        <td>
            <select name="fltrPriority" id="fltrPriority" class="fltr" onchange="Filter(this.name,this.value);">
                <option value="">Filter By Priority</option>
                <%
                for i = 0 to ubound(arryPriority,2)
                    Response.Write("<option value=" & chr(34) & arryPriority(0,i) & chr(34) & " ")
                    if fltrval = arryPriority(0,i) then
                        Response.Write("selected")
                    end if
                    Response.Write(">" & arryPriority(1,i) & "</option>" & chr(10) & chr(13))
                next                
                %>
            </select>
            <select name="fltrStatus" id="fltrStatus" class="fltr" onchange="Filter(this.name,this.value);">
                <option value="">Filter By Status</option>
                <%
                for i = 0 to ubound(arryStatus,2)
                    Response.Write("<option value=" & chr(34) & arryStatus(0,i) & chr(34) & " ")
                    if fltrval = arryStatus(0,i) then
                        Response.Write("selected")
                    end if
                    Response.Write(">" & arryStatus(0,i) & "</option>" & chr(10) & chr(13))
                next                
                %>
            </select>
            <select name="fltrProduct" id="fltrProduct" class="fltr" onchange="Filter(this.name,this.value);">
                <option value="">Filter By Product</option>
                <%
                for i = 0 to ubound(arryProducts,2)
                    Response.Write("<option value=" & chr(34) & arryProducts(0,i) & chr(34) & " ")
                    if fltrval = arryProducts(0,i) then
                        Response.Write("selected")
                    end if
                    Response.Write(">" & arryProducts(0,i) & "</option>" & chr(10) & chr(13))
                next                
                %>
            </select>
                
            <select name="fltrEnteredBy" id="fltrEnteredBy" class="fltr" onchange="Filter(this.name,this.value);">
                <option value="">Filter By Owner</option>
                <%
                for i = 0 to ubound(arryUsers,2)
                    Response.Write("<option value=" & chr(34) & arryUsers(0,i) & chr(34) & " ")
                    if fltrval = arryUsers(0,i) then
                        Response.Write("selected")
                    end if
                    if Len(arryUsers(0,i)) > 20 then
                        arryUsers(0,i) = Left(arryUsers(0,i),20) & "..."
                    end if
                    Response.Write(">" & arryUsers(0,i) & "</option>" & chr(10) & chr(13))
                next         
                %>
            </select>
        </td>
   </tr>
   </form>
   <% end if %>
   <% if scriptname = "watches.asp" then %>
   <form name="options">
   <tr>
        <td style="padding-top:10px;font-size:12px;"><input type=checkbox name=hidecomp onclick="DoComp()" <%if comp = "1" then%>checked<%end if%>> Hide Completed Requests </td>
   </tr>
   <% end if %>
   </table>
   </div>
   <div id="content_main">