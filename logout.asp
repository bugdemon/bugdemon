<%@ Language=VBScript %>
<!-- #include virtual="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2008, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

startpage = Request.Cookies("StartPage")
if startpage = "" then
   startpage = "trackmain.asp"
end if

id = Request.Cookies("UID") 'needed later in script to select login/session record

Response.Cookies("UID").Expires = Date() - 100
Response.Cookies("UserID").Expires = Date() -100
Response.Cookies("Auth").Expires = Date() -100
Response.Cookies("First").Expires = Date() -100
Response.Cookies("Last").Expires = Date() -100

if id = "" then
    Response.redirect(startpage)
end if

'now flag the last session as expired.
timestamp = now()
if dbType="MySQL" then
   timestamp = MySQLDate(timestamp)
end if
'change this to differentiate between dbs
sql = ""
if dbType = "MSSQL" then
   sql = "SELECT TOP 1 * FROM logins WHERE UserID=" & id & " ORDER BY ID DESC"
else
   sql = "SELECT ID from logins WHERE UserID=" & id & " ORDER BY ID DESC LIMIT 0,1"
end if

Response.Write(sql)

set oConn = Server.CreateObject("ADODB.Connection")
set oRS = Server.CreateObject("ADODB.RECORDSET")

oConn.Open strConnect
oRS.Open sql,oConn,1,1

if oRS.EOF <> true then
   sql = "UPDATE logins SET LogoutDate=" & delim & timestamp & delim & " WHERE ID=" & oRS("ID")
   oConn.Execute(sql)
end if

'TODO: close all tasks being timed?

oRS.Close
set oRS = nothing
oConn.Close
set oConn = nothing



Response.Redirect(startpage)
%>