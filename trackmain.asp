<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<!-- #include file="inc_FieldDef.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2014, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************

'in the event there is a task ID cached, expire it
Response.Cookies("tobject").Expires = date() - 100

'if the system is closed, kick the user
if openSystem = false then
   'check to see if the user is logged in
   %>
      <!-- #include file="inc_checklogin.asp" -->
   <%
   if Request.Cookies("Auth") = defUType then
      Response.Write("This " & appName & " system is a closed system.  If you require access, please contact the system administrator.")
      Response.End
   end if
end if
pagename = "Task Overview"
userid = Request.Cookies("UserID")
sortby = Request.Item("sortby")
ascdesc = Request.Item("ascdesc")
startpoint = Request.Item("start")
comp = Request.Cookies("ShowComp") 'Allow the completed items to be hidden
fltrname = Request.Item("filter")
fltrval = Request.Item("val")
if comp = "" then
   comp = 0
end if
if comp = 1 then
   showComp = false
end if
if startpoint = "" then
   startpoint = 0
end if
'if the filter is set, prepare it for use
if fltrname <> "" then
    'trim off the fltr
    fltrname = Replace(fltrname,"fltr","")
    fltrname = Lcase(fltrname)
end if

parms = ""

'TODO:  figure out paging solution for MSSQL and implement (1/18/08)
if dbType = "MSSQL" then
   'sqlStr = "SELECT TOP " & maxShow & " ID,Summary,LastUpdated,EnteredBy,Status,FixRelease,ParentID, AssignedTo,DATEADD(D, 0, DATEDIFF(D, 0, DueDate)) AS DueDate,Product FROM tobject"
   sqlStr = "SELECT ID,Summary,LastUpdated,EnteredBy,Status,FixRelease,ParentID, AssignedTo,DATEADD(D, 0, DATEDIFF(D, 0, DueDate)) AS DueDate,Product FROM tobject"
else
   sqlStr = "SELECT ID,Summary,LastUpdated,EnteredBy,Status,FixRelease,AssignedTo,DATE(DueDate) AS DueDate, ParentID,Product FROM tobject"
end if

if showComp <> true then
   parms = " WHERE tobject.Status <> 'Duplicate' AND tobject.Status <> 'Complete' AND tobject.Status <> 'Closed'"
end if

if showNew = false then
   if showComp <> true then
      parms = parms & " AND tobject.Status <> 'New'"
   else
      parms = parms & " WHERE tobject.Status <> 'New'"
   end if 
end if

if fltrname <> "" then
    if showComp <> true then
        parms = parms & " AND tobject." & fltrname & "='" & Replace(fltrval,"'","''") & "'"
    else
        parms = parms & " WHERE tobject." & fltrname & "='" & Replace(fltrval,"'","''") & "'"
    end if
end if

if sortby <> "" then
   parms = parms & " ORDER BY " & sortby & ", ParentID, ID " & ascdesc
else
   parms = parms & " ORDER BY ParentID, ID, LastUpdated"
end if

if dbType = "MySQL" then
   parms = parms & " LIMIT " & startpoint & "," & maxShow
end if

'prepare the count sql parameters
countparms = parms
pos = instr(countparms,"ORDER BY")
if pos > 0 then
    countparms = left(countparms, pos-1)
end if

'tack on the parameters to the base SQL
sqlStr = sqlStr & parms
'response.write sqlStr

set oConn = Server.CreateObject("ADODB.Connection")
set RS = Server.CreateObject("ADODB.Recordset")
set oRS = Server.CreateObject("ADODB.Recordset")

oConn.Open strConnect

'get record count - for paging purposes
sql = "SELECT COUNT(ID) AS TheCount FROM tobject" & countparms

oRS.Open sql,oConn,1,1
reccount = oRS("TheCount")
oRS.Close

'get unique submitters
sql = "SELECT DISTINCT(enteredby) FROM tobject"
if showComp <> true then
   parms = " WHERE tobject.Status <> 'Duplicate' AND tobject.Status <> 'Complete' AND tobject.Status <> 'Closed'"
end if
oRS.Open sql,oConn,1,1
if oRS.EOF then
    dim arryUsers(0,0)
else
    arryUsers = oRS.GetRows()
end if
oRS.Close

'now get all statuses
sql = "SELECT StatusDesc FROM statuses ORDER BY StatusID"
oRS.Open sql,oConn,1,1
arryStatus = oRS.GetRows()
oRS.Close

'then get all priorities
sql = "SELECT priorityid,prioritydesc FROM priorities ORDER BY escalationpath"
oRS.Open sql,oConn,1,1
arryPriority = oRS.GetRows()
oRS.Close

'and lastly the products
sql = "SELECT ProductName FROM products ORDER BY SortOrder"
oRS.Open sql,oConn,1,1
arryProducts = oRS.GetRows()
oRS.Close

set oRS = nothing

'do we need to page?
paging = false
if clng(reccount) > clng(maxShow) then
  paging = true
end if
'Response.Write(sqlStr)
RS.Open sqlStr,oConn
count = RS.RecordCount
bcolor = "white"
%>

<%if theme = "2003" then 
    class_table = "rrow"
%>
    <!-- #include file="themes/2003/header.html" -->
<% elseif theme = "2013" then 
    class_table = "table1"
%>
    <!-- #include file="themes/2013/header.html" -->
<% end if %>

<script type="text/javascript">
function DoComp() {
    var now = new Date();
    var aYearLater = new Date(now.getTime() + 1000 * 60 * 60 * 24 * 365);
    var aDayAgo = new Date(now.getTime() + 1000 * 60 * 60 * 24 * -1);
    if (document.options.hidecomp.checked == true) {
        document.hiddenvals.comp.value = 1;
        document.cookie = "ShowComp=1;expires=" + aYearLater.toGMTString();
    } else {
        document.hiddenvals.comp.value = 0;
        document.cookie = "ShowComp=1;expires=" + aDayAgo.toGMTString();
    }
    document.hiddenvals.submit();
}
function Search() {
    if(document.search.term.value == '') {
        alert('You did not enter any search criteria.');
        return false;
    }
    document.search.submit();
}
function Filter(filter,value) {
    if(value == '') {
        alert('You must select a value to filter on.');
        return false;
    }
    this.location='trackmain.asp?filter=' + filter + '&val=' + value;
}
</script>

<table class="<%=class_table%>" cellspacing="0" cellpadding="0" width="100%">
   <% if (Request.Cookies("Auth") = "Owner" or Request.Cookies("Auth") = "Developer") and modText <> "" then %>
      <tr><td colspan="10">
             <table border="0" cellpadding="0" cellspacing="0">
             <tr><td class="mod"><img src="images/exclamation.png" border="0" align="absmiddle"><br><img src="images/information.png" border="0" align="absmiddle"></td><td class="mod"><%=modText%></td></tr>
             </table>
          </td>
      </tr>
      <!--
      <tr><td colspan="10" class="mod">&nbsp;</td></tr>
      -->
   <% end if %>
   
   <tr><th class="rrow" width="5%">ID</th><th class="rrow" width="10%">Last Updated</th><th class="rrow" width="8%"><%=arryFields(0)%></th><th class="rrow" width="10%"><%=arryFields(2)%></th><th class="rrow" width="35%"><%=arryFields(4)%></th><th class="rrow" width="12%">Opened By</th><th class="rrow" width="9%"><%=arryFields(15)%></th><th class="rrow" width="11%">Actions</th></tr>
   <% if RS.EOF = true then %>
   <tr><td colspan=10>&nbsp;There are no records, or none matching your selected filters.</td></tr>
   <% else %>
      <%
         idx = 0
         do while (RS.EOF <> true)
            'this will control the paging for MSSQL - stopgap, find a better way  
            if dbType = "MSSQL" then
                if startpoint > 0 then
                    while(idx < startpoint + 1)
                        'response.Write("skipping...<br />")
                        idx = idx + 1
                        RS.MoveNext
                    wend
                end if
            end if
            
            Response.Write("<tr bgcolor=" & bcolor & " class='text'><td class=" & chr(34))
            if clng(RS("ID")) = clng(RS("ParentID")) then
               response.write("rrow")
            else
                response.write("rrow_child")
            end if
            pos = InStr(1,RS("EnteredBy"),"@")
            'add a test to see if this fails in the event that user IDs aren't emails
            
            Response.write(chr(34) & "><a href=newrequest.asp?id=" & RS("ID") & ">" & RS("ID") & "</td><td width='100' style='font-size:9px;' class=" & chr(34) & "rrow" & chr(34) & ">" & RS("LastUpdated") & "</td><td class=" & chr(34) & "rrow" & chr(34) & ">" & RS("Status") & "</td><td class=" & chr(34) & "rrow" & chr(34) & ">" & RS("Product") & "</td><td width=300 class=" & chr(34) & "rrow" & chr(34) & "><a href=newrequest.asp?id=" & RS("ID") & ">" & RS("Summary") & "</a></td><td class=" & chr(34) & "rrow" & chr(34) & ">" & mid(RS("EnteredBy"),1,pos-1) & "</td><td class=" & chr(34) & "rrow" & chr(34) & ">&nbsp;" & RS("DueDate") & "</td><td class=" & chr(34) & "rrow" & chr(34) & "><a href=addwatch.asp?id=" & RS("ID") & "><img src=" & chr(34) & "themes/" & theme & "/images/watch.png" & chr(34) & " title=" & chr(34) & "Watch Request" & chr(34) & " height=" & chr(34) & "25" & chr(34) & " border=" & chr(34) & "0" & chr(34) & "></a>&nbsp;<a href=deletewatch.asp?id=" & RS("ID") & "><img src=" & chr(34) & "themes/" & theme &  "/images/watch_remove.png" & chr(34) & " height=" & chr(34) & "25" & chr(34) & " border=" & chr(34) & "0" & chr(34) & " title=" & chr(34) & "Delete Watch" & chr(34) & "></a></td></tr>" & chr(13) & chr(10))
            Response.Write("<tr class='color'><td valign='top' align='center'></td><td valign='top' align='center'></td><td valign='top' align='center'></td><td valign='top' align='center'></td><td valign='top' align='center'></td><td valign='top' align='center'></td><td valign='top' align='center' class='last'></td></tr>" & chr(13) & chr(10))
            '/*** make this configurable and driven by CSS ***/
            if bcolor = "white" then
               bcolor = "lightgoldenrodyellow"
            else
               bcolor = "white"
            end if
            RS.MoveNext
            idx = idx + 1
            'if the record has met or exceeded the max show, move on
            'response.Write maxShow+startpoint & "<br />"
            if dbType = "MSSQL" then
                if idx >= maxShow + startpoint then
                    exit do
                end if
            end if
         loop
      %>
   <% end if %>
</table>
<% if paging = true then%>
<div class="paging">
<%
count = idx

if startpoint > 0 then
   'we are not on page one, we need a "page back" option
   qstr = Request.QueryString
   
   'check for the start parameter in the Querystring.  if it's there, remove it
   pos = instr(Request.QueryString,"start")
   if pos > 0 then
      pos2 = instr(Request.QueryString,"&")
      qstr = mid(qstr,pos2)
   end if
   
   Response.Write(" <a href=" & chr(34) & "trackmain.asp?start=" & (clng(startpoint) - clng(maxShow)) & "&" & qstr & chr(34)& "> < previous </a> ")
end if

if startpoint = 0 then
   'we are on the first page
   Response.Write("Page 1 of ")
else 'generate the page number
   Response.Write("Page " & round(clng(startpoint) / clng(maxShow)) + 1 & " of ") 'this isn't *the* fix, but gets us closer...
end if

'how many pages?
totalpages = cstr(clng(reccount) / clng(maxShow))
   
'if the calculation was not a whole number, find the decimal and trim it off.
pos = instr(totalpages,".")
if pos > 0 then
   totalpages = left(totalpages,pos)
end if
   
'Response.Write(totalpages)
if clng(reccount) mod clng(maxShow) > 0 then
   totalpages = clng(totalpages) + 1
end if
Response.Write(totalpages)

if clng(startpoint) + clng(count) < clng(reccount) then
   'we are not at the end yet
   qstr = Request.QueryString
   
   'check for the start parameter in the Querystring.  if it's there, remove it
   pos = instr(Request.QueryString,"start")
   if pos > 0 then
      pos2 = instr(Request.QueryString,"&")
      qstr = mid(qstr,pos2)
   end if
   
   Response.Write(" <a href=" & chr(34) & "trackmain.asp?start=" & (clng(startpoint) + clng(count)) & "&" & qstr & chr(34)& "> next > </a> ")
end if
%>
</div>
<% end if %>
<form action="trackmain.asp" method="POST" name="hiddenvals">
   <input type="hidden" name="sortby" value="<%=sortby%>">
   <input type="hidden" name="ascdesc" value="<%=ascdesc%>">
   <input type="hidden" name="comp" value="<%=comp%>">
   <input type="hidden" name="reccount" value="<%=reccount %>">
</form>

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/footer.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/footer.html" -->
<% end if %>

<%
RS.Close
set RS = nothing
oConn.Close
set oConn = nothing
%>