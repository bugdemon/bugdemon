<%@ Language=VBScript %>
<!-- #include file="include.asp" -->

<%
'********************************************************************************************
'*   Copyright 2003-2008, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

%>
   <!-- #include file="inc_checklogin.asp" -->
<%
id = Request.Item("id")
uid = Request.Cookies("UID")

sql = "SELECT * FROM timetracking WHERE UserID=" & uid & " AND TaskID=" & id & " ORDER BY ID"

set oConn = Server.CreateObject("ADODB.Connection")
set oRS = Server.CreateObject("ADODB.Recordset")
oConn.Open strConnect
oRS.Open sql,oConn,1,1
totalmins = 0
%>
<HTML>
<HEAD><TITLE>Time Overview</TITLE>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script language="JavaScript">

</script>
<link rel="stylesheet" type="text/css" href="demon.css">
</HEAD>
<BODY>
<table border=0>
   <tr bgcolor=LightSteelBlue valign=middle><th valign=middle height="40"><b>&nbsp;<%=coName%>&nbsp;<%=appName%>&nbsp;&nbsp;</b></th><th>&nbsp;&nbsp;&nbsp;&nbsp;</th><th>&nbsp;&nbsp;&nbsp;</th><th>&nbsp;&nbsp;</th><th>&nbsp;</th><th width=300 align=right bgcolor=white><img src=bug.gif align=absmiddle border=0></th></tr>
</table>
<!-- #include file="inc_nav.asp" -->
<br>
<table border="0" class="reptab">
   <tr><th class="reptab">ID</th><th class="reptab">Task ID</th><th class="reptab">Begin Time</th><th class="reptab">End Time</th><th class="reptab">Manual Entry</th><th class="reptab">Total Time</th></tr>
   <% if oRS.EOF then%>
      <tr><td colspan="4">You do not have any time assigned to this task.</td></tr>
   <% end if %>
   
   <% while not oRS.EOF %>   
      <tr><td class="reptab"><%=oRS("ID")%></td><td class="reptab"><%=oRS("TaskID")%></td><td class="reptab"><%=oRS("BeginTime")%></td><td class="reptab"><%=oRS("EndTime")%></td><td class="reptab"><%=oRS("ManualEntry")%></td><td class="reptab"><%=oRS("TotalMins")%></td>
   <% 
      totalmins = totalmins + clng(oRS("TotalMins"))
      oRS.MoveNext
      wend
   %>
</table>
</BODY>
</HTML>

<%
oRS.Close
set oRS = nothing
oConn.Close
set oConn = nothing
%>