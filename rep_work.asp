<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003 - Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one licese per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
%>
<!-- #include file="inc_checklogin.asp" -->
<%

if Request.Cookies("Auth") = "Standard" then
   Response.Write("<font face=arial>This is only available to administrators</font>")
   Response.End
end if

sqlStr = "SELECT * FROM workinfo ORDER BY EnterDate DESC"
set oConn = Server.CreateObject("ADODB.Connection")
set oRS = server.CreateObject("ADODB.Recordset")

oConn.Open strConnect

set oRS = oConn.Execute(sqlStr)
%>

<html>
   <head><title>Work Information Report</title>
      <script language="JavaScript">
      </script>
   </head>
   
   <body>
   <font face=arial>
   <table border=1 cellspacing=0 cellpadding=0 bordercolor=lightsteelblue>
      <tr><td colspan=10 align=center width=100%><table border=0 cellspacing=0 cellpadding=0 bordercolor=lightsteelblue width=100%>
         <tr><td colspan=10 align=center bgcolor="lightsteelblue"><font style="font-size:12px;"><b>Work Progress Report</b></td></tr>
         <tr><td>&nbsp;<a href="reports.asp">Back</a><td colspan=9 align=right><font style="font-size:11px;"><a href="workinfo.asp">Add Entry</a>&nbsp;&nbsp;</td></tr>
      </table></td></tr>
      <tr><td><font style="font-size:11px;"><b>Date</b></td><td><font style="font-size:11px;"><b>User</b></td><td><font style="font-size:11px;"><b>Description</td><td><font style="font-size:11px;"><b>Product</b></td><td><font style="font-size:11px;"><b>Version</b></td></tr>
      <%
      while oRS.EOF <> true
         Response.Write("<tr><td>&nbsp;<font style=font-size:11px;>" & oRS("EnterDate") & "&nbsp;</td><td>&nbsp;<font style=font-size:11px;>" & oRS("EnteredBy") & "&nbsp;</td><td><font style=font-size:11px;>" & oRS("DevDesc") & "</td><td>&nbsp;<font style=font-size:11px;>" & oRS("Product") & "&nbsp;</td><td><font style=font-size:11px;>&nbsp;" & oRS("Version") & "</td></tr>")
         oRS.MoveNext
      wend
      %>
   </table>
   </font>
   </body>
</html>

<%
oRS.Close
set oRS = nothing
oConn.Close
set oConn = nothing
%>