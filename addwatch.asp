<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003 - Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one licese per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
pn = Request.Item("pn")

%>
<!-- #include file="inc_checklogin.asp" -->
<%

id = Request.Item("id")

set oConn = server.CreateObject("ADODB.Connection")
set oRS = server.CreateObject("ADODB.Recordset")

oConn.Open strConnect

'See if they are already watching this item
sqlStr = "SELECT * FROM chgnotify WHERE ID=" & id & " AND UserID='" & userid & "'"

set oRS = oConn.Execute(sqlStr)
If oRS.EOF <> true then
   Response.write("<font face=arial color=red>You are already watching this item.</font>")
   killObjects
   printLinks
   Response.End
end if

'Check to see if they are the owner of this item
sqlStr = "SELECT * FROM tobject WHERE ID=" & id
set oRS = oConn.Execute(sqlStr)

if oRS.EOF = true then
   Response.Write("<font face=arial color=red>Request " & id & " does not exist.</font>")
   killObjects
   printLinks
   Response.End
end if

if ucase(oRS("EnteredBy")) = ucase(userid) then
   Response.Write("<font face=arial color=red>You submitted this item and by default will be notified of the changes.</font>")
   killObjects
   printLinks
   Response.End
end if

oRS.Close
set oRS = nothing

if dbType = "MySQL" then
   timestamp = MySQLDate(now())
else
   timestamp = now()
end if

sqlStr = "INSERT INTO chgnotify(ID,UserID,DateAdded) VALUES(" & id & ",'" & userid & "'," & delim & timestamp & delim & ")"
oConn.Execute(sqlStr)

oConn.Close
set oConn = nothing

call writeLog(id, 6,"Added")

if pn = "" then
   Response.Redirect("trackmain.asp")
else
   Response.Redirect(pn & "?ID=" & id)
end if

%>

<%
sub killObjects
   set oRS = nothing
   set oConn = nothing
end sub

sub printLinks
   Response.write("<font face=arial><a href=trackmain.asp>Return to the Main Menu</a></font>")
end sub

%>