/*
Navicat MySQL Data Transfer

Source Server         : BugDemon_Dev
Source Server Version : 50532
Source Host           : db.bugdemon.com:3306
Source Database       : bugdemon_dev

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2014-01-02 16:46:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for addcomments
-- ----------------------------
DROP TABLE IF EXISTS `addcomments`;
CREATE TABLE `addcomments` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) DEFAULT NULL,
  `Comments` text,
  `DateAdded` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of addcomments
-- ----------------------------

-- ----------------------------
-- Table structure for changetypes
-- ----------------------------
DROP TABLE IF EXISTS `changetypes`;
CREATE TABLE `changetypes` (
  `ChangeTypeDesc` varchar(20) NOT NULL,
  `LongTypeDesc` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ChangeTypeDesc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of changetypes
-- ----------------------------
INSERT INTO `changetypes` VALUES ('Bug', 'Problem with Product');
INSERT INTO `changetypes` VALUES ('Documentation', 'Documentation');
INSERT INTO `changetypes` VALUES ('Enhancement', 'Request for new Functionality');
INSERT INTO `changetypes` VALUES ('Project', 'New Project Request');

-- ----------------------------
-- Table structure for chgnotify
-- ----------------------------
DROP TABLE IF EXISTS `chgnotify`;
CREATE TABLE `chgnotify` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) NOT NULL,
  `DateAdded` datetime DEFAULT NULL,
  `DevNotify` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of chgnotify
-- ----------------------------

-- ----------------------------
-- Table structure for fieldnames
-- ----------------------------
DROP TABLE IF EXISTS `fieldnames`;
CREATE TABLE `fieldnames` (
  `FieldNo` int(11) NOT NULL,
  `FieldDesc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`FieldNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of fieldnames
-- ----------------------------
INSERT INTO `fieldnames` VALUES ('0', 'Status');
INSERT INTO `fieldnames` VALUES ('1', 'Change Type');
INSERT INTO `fieldnames` VALUES ('2', 'Product');
INSERT INTO `fieldnames` VALUES ('3', 'Version');
INSERT INTO `fieldnames` VALUES ('4', 'Summary');
INSERT INTO `fieldnames` VALUES ('5', 'Description');
INSERT INTO `fieldnames` VALUES ('6', 'Comments');
INSERT INTO `fieldnames` VALUES ('7', 'Assigned To');
INSERT INTO `fieldnames` VALUES ('8', 'Weight Factor');
INSERT INTO `fieldnames` VALUES ('9', 'Time');
INSERT INTO `fieldnames` VALUES ('10', 'Completed By');
INSERT INTO `fieldnames` VALUES ('11', 'Fix Release');
INSERT INTO `fieldnames` VALUES ('12', 'Internal Comments');
INSERT INTO `fieldnames` VALUES ('13', 'Affected Modules');
INSERT INTO `fieldnames` VALUES ('14', 'Admin Comments<br>(public)');
INSERT INTO `fieldnames` VALUES ('15', 'Due Date');

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `fileid` bigint(20) NOT NULL AUTO_INCREMENT,
  `objectid` bigint(20) NOT NULL,
  `authlevel` varchar(25) NOT NULL,
  `timestamp` datetime NOT NULL,
  `userid` bigint(20) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`fileid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of files
-- ----------------------------

-- ----------------------------
-- Table structure for logins
-- ----------------------------
DROP TABLE IF EXISTS `logins`;
CREATE TABLE `logins` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) DEFAULT NULL,
  `LoginDate` datetime DEFAULT NULL,
  `LogoutDate` datetime DEFAULT NULL,
  `UserAgent` text,
  `IP` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `UserID` (`UserID`),
  KEY `UserID_LoginDate` (`UserID`,`LoginDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of logins
-- ----------------------------

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `ProductName` varchar(50) DEFAULT NULL,
  `SortOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'Bug Demon', '0');

-- ----------------------------
-- Table structure for rankings
-- ----------------------------
DROP TABLE IF EXISTS `rankings`;
CREATE TABLE `rankings` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) NOT NULL,
  `Rating` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rankings
-- ----------------------------

-- ----------------------------
-- Table structure for ratings
-- ----------------------------
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings` (
  `Rating` tinyint(4) NOT NULL,
  `RatingDesc` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`Rating`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ratings
-- ----------------------------

-- ----------------------------
-- Table structure for statuses
-- ----------------------------
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses` (
  `StatusID` tinyint(4) NOT NULL,
  `StatusDesc` varchar(15) DEFAULT NULL,
  `LongStatusDesc` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`StatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of statuses
-- ----------------------------
INSERT INTO `statuses` VALUES ('1', 'New', 'New Request');
INSERT INTO `statuses` VALUES ('2', 'Open', 'Request has been Reviewed and Accepted');
INSERT INTO `statuses` VALUES ('3', 'Hold', 'Request has been Reviewd and Accepted, but will not start');
INSERT INTO `statuses` VALUES ('5', 'More Info', 'More Information Required from the Submitting Party');
INSERT INTO `statuses` VALUES ('6', 'Assigned', 'Request Assigned to a Developer');
INSERT INTO `statuses` VALUES ('8', 'In Process', 'Request has been Started');
INSERT INTO `statuses` VALUES ('10', 'Duplicate', 'Similar Request Already Entered');
INSERT INTO `statuses` VALUES ('12', 'Complete', 'Request Completed');
INSERT INTO `statuses` VALUES ('14', 'Closed', 'Closed - See Comments');

-- ----------------------------
-- Table structure for timetracking
-- ----------------------------
DROP TABLE IF EXISTS `timetracking`;
CREATE TABLE `timetracking` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) NOT NULL,
  `TaskID` bigint(20) NOT NULL,
  `BeginTime` datetime NOT NULL,
  `EndTime` datetime DEFAULT NULL,
  `TotalMins` decimal(10,3) DEFAULT NULL,
  `ManualEntry` tinyint(4) NOT NULL DEFAULT '0',
  `Reconciled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `UserID` (`UserID`,`EndTime`,`TaskID`),
  KEY `TaskID` (`TaskID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of timetracking
-- ----------------------------

-- ----------------------------
-- Table structure for tobject
-- ----------------------------
DROP TABLE IF EXISTS `tobject`;
CREATE TABLE `tobject` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Summary` varchar(256) DEFAULT NULL,
  `Description` text,
  `DateEntered` datetime DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  `ChangeType` varchar(20) DEFAULT NULL,
  `Product` varchar(50) DEFAULT NULL,
  `DateClosed` datetime DEFAULT NULL,
  `Comments` text,
  `Commented` varchar(75) DEFAULT NULL,
  `EnteredBy` varchar(50) DEFAULT NULL,
  `AssignedTo` varchar(50) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastUpdatedBy` varchar(50) DEFAULT NULL,
  `InternalComments` text,
  `WeightingFactor` tinyint(4) DEFAULT NULL,
  `ChangeDuration` float DEFAULT NULL,
  `CompletedBy` varchar(50) DEFAULT NULL,
  `Version` varchar(25) DEFAULT NULL,
  `AffFiles` varchar(1000) DEFAULT NULL,
  `FixRelease` varchar(25) DEFAULT NULL,
  `DueDate` datetime DEFAULT NULL,
  `ParentID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tobject
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(50) DEFAULT NULL,
  `UserPass` varchar(30) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(75) DEFAULT NULL,
  `UserType` varchar(20) DEFAULT NULL,
  `DateAdded` datetime DEFAULT NULL,
  `HeartBeat` datetime DEFAULT NULL,
  `StartPage` varchar(30) NOT NULL DEFAULT 'trackmain.asp',
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'jesse@qbstech.com', '', 'Jesse', 'Quijano', 'Owner', '2004-05-17 00:00:00', '2012-10-03 08:03:28', 'trackmain.asp', '2009-08-26 16:43:34');

-- ----------------------------
-- Table structure for userstypes
-- ----------------------------
DROP TABLE IF EXISTS `userstypes`;
CREATE TABLE `userstypes` (
  `UserType` varchar(20) NOT NULL,
  `LongDesc` varchar(250) DEFAULT NULL,
  `UserVal` int(11) DEFAULT NULL,
  PRIMARY KEY (`UserType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of userstypes
-- ----------------------------
INSERT INTO `userstypes` VALUES ('Approver', 'Approves New Requests', '5');
INSERT INTO `userstypes` VALUES ('Developer', 'Responsible for Development Activities', '3');
INSERT INTO `userstypes` VALUES ('Owner', 'Site Owner or Admin', '0');
INSERT INTO `userstypes` VALUES ('Standard', 'Submit and View Details of Requests', '8');

-- ----------------------------
-- Table structure for workinfo
-- ----------------------------
DROP TABLE IF EXISTS `workinfo`;
CREATE TABLE `workinfo` (
  `EnterDate` datetime DEFAULT NULL,
  `EnteredBy` varchar(50) DEFAULT NULL,
  `Version` varchar(25) DEFAULT NULL,
  `DevDesc` text,
  `Product` varchar(20) DEFAULT NULL,
  `TimeSpent` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of workinfo
-- ----------------------------
