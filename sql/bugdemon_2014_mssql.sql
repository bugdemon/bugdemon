/*
 Navicat Premium Data Transfer

 Source Server         : CTWeb1
 Source Server Type    : SQL Server
 Source Server Version : 8000760
 Source Host           : ns2.coverttechnology.net
 Source Database       : bugdemon
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 8000760
 File Encoding         : utf-8

 Date: 02/24/2014 10:02:08 AM
*/

-- ----------------------------
--  Table structure for logins
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[logins]') AND type IN ('U'))
	DROP TABLE [dbo].[logins]
GO
CREATE TABLE [dbo].[logins] (
	[ID] bigint IDENTITY(1,1) NOT NULL,
	[UserID] bigint NULL,
	[LoginDate] datetime NULL,
	[LogoutDate] datetime NULL,
	[UserAgent] text COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IP] varchar(25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for tobject
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[tobject]') AND type IN ('U'))
	DROP TABLE [dbo].[tobject]
GO
CREATE TABLE [dbo].[tobject] (
	[ID] bigint IDENTITY(1,1) NOT NULL,
	[Summary] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] text COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DateEntered] datetime NULL,
	[Status] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ChangeType] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Product] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DateClosed] datetime NULL,
	[Comments] text COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Commented] varchar(75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EnteredBy] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AssignedTo] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastUpdated] datetime NULL,
	[LastUpdatedBy] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InternalComments] text COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WeightingFactor] tinyint NULL,
	[ChangeDuration] float(53) NULL,
	[CompletedBy] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Version] varchar(25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AffFiles] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FixRelease] varchar(25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DueDate] datetime NULL,
	[ParentID] bigint NULL,
	[Priority] tinyint NULL DEFAULT (1)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for products
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[products]') AND type IN ('U'))
	DROP TABLE [dbo].[products]
GO
CREATE TABLE [dbo].[products] (
	[ProductID] int IDENTITY(1,1) NOT NULL,
	[ProductName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SortOrder] int NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Records of products
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [dbo].[products] ON
GO
INSERT INTO [dbo].[products] ([ProductID], [ProductName], [SortOrder]) VALUES ('1', 'Bug Demon', '20');
GO
SET IDENTITY_INSERT [dbo].[products] OFF
GO
COMMIT
GO

-- ----------------------------
--  Table structure for rankings
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[rankings]') AND type IN ('U'))
	DROP TABLE [dbo].[rankings]
GO
CREATE TABLE [dbo].[rankings] (
	[ID] bigint IDENTITY(1,1) NOT NULL,
	[UserID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Rating] tinyint NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for statuses
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[statuses]') AND type IN ('U'))
	DROP TABLE [dbo].[statuses]
GO
CREATE TABLE [dbo].[statuses] (
	[StatusID] tinyint IDENTITY(1,1) NOT NULL,
	[StatusDesc] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LongStatusDesc] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Records of statuses
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [dbo].[statuses] ON
GO
INSERT INTO [dbo].[statuses] ([StatusID], [StatusDesc], [LongStatusDesc]) VALUES ('1', 'New', 'New Request');
INSERT INTO [dbo].[statuses] ([StatusID], [StatusDesc], [LongStatusDesc]) VALUES ('2', 'Open', 'Request has been Reviewed and Accepted');
INSERT INTO [dbo].[statuses] ([StatusID], [StatusDesc], [LongStatusDesc]) VALUES ('3', 'Hold', 'Request has been Reviewd and Accepted, but will not start');
INSERT INTO [dbo].[statuses] ([StatusID], [StatusDesc], [LongStatusDesc]) VALUES ('5', 'More Info', 'More Information Required from the Submitting Party');
INSERT INTO [dbo].[statuses] ([StatusID], [StatusDesc], [LongStatusDesc]) VALUES ('6', 'Assigned', 'Request Assigned to a Developer');
INSERT INTO [dbo].[statuses] ([StatusID], [StatusDesc], [LongStatusDesc]) VALUES ('8', 'In Process', 'Request has been Started');
INSERT INTO [dbo].[statuses] ([StatusID], [StatusDesc], [LongStatusDesc]) VALUES ('10', 'Duplicate', 'Similar Request Already Entered');
INSERT INTO [dbo].[statuses] ([StatusID], [StatusDesc], [LongStatusDesc]) VALUES ('12', 'Complete', 'Request Completed');
INSERT INTO [dbo].[statuses] ([StatusID], [StatusDesc], [LongStatusDesc]) VALUES ('14', 'Closed', 'Closed - See Comments');
GO
SET IDENTITY_INSERT [dbo].[statuses] OFF
GO
COMMIT
GO

-- ----------------------------
--  Table structure for timetracking
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[timetracking]') AND type IN ('U'))
	DROP TABLE [dbo].[timetracking]
GO
CREATE TABLE [dbo].[timetracking] (
	[ID] bigint IDENTITY(1,1) NOT NULL,
	[UserID] bigint NOT NULL,
	[TaskID] bigint NOT NULL,
	[BeginTime] datetime NOT NULL,
	[EndTime] datetime NULL,
	[TotalMins] decimal(10,3) NULL,
	[ManualEntry] tinyint NULL,
	[Reconciled] tinyint NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for userstypes
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[userstypes]') AND type IN ('U'))
	DROP TABLE [dbo].[userstypes]
GO
CREATE TABLE [dbo].[userstypes] (
	[UserType] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LongDesc] varchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UserVal] int IDENTITY(1,1) NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Records of userstypes
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [dbo].[userstypes] ON
GO
INSERT INTO [dbo].[userstypes] ([UserType], [LongDesc], [UserVal]) VALUES ('Owner', 'Site Owner or Admin', '0');
INSERT INTO [dbo].[userstypes] ([UserType], [LongDesc], [UserVal]) VALUES ('Developer', 'Responsible for Development Activities', '3');
INSERT INTO [dbo].[userstypes] ([UserType], [LongDesc], [UserVal]) VALUES ('Standard', 'Submit and View Details of Requests', '8');
GO
SET IDENTITY_INSERT [dbo].[userstypes] OFF
GO
COMMIT
GO

-- ----------------------------
--  Table structure for workinfo
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[workinfo]') AND type IN ('U'))
	DROP TABLE [dbo].[workinfo]
GO
CREATE TABLE [dbo].[workinfo] (
	[EnterDate] datetime NOT NULL,
	[EnteredBy] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Version] varchar(25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DevDesc] text COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Product] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TimeSpent] float(53) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for chgnotify
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[chgnotify]') AND type IN ('U'))
	DROP TABLE [dbo].[chgnotify]
GO
CREATE TABLE [dbo].[chgnotify] (
	[ID] bigint NOT NULL,
	[UserID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DateAdded] datetime NULL,
	[DevNotify] tinyint NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for users
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[users]') AND type IN ('U'))
	DROP TABLE [dbo].[users]
GO
CREATE TABLE [dbo].[users] (
	[ID] bigint IDENTITY(1,1) NOT NULL,
	[UserID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UserPass] varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FirstName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastName] varchar(75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UserType] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DateAdded] datetime NULL,
	[HeartBeat] datetime NULL,
	[StartPage] varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastUpdated] datetime NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Records of users
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [dbo].[users] ON
GO
INSERT INTO [dbo].[users] ([ID], [UserID], [UserPass], [FirstName], [LastName], [UserType], [DateAdded], [HeartBeat], [StartPage], [LastUpdated]) VALUES ('1', 'jesse@qbstech.com', 'abc123', 'Jesse', 'Quijano', 'Owner', '2004-05-17 00:00:00.000', '2014-02-24 08:24:20.000', 'trackmain.asp', '2013-08-18 22:11:45.000');
GO
SET IDENTITY_INSERT [dbo].[users] OFF
GO
COMMIT
GO

-- ----------------------------
--  Table structure for addcomments
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[addcomments]') AND type IN ('U'))
	DROP TABLE [dbo].[addcomments]
GO
CREATE TABLE [dbo].[addcomments] (
	[ID] bigint NOT NULL,
	[UserID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Comments] text COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DateAdded] datetime NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for files
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[files]') AND type IN ('U'))
	DROP TABLE [dbo].[files]
GO
CREATE TABLE [dbo].[files] (
	[fileid] bigint IDENTITY(1,1) NOT NULL,
	[objectid] bigint NOT NULL,
	[authlevel] varchar(25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[timestamp] datetime NOT NULL,
	[userid] bigint NOT NULL,
	[filename] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[active] tinyint NOT NULL DEFAULT (1)
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for priorities
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[priorities]') AND type IN ('U'))
	DROP TABLE [dbo].[priorities]
GO
CREATE TABLE [dbo].[priorities] (
	[priorityid] tinyint NOT NULL,
	[prioritydesc] varchar(25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[notifythreshold1] int NULL,
	[notifythreshold2] int NULL,
	[notifythreshold3] int NULL,
	[notifyannoy] int NULL,
	[escalationpath] tinyint NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Records of priorities
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[priorities] VALUES ('1', 'Normal', '1440', '2880', '4320', '60', '1');
INSERT INTO [dbo].[priorities] VALUES ('2', 'Low', '4320', '5760', '10080', '1440', '0');
INSERT INTO [dbo].[priorities] VALUES ('3', 'None', '0', '0', '0', '0', '99');
INSERT INTO [dbo].[priorities] VALUES ('4', 'High', '720', '1440', '2880', '60', '2');
INSERT INTO [dbo].[priorities] VALUES ('5', 'Urgent', '60', '360', '720', '30', '3');
INSERT INTO [dbo].[priorities] VALUES ('6', 'Critical', '15', '30', '45', '15', '4');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for history
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[history]') AND type IN ('U'))
	DROP TABLE [dbo].[history]
GO
CREATE TABLE [dbo].[history] (
	[historyid] bigint IDENTITY(1,1) NOT NULL,
	[objectid] bigint NOT NULL,
	[userid] int NOT NULL,
	[comment] text COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[activitytype] tinyint NOT NULL DEFAULT (1),
	[timestamp] datetime NOT NULL,
	[ipaddress] varchar(25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO
EXEC sp_addextendedproperty 'MS_Description', '1 = View, 2=Update, 3=Add, 4=Comment, 5 = File, 6=Notify, 8=WorkInfo', 'USER', 'dbo', 'TABLE', 'history', 'COLUMN', 'activitytype'
GO

-- ----------------------------
--  Table structure for changetypes
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[changetypes]') AND type IN ('U'))
	DROP TABLE [dbo].[changetypes]
GO
CREATE TABLE [dbo].[changetypes] (
	[ChangeTypeDesc] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LongTypeDesc] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Records of changetypes
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[changetypes] VALUES ('Bug', 'Problem with Product');
INSERT INTO [dbo].[changetypes] VALUES ('Documentation', 'Documentation');
INSERT INTO [dbo].[changetypes] VALUES ('Request', 'Request for new Functionality');
INSERT INTO [dbo].[changetypes] VALUES ('Project', 'New Project');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for fieldnames
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[fieldnames]') AND type IN ('U'))
	DROP TABLE [dbo].[fieldnames]
GO
CREATE TABLE [dbo].[fieldnames] (
	[FieldNo] int NOT NULL,
	[FieldDesc] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Records of fieldnames
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[fieldnames] VALUES ('0', 'Status');
INSERT INTO [dbo].[fieldnames] VALUES ('1', 'Task Type');
INSERT INTO [dbo].[fieldnames] VALUES ('2', 'Product');
INSERT INTO [dbo].[fieldnames] VALUES ('3', 'Version');
INSERT INTO [dbo].[fieldnames] VALUES ('4', 'Summary');
INSERT INTO [dbo].[fieldnames] VALUES ('5', 'Description');
INSERT INTO [dbo].[fieldnames] VALUES ('6', 'Comments');
INSERT INTO [dbo].[fieldnames] VALUES ('7', 'Assigned To');
INSERT INTO [dbo].[fieldnames] VALUES ('8', 'Weight Factor');
INSERT INTO [dbo].[fieldnames] VALUES ('9', 'Time');
INSERT INTO [dbo].[fieldnames] VALUES ('10', 'Completed By');
INSERT INTO [dbo].[fieldnames] VALUES ('11', 'Fix Release');
INSERT INTO [dbo].[fieldnames] VALUES ('12', 'Internal Comments');
INSERT INTO [dbo].[fieldnames] VALUES ('13', 'Affected Modules');
INSERT INTO [dbo].[fieldnames] VALUES ('14', 'Admin Comments<br>(public)');
INSERT INTO [dbo].[fieldnames] VALUES ('15', 'Due Date');
INSERT INTO [dbo].[fieldnames] VALUES ('16', 'Priority');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for ratings
-- ----------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[ratings]') AND type IN ('U'))
	DROP TABLE [dbo].[ratings]
GO
CREATE TABLE [dbo].[ratings] (
	[Rating] tinyint NOT NULL,
	[RatingDesc] varchar(75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO


-- ----------------------------
--  Primary key structure for table logins
-- ----------------------------
ALTER TABLE [dbo].[logins] ADD
	CONSTRAINT [PK_logins] PRIMARY KEY CLUSTERED ([ID]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Indexes structure for table logins
-- ----------------------------
CREATE NONCLUSTERED INDEX [_WA_Sys_LoginDate_22AA2996]
ON [dbo].[logins] ([LoginDate] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_LogoutDate_22AA2996]
ON [dbo].[logins] ([LogoutDate] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_UserID_22AA2996]
ON [dbo].[logins] ([UserID] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO

-- ----------------------------
--  Primary key structure for table tobject
-- ----------------------------
ALTER TABLE [dbo].[tobject] ADD
	CONSTRAINT [PK_tobject] PRIMARY KEY CLUSTERED ([ID]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Indexes structure for table tobject
-- ----------------------------
CREATE NONCLUSTERED INDEX [_WA_Sys_AssignedTo_239E4DCF]
ON [dbo].[tobject] ([AssignedTo] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_ChangeDuration_239E4DCF]
ON [dbo].[tobject] ([ChangeDuration] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_ChangeType_239E4DCF]
ON [dbo].[tobject] ([ChangeType] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_Commented_239E4DCF]
ON [dbo].[tobject] ([Commented] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_CompletedBy_239E4DCF]
ON [dbo].[tobject] ([CompletedBy] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_DateClosed_239E4DCF]
ON [dbo].[tobject] ([DateClosed] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_DateEntered_239E4DCF]
ON [dbo].[tobject] ([DateEntered] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_DueDate_239E4DCF]
ON [dbo].[tobject] ([DueDate] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_EnteredBy_239E4DCF]
ON [dbo].[tobject] ([EnteredBy] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_FixRelease_239E4DCF]
ON [dbo].[tobject] ([FixRelease] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_LastUpdated_239E4DCF]
ON [dbo].[tobject] ([LastUpdated] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_LastUpdatedBy_239E4DCF]
ON [dbo].[tobject] ([LastUpdatedBy] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_ParentID_239E4DCF]
ON [dbo].[tobject] ([ParentID] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_Priority_239E4DCF]
ON [dbo].[tobject] ([Priority] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_Product_239E4DCF]
ON [dbo].[tobject] ([Product] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_Status_239E4DCF]
ON [dbo].[tobject] ([Status] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_Summary_239E4DCF]
ON [dbo].[tobject] ([Summary] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_Version_239E4DCF]
ON [dbo].[tobject] ([Version] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_WeightingFactor_239E4DCF]
ON [dbo].[tobject] ([WeightingFactor] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO

-- ----------------------------
--  Primary key structure for table products
-- ----------------------------
ALTER TABLE [dbo].[products] ADD
	CONSTRAINT [PK_products] PRIMARY KEY CLUSTERED ([ProductID]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Indexes structure for table products
-- ----------------------------
CREATE NONCLUSTERED INDEX [_WA_Sys_ProductName_2C3393D0]
ON [dbo].[products] ([ProductName] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_SortOrder_2C3393D0]
ON [dbo].[products] ([SortOrder] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO

-- ----------------------------
--  Primary key structure for table rankings
-- ----------------------------
ALTER TABLE [dbo].[rankings] ADD
	CONSTRAINT [PK_rankings] PRIMARY KEY CLUSTERED ([ID]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table statuses
-- ----------------------------
ALTER TABLE [dbo].[statuses] ADD
	CONSTRAINT [PK_statuses] PRIMARY KEY CLUSTERED ([StatusID]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table timetracking
-- ----------------------------
ALTER TABLE [dbo].[timetracking] ADD
	CONSTRAINT [PK_timetracking] PRIMARY KEY CLUSTERED ([ID]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Indexes structure for table timetracking
-- ----------------------------
CREATE NONCLUSTERED INDEX [_WA_Sys_BeginTime_31EC6D26]
ON [dbo].[timetracking] ([BeginTime] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_EndTime_31EC6D26]
ON [dbo].[timetracking] ([EndTime] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_TaskID_31EC6D26]
ON [dbo].[timetracking] ([TaskID] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_UserID_31EC6D26]
ON [dbo].[timetracking] ([UserID] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO

-- ----------------------------
--  Primary key structure for table userstypes
-- ----------------------------
ALTER TABLE [dbo].[userstypes] ADD
	CONSTRAINT [PK_userstypes] PRIMARY KEY CLUSTERED ([UserVal]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table workinfo
-- ----------------------------
ALTER TABLE [dbo].[workinfo] ADD
	CONSTRAINT [PK_workinfo] PRIMARY KEY CLUSTERED ([EnterDate],[EnteredBy]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table chgnotify
-- ----------------------------
ALTER TABLE [dbo].[chgnotify] ADD
	CONSTRAINT [PK__chgnotify__4D94879B] PRIMARY KEY CLUSTERED ([ID],[UserID]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Indexes structure for table chgnotify
-- ----------------------------
CREATE NONCLUSTERED INDEX [_WA_Sys_DateAdded_3A81B327]
ON [dbo].[chgnotify] ([DateAdded] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_DevNotify_3A81B327]
ON [dbo].[chgnotify] ([DevNotify] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_ID_3A81B327]
ON [dbo].[chgnotify] ([ID] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_UserID_3A81B327]
ON [dbo].[chgnotify] ([UserID] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO

-- ----------------------------
--  Primary key structure for table users
-- ----------------------------
ALTER TABLE [dbo].[users] ADD
	CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED ([ID]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Indexes structure for table users
-- ----------------------------
CREATE NONCLUSTERED INDEX [_WA_Sys_DateAdded_3B75D760]
ON [dbo].[users] ([DateAdded] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_FirstName_3B75D760]
ON [dbo].[users] ([FirstName] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_HeartBeat_3B75D760]
ON [dbo].[users] ([HeartBeat] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_LastName_3B75D760]
ON [dbo].[users] ([LastName] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_LastUpdated_3B75D760]
ON [dbo].[users] ([LastUpdated] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_StartPage_3B75D760]
ON [dbo].[users] ([StartPage] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_UserID_3B75D760]
ON [dbo].[users] ([UserID] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_UserPass_3B75D760]
ON [dbo].[users] ([UserPass] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_UserType_3B75D760]
ON [dbo].[users] ([UserType] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO

-- ----------------------------
--  Indexes structure for table addcomments
-- ----------------------------
CREATE NONCLUSTERED INDEX [idx_id]
ON [dbo].[addcomments] ([ID] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO

-- ----------------------------
--  Primary key structure for table files
-- ----------------------------
ALTER TABLE [dbo].[files] ADD
	CONSTRAINT [PK__files__4316F928] PRIMARY KEY CLUSTERED ([fileid]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Indexes structure for table files
-- ----------------------------
CREATE NONCLUSTERED INDEX [_WA_Sys_active_4222D4EF]
ON [dbo].[files] ([active] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_objectid_4222D4EF]
ON [dbo].[files] ([objectid] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO

-- ----------------------------
--  Primary key structure for table priorities
-- ----------------------------
ALTER TABLE [dbo].[priorities] ADD
	CONSTRAINT [PK__priorities__46E78A0C] PRIMARY KEY CLUSTERED ([priorityid]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table history
-- ----------------------------
ALTER TABLE [dbo].[history] ADD
	CONSTRAINT [PK__history__4AB81AF0] PRIMARY KEY CLUSTERED ([historyid]) WITH FILLFACTOR = 90
	ON [default]
GO

-- ----------------------------
--  Indexes structure for table fieldnames
-- ----------------------------
CREATE NONCLUSTERED INDEX [_WA_Sys_FieldDesc_77BFCB91]
ON [dbo].[fieldnames] ([FieldDesc] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_WA_Sys_FieldNo_77BFCB91]
ON [dbo].[fieldnames] ([FieldNo] ASC)
WITH FILLFACTOR = 90
ON [PRIMARY]
GO

-- ----------------------------
--  Options for table logins
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[logins]', RESEED, 1)
GO

-- ----------------------------
--  Options for table tobject
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tobject]', RESEED, 1)
GO

-- ----------------------------
--  Options for table products
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[products]', RESEED, 17)
GO

-- ----------------------------
--  Options for table rankings
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[rankings]', RESEED, 1)
GO

-- ----------------------------
--  Options for table statuses
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[statuses]', RESEED, 14)
GO

-- ----------------------------
--  Options for table timetracking
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[timetracking]', RESEED, 1)
GO

-- ----------------------------
--  Options for table userstypes
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[userstypes]', RESEED, 8)
GO

-- ----------------------------
--  Options for table users
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[users]', RESEED, 30)
GO

-- ----------------------------
--  Options for table files
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[files]', RESEED, 1)
GO

-- ----------------------------
--  Options for table history
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[history]', RESEED, 1)
GO

