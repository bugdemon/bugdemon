/*
 Navicat MySQL Data Transfer

 Source Server         : Bugdemon
 Source Server Version : 50095
 Source Host           : bugdemon.com
 Source Database       : bugdemon_db

 Target Server Version : 50095
 File Encoding         : utf-8

 Date: 10/03/2012 09:30:03 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `addcomments`
-- ----------------------------
DROP TABLE IF EXISTS `addcomments`;
CREATE TABLE `addcomments` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) default NULL,
  `Comments` text,
  `DateAdded` datetime default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `changetypes`
-- ----------------------------
DROP TABLE IF EXISTS `changetypes`;
CREATE TABLE `changetypes` (
  `ChangeTypeDesc` varchar(20) NOT NULL,
  `LongTypeDesc` varchar(256) default NULL,
  PRIMARY KEY  (`ChangeTypeDesc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `changetypes`
-- ----------------------------
BEGIN;
INSERT INTO `changetypes` VALUES ('Bug', 'Problem with Product'), ('Documentation', 'Documentation'), ('Request', 'Request for new Functionality');
COMMIT;

-- ----------------------------
--  Table structure for `chgnotify`
-- ----------------------------
DROP TABLE IF EXISTS `chgnotify`;
CREATE TABLE `chgnotify` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) NOT NULL,
  `DateAdded` datetime default NULL,
  `DevNotify` tinyint(4) default '0',
  PRIMARY KEY  (`ID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `fieldnames`
-- ----------------------------
DROP TABLE IF EXISTS `fieldnames`;
CREATE TABLE `fieldnames` (
  `FieldNo` int(11) NOT NULL,
  `FieldDesc` varchar(50) default NULL,
  PRIMARY KEY  (`FieldNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `fieldnames`
-- ----------------------------
BEGIN;
INSERT INTO `fieldnames` VALUES ('0', 'Status'), ('1', 'Change Type'), ('2', 'Product'), ('3', 'Version'), ('4', 'Summary'), ('5', 'Description'), ('6', 'Comments'), ('7', 'Assigned To'), ('8', 'Weight Factor'), ('9', 'Time'), ('10', 'Completed By'), ('11', 'Fix Release'), ('12', 'Internal Comments'), ('13', 'Affected Modules'), ('14', 'Admin Comments<br>(public)');
COMMIT;

-- ----------------------------
--  Table structure for `logins`
-- ----------------------------
DROP TABLE IF EXISTS `logins`;
CREATE TABLE `logins` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` bigint(20) default NULL,
  `LoginDate` datetime default NULL,
  `LogoutDate` datetime default NULL,
  `UserAgent` text,
  `IP` varchar(25) default NULL,
  PRIMARY KEY  (`ID`),
  KEY `UserID` (`UserID`),
  KEY `UserID_LoginDate` (`UserID`,`LoginDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `ProductName` varchar(50) default NULL,
  `SortOrder` int(11) default NULL,
  PRIMARY KEY  (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `products`
-- ----------------------------
BEGIN;
INSERT INTO `products` VALUES ('1', 'Bug Demon', '0');
COMMIT;

-- ----------------------------
--  Table structure for `rankings`
-- ----------------------------
DROP TABLE IF EXISTS `rankings`;
CREATE TABLE `rankings` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) NOT NULL,
  `Rating` tinyint(4) default NULL,
  PRIMARY KEY  (`ID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `ratings`
-- ----------------------------
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings` (
  `Rating` tinyint(4) NOT NULL,
  `RatingDesc` varchar(75) default NULL,
  PRIMARY KEY  (`Rating`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `statuses`
-- ----------------------------
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses` (
  `StatusID` tinyint(4) NOT NULL,
  `StatusDesc` varchar(15) default NULL,
  `LongStatusDesc` varchar(256) default NULL,
  PRIMARY KEY  (`StatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `statuses`
-- ----------------------------
BEGIN;
INSERT INTO `statuses` VALUES ('1', 'New', 'New Request'), ('2', 'Open', 'Request has been Reviewed and Accepted'), ('3', 'Hold', 'Request has been Reviewd and Accepted, but will not start'), ('5', 'More Info', 'More Information Required from the Submitting Party'), ('6', 'Assigned', 'Request Assigned to a Developer'), ('8', 'In Process', 'Request has been Started'), ('10', 'Duplicate', 'Similar Request Already Entered'), ('12', 'Complete', 'Request Completed'), ('14', 'Closed', 'Closed - See Comments');
COMMIT;

-- ----------------------------
--  Table structure for `timetracking`
-- ----------------------------
DROP TABLE IF EXISTS `timetracking`;
CREATE TABLE `timetracking` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` bigint(20) NOT NULL,
  `TaskID` bigint(20) NOT NULL,
  `BeginTime` datetime NOT NULL,
  `EndTime` datetime default NULL,
  `TotalMins` decimal(10,3) default NULL,
  `ManualEntry` tinyint(4) NOT NULL default '0',
  `Reconciled` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`ID`),
  KEY `UserID` (`UserID`,`EndTime`,`TaskID`),
  KEY `TaskID` (`TaskID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `tobject`
-- ----------------------------
DROP TABLE IF EXISTS `tobject`;
CREATE TABLE `tobject` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Summary` varchar(256) default NULL,
  `Description` text,
  `DateEntered` datetime default NULL,
  `Status` varchar(15) default NULL,
  `ChangeType` varchar(20) default NULL,
  `Product` varchar(50) default NULL,
  `DateClosed` datetime default NULL,
  `Comments` text,
  `Commented` varchar(75) default NULL,
  `EnteredBy` varchar(50) default NULL,
  `AssignedTo` varchar(50) default NULL,
  `LastUpdated` datetime default NULL,
  `LastUpdatedBy` varchar(50) default NULL,
  `InternalComments` text,
  `WeightingFactor` tinyint(4) default NULL,
  `ChangeDuration` float default NULL,
  `CompletedBy` varchar(50) default NULL,
  `Version` varchar(25) default NULL,
  `AffFiles` varchar(1000) default NULL,
  `FixRelease` varchar(25) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` varchar(50) default NULL,
  `UserPass` varchar(30) default NULL,
  `FirstName` varchar(50) default NULL,
  `LastName` varchar(75) default NULL,
  `UserType` varchar(20) default NULL,
  `DateAdded` datetime default NULL,
  `HeartBeat` datetime default NULL,
  `StartPage` varchar(30) NOT NULL default 'trackmain.asp',
  `LastUpdated` datetime default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', 'jesse@qbstech.com', '', 'Jesse', 'Quijano', 'Owner', '2007-05-17 00:00:00', '2012-10-03 08:03:28', 'trackmain.asp', '2009-08-26 16:43:34');
COMMIT;

-- ----------------------------
--  Table structure for `userstypes`
-- ----------------------------
DROP TABLE IF EXISTS `userstypes`;
CREATE TABLE `userstypes` (
  `UserType` varchar(20) NOT NULL,
  `LongDesc` varchar(250) default NULL,
  `UserVal` int(11) default NULL,
  PRIMARY KEY  (`UserType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `userstypes`
-- ----------------------------
BEGIN;
INSERT INTO `userstypes` VALUES ('Developer', 'Responsible for Development Activities', '3'), ('Owner', 'Site Owner or Admin', '0'), ('Standard', 'Submit and View Details of Requests', '8');
COMMIT;

-- ----------------------------
--  Table structure for `workinfo`
-- ----------------------------
DROP TABLE IF EXISTS `workinfo`;
CREATE TABLE `workinfo` (
  `EnterDate` datetime default NULL,
  `EnteredBy` varchar(50) default NULL,
  `Version` varchar(25) default NULL,
  `DevDesc` text,
  `Product` varchar(20) default NULL,
  `TimeSpent` float default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
