/*
 Navicat MySQL Data Transfer

 Source Server         : ctsql1 - no VPN
 Source Server Version : 50018
 Source Host           : 8.14.136.243
 Source Database       : BugDemon_SimpleTourney

 Target Server Version : 50018
 File Encoding         : utf-8

 Date: 06/12/2012 10:21:17 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `addcomments`
-- ----------------------------
DROP TABLE IF EXISTS `addcomments`;
CREATE TABLE `addcomments` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) default NULL,
  `Comments` text,
  `DateAdded` datetime default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `addcomments`
-- ----------------------------
BEGIN;
INSERT INTO `addcomments` VALUES ('7', 'jesse@qbstech.com', 'Latitude and Longitude should be optional, not required.', '2010-02-16 17:28:32');
COMMIT;

-- ----------------------------
--  Table structure for `changetypes`
-- ----------------------------
DROP TABLE IF EXISTS `changetypes`;
CREATE TABLE `changetypes` (
  `ChangeTypeDesc` varchar(20) NOT NULL,
  `LongTypeDesc` varchar(256) default NULL,
  PRIMARY KEY  (`ChangeTypeDesc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `changetypes`
-- ----------------------------
BEGIN;
INSERT INTO `changetypes` VALUES ('Bug', 'Problem with Product'), ('Documentation', 'Documentation'), ('Request', 'Request for new Functionality');
COMMIT;

-- ----------------------------
--  Table structure for `chgnotify`
-- ----------------------------
DROP TABLE IF EXISTS `chgnotify`;
CREATE TABLE `chgnotify` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) NOT NULL,
  `DateAdded` datetime default NULL,
  `DevNotify` tinyint(4) default '0',
  PRIMARY KEY  (`ID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `chgnotify`
-- ----------------------------
BEGIN;
INSERT INTO `chgnotify` VALUES ('1', 'coty@latenight-solutions.com', '2010-02-16 17:19:04', '0'), ('1', 'jesse@qbstech.com', '2009-08-26 16:40:25', '0'), ('2', 'coty@latenight-solutions.com', '2010-02-16 17:00:33', '1'), ('2', 'jesse@qbstech.com', '2010-02-16 16:44:27', '0'), ('3', 'coty@latenight-solutions.com', '2010-02-16 16:57:46', '1'), ('3', 'jesse@qbstech.com', '2010-02-16 16:50:37', '0'), ('4', 'coty@latenight-solutions.com', '2010-02-16 17:01:23', '1'), ('4', 'jesse@qbstech.com', '2010-02-16 17:00:08', '0'), ('5', 'coty@latenight-solutions.com', '2010-02-16 17:03:53', '1'), ('5', 'jesse@qbstech.com', '2010-02-16 17:03:39', '0'), ('6', 'coty@latenight-solutions.com', '2010-02-16 17:26:08', '1'), ('6', 'jesse@qbstech.com', '2010-02-16 17:24:19', '0'), ('7', 'coty@latenight-solutions.com', '2010-02-16 17:29:40', '1'), ('7', 'jesse@qbstech.com', '2010-02-16 17:27:58', '0'), ('8', 'coty@latenight-solutions.com', '2010-02-17 13:54:09', '1'), ('8', 'jesse@qbstech.com', '2010-02-17 13:53:21', '0'), ('9', 'coty@latenight-solutions.com', '2010-02-17 14:13:04', '1'), ('9', 'jesse@qbstech.com', '2010-02-17 14:09:56', '0'), ('10', 'claytonvn@gmail.com', '2010-02-17 15:20:37', '1'), ('10', 'jesse@qbstech.com', '2010-02-17 15:20:05', '0'), ('11', 'claytonvn@gmail.com', '2010-02-17 15:23:27', '1'), ('11', 'jesse@qbstech.com', '2010-02-17 15:22:49', '0'), ('12', 'coty@latenight-solutions.com', '2010-02-24 16:06:35', '1'), ('12', 'jesse@qbstech.com', '2010-02-24 16:06:21', '0'), ('13', 'coty@latenight-solutions.com', '2010-02-25 09:42:51', '1'), ('13', 'jesse@qbstech.com', '2010-02-25 09:14:53', '0'), ('14', 'claytonvn@gmail.com', '2010-02-25 09:43:12', '1'), ('14', 'jesse@qbstech.com', '2010-02-25 09:42:33', '0'), ('15', 'coty@latenight-solutions.com', '2010-02-25 10:33:48', '1'), ('15', 'jesse@qbstech.com', '2010-02-25 10:33:31', '0'), ('16', 'coty@latenight-solutions.com', '2010-02-25 12:54:18', '1'), ('16', 'jesse@qbstech.com', '2010-02-25 12:53:41', '0'), ('17', 'coty@latenight-solutions.com', '2010-02-25 13:12:18', '1'), ('17', 'jesse@qbstech.com', '2010-02-25 13:09:35', '0'), ('18', 'coty@latenight-solutions.com', '2010-02-25 13:12:53', '1'), ('18', 'jesse@qbstech.com', '2010-02-25 13:12:06', '0'), ('19', 'coty@latenight-solutions.com', '2010-03-09 06:12:12', '1'), ('19', 'jesse@qbstech.com', '2010-03-09 06:11:58', '0'), ('20', 'coty@latenight-solutions.com', '2010-03-09 06:25:13', '1'), ('20', 'jesse@qbstech.com', '2010-03-09 06:25:02', '0');
COMMIT;

-- ----------------------------
--  Table structure for `fieldnames`
-- ----------------------------
DROP TABLE IF EXISTS `fieldnames`;
CREATE TABLE `fieldnames` (
  `FieldNo` int(11) NOT NULL,
  `FieldDesc` varchar(50) default NULL,
  PRIMARY KEY  (`FieldNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `fieldnames`
-- ----------------------------
BEGIN;
INSERT INTO `fieldnames` VALUES ('0', 'Status'), ('1', 'Change Type'), ('2', 'Product'), ('3', 'Version'), ('4', 'Summary'), ('5', 'Description'), ('6', 'Comments'), ('7', 'Assigned To'), ('8', 'Weight Factor'), ('9', 'Time'), ('10', 'Completed By'), ('11', 'Fix Release'), ('12', 'Internal Comments'), ('13', 'Affected Modules'), ('14', 'Admin Comments<br>(public)');
COMMIT;

-- ----------------------------
--  Table structure for `logins`
-- ----------------------------
DROP TABLE IF EXISTS `logins`;
CREATE TABLE `logins` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` bigint(20) default NULL,
  `LoginDate` datetime default NULL,
  `LogoutDate` datetime default NULL,
  `UserAgent` text,
  `IP` varchar(25) default NULL,
  PRIMARY KEY  (`ID`),
  KEY `UserID` (`UserID`),
  KEY `UserID_LoginDate` (`UserID`,`LoginDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `logins`
-- ----------------------------
BEGIN;
INSERT INTO `logins` VALUES ('1', '2', '2009-08-26 16:36:10', '2009-08-26 16:42:45', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-us) AppleWebKit/531.9 (KHTML, like Gecko) Version/4.0.3 Safari/531.9', '76.90.160.86'), ('2', '2', '2009-08-26 16:43:20', '2009-08-26 16:43:51', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-us) AppleWebKit/531.9 (KHTML, like Gecko) Version/4.0.3 Safari/531.9', '76.90.160.86'), ('3', '2', '2009-08-26 16:56:15', '2009-08-26 16:58:43', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-us) AppleWebKit/531.9 (KHTML, like Gecko) Version/4.0.3 Safari/531.9', '76.90.160.86'), ('4', '2', '2009-08-26 16:59:02', '2009-08-26 16:59:23', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-us) AppleWebKit/531.9 (KHTML, like Gecko) Version/4.0.3 Safari/531.9', '76.90.160.86'), ('5', '2', '2009-08-26 16:59:40', '2009-08-26 17:00:35', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-us) AppleWebKit/531.9 (KHTML, like Gecko) Version/4.0.3 Safari/531.9', '76.90.160.86'), ('6', '2', '2009-08-26 17:02:11', '2009-08-26 17:05:39', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-us) AppleWebKit/531.9 (KHTML, like Gecko) Version/4.0.3 Safari/531.9', '76.90.160.86'), ('7', '2', '2009-08-26 17:06:20', '2009-08-26 18:10:05', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-us) AppleWebKit/531.9 (KHTML, like Gecko) Version/4.0.3 Safari/531.9', '76.90.160.86'), ('8', '2', '2009-08-27 14:13:57', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-us) AppleWebKit/531.9 (KHTML, like Gecko) Version/4.0.3 Safari/531.9', '76.90.160.86'), ('9', '1', '2010-02-16 13:26:20', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10', '76.90.162.23'), ('10', '3', '2010-02-16 17:11:29', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7 (.NET CLR 3.5.30729)', '99.73.106.7'), ('11', '1', '2010-02-17 08:56:47', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10', '69.235.89.104'), ('12', '3', '2010-02-17 11:01:11', '2010-02-17 11:57:13', 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7 (.NET CLR 3.5.30729)', '99.73.106.7'), ('13', '3', '2010-02-17 12:02:25', '2010-02-17 13:02:00', 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7 (.NET CLR 3.5.30729)', '99.73.106.7'), ('14', '3', '2010-02-17 13:10:13', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7 (.NET CLR 3.5.30729)', '99.73.106.7'), ('15', '3', '2010-02-18 12:52:03', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7 (.NET CLR 3.5.30729)', '99.73.106.7'), ('16', '3', '2010-02-19 09:42:29', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7 (.NET CLR 3.5.30729)', '99.73.106.7'), ('17', '1', '2010-02-19 10:07:10', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10', '76.90.162.23'), ('18', '1', '2010-02-22 09:46:03', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10', '76.90.162.23'), ('19', '3', '2010-02-23 13:45:13', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '75.57.221.8'), ('20', '3', '2010-02-24 15:27:54', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '75.57.221.8'), ('21', '1', '2010-02-24 16:05:30', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10', '76.90.162.23'), ('22', '1', '2010-02-25 09:11:39', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10', '76.90.162.23'), ('23', '3', '2010-02-25 09:36:37', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '75.57.221.8'), ('24', '3', '2010-02-26 10:11:58', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '98.227.70.241'), ('25', '1', '2010-02-26 13:28:41', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10', '76.90.162.23'), ('26', '3', '2010-02-28 12:41:56', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '75.57.221.8'), ('27', '1', '2010-03-01 08:13:12', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10', '76.90.162.23'), ('28', '3', '2010-03-02 16:30:05', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '75.57.212.93'), ('29', '3', '2010-03-03 17:19:51', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '75.57.212.93'), ('30', '3', '2010-03-05 12:38:32', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '75.57.212.93'), ('31', '3', '2010-03-06 18:18:26', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '75.57.212.93'), ('32', '3', '2010-03-07 14:24:16', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '75.57.212.93'), ('33', '1', '2010-03-09 06:11:00', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10', '76.90.162.23'), ('34', '3', '2010-03-09 09:30:39', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '75.57.212.93'), ('35', '3', '2010-03-10 14:04:22', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 (.NET CLR 3.5.30729)', '75.57.212.93'), ('36', '1', '2010-03-10 14:56:55', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10', '76.90.162.23'), ('37', '1', '2010-04-02 19:24:05', null, 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-us) AppleWebKit/531.22.7 (KHTML, like Gecko) Version/4.0.5 Safari/531.22.7', '76.90.162.66'), ('38', '3', '2010-05-11 01:05:37', null, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 ( .NET CLR 3.5.30729)', '99.66.253.202'), ('39', '1', '2012-05-21 21:54:58', null, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', '76.90.41.185');
COMMIT;

-- ----------------------------
--  Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `ProductName` varchar(50) default NULL,
  `SortOrder` int(11) default NULL,
  PRIMARY KEY  (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `products`
-- ----------------------------
BEGIN;
INSERT INTO `products` VALUES ('1', 'Simple Tourney', '0');
COMMIT;

-- ----------------------------
--  Table structure for `rankings`
-- ----------------------------
DROP TABLE IF EXISTS `rankings`;
CREATE TABLE `rankings` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) NOT NULL,
  `Rating` tinyint(4) default NULL,
  PRIMARY KEY  (`ID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `ratings`
-- ----------------------------
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings` (
  `Rating` tinyint(4) NOT NULL,
  `RatingDesc` varchar(75) default NULL,
  PRIMARY KEY  (`Rating`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `statuses`
-- ----------------------------
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses` (
  `StatusID` tinyint(4) NOT NULL,
  `StatusDesc` varchar(15) default NULL,
  `LongStatusDesc` varchar(256) default NULL,
  PRIMARY KEY  (`StatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `statuses`
-- ----------------------------
BEGIN;
INSERT INTO `statuses` VALUES ('1', 'New', 'New Request'), ('2', 'Open', 'Request has been Reviewed and Accepted'), ('3', 'Hold', 'Request has been Reviewd and Accepted, but will not start'), ('5', 'More Info', 'More Information Required from the Submitting Party'), ('6', 'Assigned', 'Request Assigned to a Developer'), ('8', 'In Process', 'Request has been Started'), ('10', 'Duplicate', 'Similar Request Already Entered'), ('12', 'Complete', 'Request Completed'), ('14', 'Closed', 'Closed - See Comments');
COMMIT;

-- ----------------------------
--  Table structure for `timetracking`
-- ----------------------------
DROP TABLE IF EXISTS `timetracking`;
CREATE TABLE `timetracking` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` bigint(20) NOT NULL,
  `TaskID` bigint(20) NOT NULL,
  `BeginTime` datetime NOT NULL,
  `EndTime` datetime default NULL,
  `TotalMins` decimal(10,3) default NULL,
  `ManualEntry` tinyint(4) NOT NULL default '0',
  `Reconciled` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`ID`),
  KEY `UserID` (`UserID`,`EndTime`,`TaskID`),
  KEY `TaskID` (`TaskID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `timetracking`
-- ----------------------------
BEGIN;
INSERT INTO `timetracking` VALUES ('1', '3', '1', '2010-02-17 11:10:09', '2010-02-17 12:02:34', '52.000', '0', '0'), ('2', '3', '1', '2010-02-17 12:03:50', '2010-02-17 12:04:09', '1.000', '0', '0'), ('3', '3', '1', '2010-02-17 12:29:58', '2010-02-17 12:41:00', '12.000', '0', '0'), ('4', '3', '2', '2010-02-17 12:44:07', '2010-02-17 12:49:56', '5.000', '0', '0'), ('5', '3', '4', '2010-02-17 12:51:01', '2010-02-17 12:51:28', '0.000', '0', '0'), ('6', '3', '5', '2010-02-17 12:53:30', '2010-02-17 12:56:36', '3.000', '0', '0'), ('7', '3', '8', '2010-02-19 09:42:54', '2010-02-19 10:58:56', '76.000', '0', '0'), ('8', '3', '8', '2010-02-19 11:17:33', '2010-02-19 11:41:00', '24.000', '0', '0'), ('9', '3', '9', '2010-02-24 15:28:06', '2010-02-24 15:49:30', '21.000', '0', '0'), ('10', '3', '7', '2010-02-24 15:51:16', '2010-02-24 16:00:37', '9.000', '0', '0'), ('11', '3', '8', '2010-02-24 16:01:09', '2010-02-24 16:01:12', '0.000', '0', '0'), ('12', '3', '6', '2010-02-24 16:03:37', '2010-02-24 16:31:04', '28.000', '0', '0'), ('13', '3', '3', '2010-02-24 16:33:05', '2010-02-24 17:05:05', '32.000', '0', '0'), ('14', '3', '16', '2010-03-02 16:30:16', '2010-03-02 16:49:30', '19.000', '0', '0'), ('15', '3', '16', '2010-03-02 16:49:33', '2010-03-02 17:29:44', '40.000', '0', '0'), ('16', '3', '18', '2010-03-02 17:32:24', '2010-03-02 17:43:34', '11.000', '0', '0'), ('17', '3', '13', '2010-03-03 17:23:10', '2010-03-03 18:34:32', '71.000', '0', '0'), ('18', '3', '15', '2010-03-03 18:17:15', '2010-03-03 18:34:43', '17.000', '0', '0'), ('19', '3', '13', '2010-03-05 12:39:32', '2010-03-05 14:47:24', '128.000', '0', '0'), ('20', '3', '13', '2010-03-06 18:18:31', '2010-03-06 18:42:25', '24.000', '0', '0'), ('21', '3', '12', '2010-03-07 14:24:31', '2010-03-07 15:22:51', '58.000', '0', '0'), ('22', '3', '15', '2010-03-07 15:27:54', '2010-03-07 15:54:05', '27.000', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `tobject`
-- ----------------------------
DROP TABLE IF EXISTS `tobject`;
CREATE TABLE `tobject` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Summary` varchar(256) default NULL,
  `Description` text,
  `DateEntered` datetime default NULL,
  `Status` varchar(15) default NULL,
  `ChangeType` varchar(20) default NULL,
  `Product` varchar(50) default NULL,
  `DateClosed` datetime default NULL,
  `Comments` text,
  `Commented` varchar(75) default NULL,
  `EnteredBy` varchar(50) default NULL,
  `AssignedTo` varchar(50) default NULL,
  `LastUpdated` datetime default NULL,
  `LastUpdatedBy` varchar(50) default NULL,
  `InternalComments` text,
  `WeightingFactor` tinyint(4) default NULL,
  `ChangeDuration` float default NULL,
  `CompletedBy` varchar(50) default NULL,
  `Version` varchar(25) default NULL,
  `AffFiles` varchar(1000) default NULL,
  `FixRelease` varchar(25) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tobject`
-- ----------------------------
BEGIN;
INSERT INTO `tobject` VALUES ('1', 'Resend Validation Code', 'Need a system / page that allows people to resend their validation code.', '2010-02-16 16:34:12', 'Complete', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-02-17 12:43:37', 'coty@latenight-solutions.com', '', '0', '1.1', 'coty@latenight-solutions.com', 'Beta', 'revalidate.php', ''), ('2', 'Complete Functionality for Adding a New Tournament', 'Complete Functionality for Adding a New Tournament', '2010-02-16 16:44:27', 'Complete', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-02-17 12:57:55', 'coty@latenight-solutions.com', '', '0', '0.1', 'coty@latenight-solutions.com', 'Beta', null, ''), ('3', 'Allow ability to publish schedules.', 'During the setup process of a tournament, the administrator will be working on putting together the schedule and it may be incomplete, or require additional work.  There should be a flag that allows the admin to set the schedule to \"offline\" while being worked on, and put back online when the admin is ready.', '2010-02-16 16:50:37', 'Complete', 'Request', 'Simple Tourney', null, 'The publish feature should be available in the admin cockpit in the right hand menu.', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-02-24 17:05:25', 'coty@latenight-solutions.com', '<font color=blue>2/24/2010 5:05:25 PM - Coty Lake</font><br><p><font color=blue>2/16/2010 4:57:46 PM - Jesse Quijano</font><br>You will need to add a new field in the \"events\" table.  The field should be a tinyint and be called \"published\".  The default should be 0.  The script should be accessed from the event detail menu and update the field to be 1 or 0 depending on the published status.<p><p>', '0', '0.5', 'coty@latenight-solutions.com', 'Beta', 'publish.php', ''), ('4', 'Link Bug Reporting to Admin Cockpit', 'Update the link in the admin cockpit to point to http://bugz.tech-pig.com', '2010-02-16 17:00:08', 'Complete', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-02-17 12:51:43', 'coty@latenight-solutions.com', '', '0', '0.1', 'coty@latenight-solutions.com', 'Beta', null, ''), ('5', 'Fix Formatting for Entering Cards', 'The formatting for entering cards is too spread out.  It should be more compact.', '2010-02-16 17:03:39', 'Complete', 'Bug', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-02-17 12:56:56', 'coty@latenight-solutions.com', '', '0', '0.03', 'coty@latenight-solutions.com', 'Beta', null, ''), ('6', 'Venue Should Be Event Specific', 'Currently the venue is not event specific, but should be.', '2010-02-16 17:24:19', 'Complete', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-02-24 16:31:33', 'coty@latenight-solutions.com', '<font color=blue>2/24/2010 4:31:33 PM - Coty Lake</font><br><p><font color=blue>2/16/2010 5:26:08 PM - Jesse Quijano</font><br>Need to add a field to the \"venues\" table.  The field should be a bigint and be named \"eventid\".  Default value should be 0.  Insert the event id into the field based on $_COOKIE[\"eid\"]<p><p>', '0', '0.5', 'coty@latenight-solutions.com', '', 'newVenue.php', ''), ('7', 'Allow Latitude and Longitude for Venues', 'There are currently fields for lat. and long., but the data is not saved.', '2010-02-16 17:27:58', 'Complete', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-02-24 16:00:58', 'coty@latenight-solutions.com', '<font color=blue>2/24/2010 4:00:58 PM - Coty Lake</font><br><p><font color=blue>2/16/2010 5:29:40 PM - Jesse Quijano</font><br>need to add two fields to the venues table:  latitude and longitude.  Allow NULL because the values are not required.<p><p>', '0', '0.1', 'coty@latenight-solutions.com', '', null, ''), ('8', 'Fix email headers for mail sending functions', 'Validation emails are not making it to their recipients.  I believe this is due to improper mail headers.  Add From, To, and Content Type to the existing mail functions.', '2010-02-17 13:53:21', 'Complete', 'Bug', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-02-24 16:01:29', 'coty@latenight-solutions.com', '<font color=blue>2/24/2010 4:01:29 PM - Coty Lake</font><br><p><font color=blue>2/24/2010 3:50:08 PM - Coty Lake</font><br><p><font color=blue>2/17/2010 1:54:09 PM - Jesse Quijano</font><br>Here is a sample function:\r\nfunction sendMail($sTo,$sFrom,$sCC,$sBCC,$sSubject,$sBody,$bHTML)\r\n{\r\n    $sHeaders=\"\";\r\n    if($bHTML==1)\r\n    {\r\n        /* To send HTML mail, you can set the Content-type header. */\r\n        $sHeaders .= \"MIME-Version: 1.0\r\n\";\r\n        $sHeaders .= \"Content-type: text/html; charset=iso-8859-1\r\n\";\r\n    }\r\n\r\n    $sHeaders .= \"To: $sTo\r\n\";\r\n    $sHeaders .= \"From: $sFrom\r\n\";\r\n    if(strlen($sCC)>0) $sHeaders .= \"Cc: $sCC\r\n\";\r\n    if(strlen($sBCC)>0) $sHeaders .= \"Bcc: $sCCC\r\n\";\r\n\r\n    mail($sTo, $sSubject, $sBody, $sHeaders);\r\n}\r\n<p><p><p>', '0', '1.7', 'coty@latenight-solutions.com', 'Beta', 'validate.php, revalidate.php', ''), ('9', 'Allow selection of theme for each event', 'Each event will have a \"public\" face.  We have several themes available which will be located in the themes table.', '2010-02-17 14:09:56', 'Complete', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-02-24 15:49:55', 'coty@latenight-solutions.com', '<font color=blue>2/24/2010 3:49:55 PM - Coty Lake</font><br><p><font color=blue>2/17/2010 2:13:04 PM - Jesse Quijano</font><br>Expand the events table to include an integer (int) field called \"theme\".  This will correspond to the id in the themes table.  Default to theme 1.  The theme field should be a drop down list of \"name\" from the themes table.<p><p>', '0', '0.2', 'coty@latenight-solutions.com', 'Beta', 'eventadd.php, eventedit.php', ''), ('10', 'Need to add login functionality to the main landing', 'There is no easy way to log into the admin cockpit. We need an easily visible login located on the main home page.', '2010-02-17 15:20:05', 'Assigned', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'claytonvn@gmail.com', '2010-02-17 15:20:37', 'jesse@qbstech.com', '', '0', '0', '', 'Beta', null, ''), ('11', 'Change \"FREE\" on the home page', 'The tab is not entirely clear so I would like to change it to \"Sign-Up Free\"', '2010-02-17 15:22:49', 'Assigned', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'claytonvn@gmail.com', '2010-02-17 15:23:27', 'jesse@qbstech.com', '', '0', '0', '', 'Beta', null, ''), ('12', 'Update Preferences', 'Create section in admin area where user can modify their password, name, address, email address, etc.\r\n\r\ndata spans two tables:  users and users_data', '2010-02-24 16:06:21', 'Complete', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-03-07 15:23:02', 'coty@latenight-solutions.com', '', '0', '1', 'coty@latenight-solutions.com', '', null, ''), ('13', 'Allow Updating an Event (Tournament)', 'functionality should basically pull the information from the events table and allow changes and saving of the data.', '2010-02-25 09:14:53', 'Complete', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-03-06 18:42:51', 'coty@latenight-solutions.com', '', '0', '3.7', 'coty@latenight-solutions.com', 'Beta', null, ''), ('14', 'Increase Font Sizes', 'Font sizes for the Validation and Sign-up are too small.  Please increase.', '2010-02-25 09:42:33', 'Assigned', 'Bug', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'claytonvn@gmail.com', '2010-02-25 09:43:12', 'jesse@qbstech.com', '', '0', '0', '', 'Beta', null, ''), ('15', 'Close Account Functionality', 'Create ability to close account: when clicking on the initial link, you should be prompted to confirm your intention to close your account.  If yes, set the status in the database to 2.', '2010-02-25 10:33:31', 'Complete', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-03-07 15:54:18', 'coty@latenight-solutions.com', '', '0', '0.7', 'coty@latenight-solutions.com', 'Beta', null, ''), ('16', 'Cannot Insert Venue without Lat/Long.', 'We either need address, OR latitude and longitude.  If address is specified and not lat/long, then insert 0 into both latitude/longitude fields.\r\n\r\nRight now, an error is thrown stating that lat/long cannot be NULL.', '2010-02-25 12:53:41', 'Complete', 'Bug', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-03-02 17:30:01', 'coty@latenight-solutions.com', '', '0', '1', 'coty@latenight-solutions.com', 'Beta', null, ''), ('17', 'Add Edit Venue functionality', 'When clicking on the Edit Venues link, you should be presented with a listing of all venues for the Event.  Next to each, there should be an edit and delete link.  Use javascript to prompt for confirmation of delete.  Edit should allow you to update name, address, lattitude, and longitude.', '2010-02-25 13:09:35', 'Assigned', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-02-25 13:12:18', 'jesse@qbstech.com', '', '0', '0', '', '', null, ''), ('18', 'Add administrative fields to Venues table', 'Add a bigint field to Venues called `addedby`, and a datetime field called `dateadded`.  Both should be automatically populated during venue creation.  These should not be updatable during editing of the venue.', '2010-02-25 13:12:06', 'Complete', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-03-02 17:43:52', 'coty@latenight-solutions.com', '', '0', '0.2', 'coty@latenight-solutions.com', '', null, ''), ('19', 'Password Reset', 'Need functionality to be able to reset a forgotten password.', '2010-03-09 06:11:58', 'Assigned', 'Request', 'Simple Tourney', null, '', null, 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-03-09 06:12:12', 'jesse@qbstech.com', '', '0', '0', '', 'Beta', null, ''), ('20', 'Review Applications Report', 'Report should pull data from the applications table and include:  Age Group, Gender, Team Name, Club Name, Play Level, Requested Level, Payment Status, Payment Type, Application Status.\r\n\r\nFrom generated listing, you should be able to click on the line item and get full details for the application.  Details should be updatable for the admin.\r\n\r\nAside from the ability to update, there should be another button for \"Final Acceptance\".  This will update the status field in applications to 2.  It should also insert the appropriate data into the teams table.  Before doing this, an intermediate screen should prompt the admin to assign an agegroup, gender, and flight and pool.  Gender and agegroup should default to what is requested, but allow the admin to override to a different value if necessary.  Flight should just be a numeric value 1-10.  Pool should be numeric 1-25.  Once the data is updated, applications and teams should be updated.', '2010-03-09 06:25:02', 'Complete', 'Request', 'Simple Tourney', null, 'Done.', '<font color=blue>4/2/2010 7:25:06 PM - Jesse Quijano</font><br>', 'jesse@qbstech.com', 'coty@latenight-solutions.com', '2010-04-02 19:25:06', 'jesse@qbstech.com', '', '0', '6', 'jesse@qbstech.com', '', null, '');
COMMIT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` varchar(50) default NULL,
  `UserPass` varchar(30) default NULL,
  `FirstName` varchar(50) default NULL,
  `LastName` varchar(75) default NULL,
  `UserType` varchar(20) default NULL,
  `DateAdded` datetime default NULL,
  `HeartBeat` datetime default NULL,
  `StartPage` varchar(30) NOT NULL default 'trackmain.asp',
  `LastUpdated` datetime default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', 'jesse@qbstech.com', 'karaq1', 'Jesse', 'Quijano', 'Owner', '2007-05-17 00:00:00', '2012-05-21 21:55:16', 'trackmain.asp', '2009-08-26 16:43:34'), ('2', 'public@quijano.net', 'karaq1', 'John Q', 'Public', 'Standard', '2007-05-24 15:22:00', null, 'trackmain.asp', null), ('3', 'coty@latenight-solutions.com', 'm3t4lguy', 'Coty', 'Lake', 'Developer', '2008-01-23 00:00:00', '2010-05-11 01:05:49', 'trackmain.asp', null), ('4', 'claytonvn@gmail.com', 'cl2103', 'Clayton', 'Van Niekerk', 'Developer', '2010-02-16 13:13:00', null, 'trackmain.asp', null), ('11', 'order female sexual oil', 'QxXM93Js', 'order female sexual oil', 'order female sexual oil', 'Standard', '2010-03-19 07:50:17', null, 'trackmain.asp', null);
COMMIT;

-- ----------------------------
--  Table structure for `userstypes`
-- ----------------------------
DROP TABLE IF EXISTS `userstypes`;
CREATE TABLE `userstypes` (
  `UserType` varchar(20) NOT NULL,
  `LongDesc` varchar(250) default NULL,
  `UserVal` int(11) default NULL,
  PRIMARY KEY  (`UserType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `userstypes`
-- ----------------------------
BEGIN;
INSERT INTO `userstypes` VALUES ('Developer', 'Responsible for Development Activities', '3'), ('Owner', 'Site Owner or Admin', '0'), ('Standard', 'Submit and View Details of Requests', '8');
COMMIT;

-- ----------------------------
--  Table structure for `workinfo`
-- ----------------------------
DROP TABLE IF EXISTS `workinfo`;
CREATE TABLE `workinfo` (
  `EnterDate` datetime default NULL,
  `EnteredBy` varchar(50) default NULL,
  `Version` varchar(25) default NULL,
  `DevDesc` text,
  `Product` varchar(20) default NULL,
  `TimeSpent` float default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `workinfo`
-- ----------------------------
BEGIN;
INSERT INTO `workinfo` VALUES ('2010-02-17 12:43:23', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=1>1</a><br>&nbsp;&nbsp;Resend Validation Code', 'Simple Tourney', '1'), ('2010-02-17 12:43:37', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=1>1</a><br>&nbsp;&nbsp;Resend Validation Code', 'Simple Tourney', '1.1'), ('2010-02-17 12:50:26', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=2>2</a><br>&nbsp;&nbsp;Complete Functionality for Adding a New Tournament', 'Simple Tourney', '0.1'), ('2010-02-17 12:51:43', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=4>4</a><br>&nbsp;&nbsp;Link Bug Reporting to Admin Cockpit', 'Simple Tourney', '0.1'), ('2010-02-17 12:56:56', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=5>5</a><br>&nbsp;&nbsp;Fix Formatting for Entering Cards', 'Simple Tourney', '0.03'), ('2010-02-17 12:57:21', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=2>2</a><br>&nbsp;&nbsp;Complete Functionality for Adding a New Tournament', 'Simple Tourney', '0.1'), ('2010-02-17 12:57:55', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=2>2</a><br>&nbsp;&nbsp;Complete Functionality for Adding a New Tournament', 'Simple Tourney', '0.1'), ('2010-02-24 15:49:55', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=9>9</a><br>&nbsp;&nbsp;Allow selection of theme for each event', 'Simple Tourney', '0.2'), ('2010-02-24 16:00:58', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=7>7</a><br>&nbsp;&nbsp;Allow Latitude and Longitude for Venues', 'Simple Tourney', '0.1'), ('2010-02-24 16:01:29', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=8>8</a><br>&nbsp;&nbsp;Fix email headers for mail sending functions', 'Simple Tourney', '1.7'), ('2010-02-24 16:31:33', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=6>6</a><br>&nbsp;&nbsp;Venue Should Be Event Specific', 'Simple Tourney', '0.5'), ('2010-02-24 17:05:25', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=3>3</a><br>&nbsp;&nbsp;Allow ability to publish schedules.', 'Simple Tourney', '0.5'), ('2010-03-02 17:30:01', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=16>16</a><br>&nbsp;&nbsp;Cannot Insert Venue without Lat/Long.', 'Simple Tourney', '1'), ('2010-03-02 17:43:52', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=18>18</a><br>&nbsp;&nbsp;Add administrative fields to Venues table', 'Simple Tourney', '0.2'), ('2010-03-06 18:42:51', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=13>13</a><br>&nbsp;&nbsp;Allow Updating an Event (Tournament)', 'Simple Tourney', '3.7'), ('2010-03-07 15:23:02', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=12>12</a><br>&nbsp;&nbsp;Update Preferences', 'Simple Tourney', '1'), ('2010-03-07 15:54:18', 'coty@latenight-solutions.com', '', 'Completed Request <a href=newrequest.asp?id=15>15</a><br>&nbsp;&nbsp;Close Account Functionality', 'Simple Tourney', '0.7'), ('2010-04-02 19:25:06', 'jesse@qbstech.com', '', 'Completed Request <a href=newrequest.asp?id=20>20</a><br>&nbsp;&nbsp;Review Applications Report', 'Simple Tourney', '6');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
