/*
 Navicat Premium Data Transfer

 Source Server         : Bugdemon_dev
 Source Server Type    : MySQL
 Source Server Version : 50534
 Source Host           : db.bugdemon.com
 Source Database       : bugdemon_dev

 Target Server Type    : MySQL
 Target Server Version : 50534
 File Encoding         : utf-8

 Date: 02/24/2014 08:44:17 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `addcomments`
-- ----------------------------
DROP TABLE IF EXISTS `addcomments`;
CREATE TABLE `addcomments` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) DEFAULT NULL,
  `Comments` text,
  `DateAdded` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `changetypes`
-- ----------------------------
DROP TABLE IF EXISTS `changetypes`;
CREATE TABLE `changetypes` (
  `ChangeTypeDesc` varchar(20) NOT NULL,
  `LongTypeDesc` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ChangeTypeDesc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `changetypes`
-- ----------------------------
BEGIN;
INSERT INTO `changetypes` VALUES ('Bug', 'Problem with Product'), ('Documentation', 'Documentation'), ('Enhancement', 'Request for new Functionality'), ('Project', 'New Project Request');
COMMIT;

-- ----------------------------
--  Table structure for `chgnotify`
-- ----------------------------
DROP TABLE IF EXISTS `chgnotify`;
CREATE TABLE `chgnotify` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) NOT NULL,
  `DateAdded` datetime DEFAULT NULL,
  `DevNotify` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `fieldnames`
-- ----------------------------
DROP TABLE IF EXISTS `fieldnames`;
CREATE TABLE `fieldnames` (
  `FieldNo` int(11) NOT NULL,
  `FieldDesc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`FieldNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `fieldnames`
-- ----------------------------
BEGIN;
INSERT INTO `fieldnames` VALUES ('0', 'Status'), ('1', 'Change Type'), ('2', 'Product'), ('3', 'Version'), ('4', 'Summary'), ('5', 'Description'), ('6', 'Comments'), ('7', 'Assigned To'), ('8', 'Weight Factor'), ('9', 'Time'), ('10', 'Completed By'), ('11', 'Fix Release'), ('12', 'Internal Comments'), ('13', 'Affected Modules'), ('14', 'Admin Comments<br>(public)'), ('15', 'Due Date'), ('16', 'Priority');
COMMIT;

-- ----------------------------
--  Table structure for `files`
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `fileid` bigint(20) NOT NULL AUTO_INCREMENT,
  `objectid` bigint(20) NOT NULL,
  `authlevel` varchar(25) NOT NULL,
  `timestamp` datetime NOT NULL,
  `userid` bigint(20) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`fileid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `history`
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history` (
  `historyid` bigint(20) NOT NULL AUTO_INCREMENT,
  `objectid` bigint(20) NOT NULL,
  `userid` int(11) NOT NULL,
  `comment` text,
  `activitytype` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = View, 2=Update, 3=Update, 4=Comment',
  `timestamp` datetime NOT NULL,
  `ipaddress` varchar(25) NOT NULL,
  PRIMARY KEY (`historyid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `logins`
-- ----------------------------
DROP TABLE IF EXISTS `logins`;
CREATE TABLE `logins` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) DEFAULT NULL,
  `LoginDate` datetime DEFAULT NULL,
  `LogoutDate` datetime DEFAULT NULL,
  `UserAgent` text,
  `IP` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `UserID` (`UserID`),
  KEY `UserID_LoginDate` (`UserID`,`LoginDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `priorities`
-- ----------------------------
DROP TABLE IF EXISTS `priorities`;
CREATE TABLE `priorities` (
  `priorityid` tinyint(4) NOT NULL,
  `prioritydesc` varchar(25) NOT NULL,
  `notifythreshold1` int(11) NOT NULL,
  `notifythreshold2` int(11) NOT NULL,
  `notifythreshold3` int(11) NOT NULL,
  `notifyannoy` int(11) NOT NULL,
  `escalationpath` tinyint(4) NOT NULL,
  PRIMARY KEY (`priorityid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `priorities`
-- ----------------------------
BEGIN;
INSERT INTO `priorities` VALUES ('1', 'Normal', '1440', '2880', '4320', '60', '1'), ('2', 'Low', '4320', '5760', '10080', '1440', '0'), ('3', 'None', '0', '0', '0', '0', '99'), ('4', 'High', '720', '1440', '2880', '60', '2'), ('5', 'Urgent', '60', '360', '720', '30', '3'), ('6', 'Critical', '15', '30', '45', '15', '4');
COMMIT;

-- ----------------------------
--  Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `ProductName` varchar(50) DEFAULT NULL,
  `SortOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `products`
-- ----------------------------
BEGIN;
INSERT INTO `products` VALUES ('1', 'Bug Demon', '0');
COMMIT;

-- ----------------------------
--  Table structure for `rankings`
-- ----------------------------
DROP TABLE IF EXISTS `rankings`;
CREATE TABLE `rankings` (
  `ID` bigint(20) NOT NULL,
  `UserID` varchar(50) NOT NULL,
  `Rating` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `ratings`
-- ----------------------------
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings` (
  `Rating` tinyint(4) NOT NULL,
  `RatingDesc` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`Rating`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `statuses`
-- ----------------------------
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses` (
  `StatusID` tinyint(4) NOT NULL,
  `StatusDesc` varchar(15) DEFAULT NULL,
  `LongStatusDesc` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`StatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `statuses`
-- ----------------------------
BEGIN;
INSERT INTO `statuses` VALUES ('1', 'New', 'New Request'), ('2', 'Open', 'Request has been Reviewed and Accepted'), ('3', 'Hold', 'Request has been Reviewd and Accepted, but will not start'), ('5', 'More Info', 'More Information Required from the Submitting Party'), ('6', 'Assigned', 'Request Assigned to a Developer'), ('8', 'In Process', 'Request has been Started'), ('10', 'Duplicate', 'Similar Request Already Entered'), ('12', 'Complete', 'Request Completed'), ('14', 'Closed', 'Closed - See Comments');
COMMIT;

-- ----------------------------
--  Table structure for `timetracking`
-- ----------------------------
DROP TABLE IF EXISTS `timetracking`;
CREATE TABLE `timetracking` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) NOT NULL,
  `TaskID` bigint(20) NOT NULL,
  `BeginTime` datetime NOT NULL,
  `EndTime` datetime DEFAULT NULL,
  `TotalMins` decimal(10,3) DEFAULT NULL,
  `ManualEntry` tinyint(4) DEFAULT '0',
  `Reconciled` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `UserID` (`UserID`,`EndTime`,`TaskID`),
  KEY `TaskID` (`TaskID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `tobject`
-- ----------------------------
DROP TABLE IF EXISTS `tobject`;
CREATE TABLE `tobject` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Summary` varchar(256) DEFAULT NULL,
  `Description` text,
  `DateEntered` datetime DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  `ChangeType` varchar(20) DEFAULT NULL,
  `Product` varchar(50) DEFAULT NULL,
  `DateClosed` datetime DEFAULT NULL,
  `Comments` text,
  `Commented` varchar(75) DEFAULT NULL,
  `EnteredBy` varchar(50) DEFAULT NULL,
  `AssignedTo` varchar(50) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastUpdatedBy` varchar(50) DEFAULT NULL,
  `InternalComments` text,
  `WeightingFactor` tinyint(4) DEFAULT NULL,
  `ChangeDuration` float DEFAULT NULL,
  `CompletedBy` varchar(50) DEFAULT NULL,
  `Version` varchar(25) DEFAULT NULL,
  `AffFiles` varchar(1000) DEFAULT NULL,
  `FixRelease` varchar(25) DEFAULT NULL,
  `DueDate` datetime DEFAULT NULL,
  `ParentID` bigint(20) DEFAULT NULL,
  `Priority` int(11) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(50) DEFAULT NULL,
  `UserPass` varchar(30) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(75) DEFAULT NULL,
  `UserType` varchar(20) DEFAULT NULL,
  `DateAdded` datetime DEFAULT NULL,
  `HeartBeat` datetime DEFAULT NULL,
  `StartPage` varchar(30) NOT NULL DEFAULT 'trackmain.asp',
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', 'jesse@qbstech.com', 'abc123', 'Jesse', 'Quijano', 'Owner', '2004-05-17 00:00:00', '2014-02-20 17:56:00', 'trackmain.asp', '2009-08-26 16:43:34');
COMMIT;

-- ----------------------------
--  Table structure for `userstypes`
-- ----------------------------
DROP TABLE IF EXISTS `userstypes`;
CREATE TABLE `userstypes` (
  `UserType` varchar(20) NOT NULL,
  `LongDesc` varchar(250) DEFAULT NULL,
  `UserVal` int(11) DEFAULT NULL,
  PRIMARY KEY (`UserType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `userstypes`
-- ----------------------------
BEGIN;
INSERT INTO `userstypes` VALUES ('Approver', 'Approves New Requests', '5'), ('Developer', 'Responsible for Development Activities', '3'), ('Owner', 'Site Owner or Admin', '0'), ('Standard', 'Submit and View Details of Requests', '8');
COMMIT;

-- ----------------------------
--  Table structure for `workinfo`
-- ----------------------------
DROP TABLE IF EXISTS `workinfo`;
CREATE TABLE `workinfo` (
  `EnterDate` datetime DEFAULT NULL,
  `EnteredBy` varchar(50) DEFAULT NULL,
  `Version` varchar(25) DEFAULT NULL,
  `DevDesc` text,
  `Product` varchar(20) DEFAULT NULL,
  `TimeSpent` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
