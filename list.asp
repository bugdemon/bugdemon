<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<!-- #include file="inc_FieldDef.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2014, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
userid = Request.Cookies("UserID")
sortby = Request.Item("sortby")
ascdesc = Request.Item("ascdesc")
comp = Request.Cookies("ShowComp") 'Allow the completed items to be hidden
if comp = "" then
   comp = 0
end if
if comp = 1 then
   'Response.Write("Inside")
   showComp = false
end if

'if dbType = "MSSQL" then
'   sqlStr = "SELECT TOP " & maxShow & " ID,Summary,LastUpdated,EnteredBy,Status,FixRelease FROM tobject"
'else
'   sqlStr = "SELECT ID,Summary,LastUpdated,EnteredBy,Status,FixRelease FROM tobject"
'end if
term = Request.Item("term")

sqlStr = "select * from tobject where Summary like '%" & term & "%' OR Description like '%" & term & "%'"
'sqlStr = Request.Form("q")

if showComp <> true then
   sqlStr = sqlStr & " AND tobject.Status <> 'Duplicate' AND tobject.Status <> 'Complete' AND tobject.Status <> 'Closed'"
end if

if showNew = false then
   sqlStr = sqlStr & " AND tobject.Status <> 'New'" 
end if

if sortby <> "" then
   sqlStr = sqlStr & " ORDER BY " & sortby & " " & ascdesc
else
   sqlStr = sqlStr & " ORDER BY LastUpdated"
end if

'if dbType = "MySQL" then
'   sqlStr = sqlStr & " LIMIT 0," & maxShow
'end if

'if comp = "1" then
'   Response.Write(sqlStr)
'end if

set oConn = Server.CreateObject("ADODB.Connection")
set RS = Server.CreateObject("ADODB.Recordset")

oConn.Open strConnect

'set RS = oConn.Execute(sqlStr)
RS.Open sqlStr,oConn,1,1

bcolor = "white"

%>
<%if theme = "2003" then
    class_table = "rrow"
%>
    <!-- #include file="themes/2003/header.html" -->
<% elseif theme = "2013" then
    class_table = "table1"
%>
    <!-- #include file="themes/2013/header.html" -->
<% end if %>
<script language="JavaScript">
   function DoComp() {
      var now = new Date();
      var aYearLater = new Date(now.getTime() + 1000 * 60 * 60 * 24 * 365);
      var aDayAgo = new Date(now.getTime() + 1000 * 60 * 60 * 24 * -1);
      if (document.options.hidecomp.checked == true) {
         document.hiddenvals.comp.value = 1;
         document.cookie = "ShowComp=1;expires=" + aYearLater.toGMTString();
      } else {
         document.hiddenvals.comp.value = 0;
         document.cookie = "ShowComp=1;expires=" + aDayAgo.toGMTString();
      }
      document.hiddenvals.submit();
   }
</script>

<form name="options">
   <input type=checkbox name=hidecomp onclick="DoComp()" <%if comp = 1 then%>checked<%end if%>> Hide Completed Requests
</form>
<small><font color=blue>Click on the ID Number to view details of each item.</font></small><br>
<table border="1" width="100%" bordercolor="LightGrey" class="<%=class_table%>" cellspacing="0" cellpadding="5" width="900">
   <tr bgcolor=lightsteelblue><th align=left>ID <small><small>(<a href="list.asp?comp=<%=comp%>&sortby=ID&ascdesc=ASC&term=<%=term%>">^</a>|<a href="list.asp?comp=<%=comp%>&sortby=ID&ascdesc=DESC&term=<%=term%>">v</a>)</small></small></th><th align=left><small>Last Updated <small>(<a href="list.asp?comp=<%=comp%>&sortby=LastUpdated&ascdesc=ASC&term=<%=term%>">^</a>|<a href="list.asp?comp=<%=comp%>&sortby=LastUpdated&ascdesc=DESC&term=<%=term%>">v</a>)</small></small></th><th align=left><small><%=arryFields(0)%><small><small> (<a href="list.asp?comp=<%=comp%>&sortby=Status&ascdesc=ASC&term=<%=term%>">^</a>|<a href="list.asp?comp=<%=comp%>&sortby=Status&ascdesc=DESC&term=<%=term%>">v</a>)</small></small></small></th><th align=left><small><%=arryFields(4)%></small></th><th align=left><small>Opened By <small>(<a href="list.asp?comp=<%=comp%>&sortby=EnteredBy&ascdesc=ASC&term=<%=term%>">^</a>|<a href="list.asp?comp=<%=comp%>&sortby=EnteredBy&ascdesc=DESC&term=<%=term%>">v</a>)</small></small></th><th align=left><small><%=arryFields(11)%></small></th><th align=left><small>Actions</th></tr>
   <% if RS.EOF = true then %>
   <tr><td colspan=10>No records matched your query.</td></tr>
   <% else %>
      <%
         while RS.EOF <> true
            Response.Write("<tr bgcolor=" & bcolor & " class='text'><td><a href=newrequest.asp?id=" & RS("ID") & ">" & RS("ID") & "</td><td width=80><small><small>" & RS("LastUpdated") & "</small></td><td><small>" & RS("Status") & "</small></td><td width=300><small>" & RS("Summary") & "</small></td><td><small>" & RS("EnteredBy") & "</small></td><td>&nbsp;" & RS("FixRelease") & "</small></td><td><a href=addwatch.asp?id=" & RS("ID") & "><img src='/themes/" & theme & "/images/watch.png' title='Watch Request' alt=" & chr(34) & "Watch Request" & chr(34) & " border=0></a>&nbsp;<a href=deletewatch.asp?id=" & RS("ID") & "><img src='/themes/" & theme & "/images/watch_remove.png' border=0 height=23 width=23 alt=" & chr(34) & "Delete Watch" & chr(34) & " title='Delete Watch'></a></td></tr>")
            if bcolor = "white" then
               bcolor = "lightgoldenrodyellow"
            else
               bcolor = "white"
            end if
            RS.MoveNext
         wend
      %>
   <% end if %>
</table>
<form action="list.asp" method="POST" name="hiddenvals">
   <input type=hidden name=sortby value="<%=sortby%>">
   <input type=hidden name=ascdesc value="<%=ascdesc%>">
   <input type=hidden name=comp value="<%=comp%>">
   <input type="hidden" name="term" value="<%=term%>">
</form>
<%if theme = "2003" then %>
    <!-- #include file="themes/2003/footer.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/footer.html" -->
<% end if %>
