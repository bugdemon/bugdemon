<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2013, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

userid = Request.Item("userid")
if userid = "" then
   Response.Write("User ID not specified.")
   Response.End
end if

password = Request.Item("password")
if password = "" then
   Response.Write("Password not specified.")
   Response.End
end if

lastname = Request.Item("lastname")
if lastname = "" then
   Response.Write("Last Name not specified.")
   Response.End
end if

firstname = Request.Item("firstname")
if firstname = "" then
   Response.Write("First Name not specified.")
   Response.End
end if

if dbType = "MySQL" then
   timestamp = MySQLDate(now())
else
   timestamp = now()
end if

sqlStr = "INSERT INTO users (UserID, UserPass, FirstName, LastName, UserType,DateAdded, StartPage) VALUES ('" & userid & "','" & password & "','" & firstname & "','" & lastname & "','" & defUType & "'," & delim & timestamp & delim & ",'trackmain.asp')"
set oConn = Server.CreateObject("ADODB.Connection")
oConn.Open strConnect

oConn.Execute(sqlStr)
Response.Redirect("logon.asp") 'force them to login with their new credentials

%>