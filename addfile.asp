<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<!--#include file="include/clsUpload.asp"-->
<%
'********************************************************************************************
'*   Copyright 2003-2014, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

%>
<!-- #include file="inc_checklogin.asp" -->
<%
Response.Buffer = false

id = Request.Cookies("tobject")
if id = "" then
   Response.Write("You did not specify the ID number.")
   Response.End
end if

'upload file and catalog in database if necessary
if Request.ServerVariables("REQUEST_METHOD") = "POST" then

    Dim objUpload
    Dim strFileName
    Dim strPath

    ' Instantiate Upload Class
    Set objUpload = New clsUpload

    ' Grab the file name
    strFileName = objUpload.Fields("File1").FileName
    Response.write("<p>File Name: " & objUpload.Fields("File1").FileName)

    ' Compile path to save file to
    strPath = Server.MapPath(uploadPath) & "\" & id & "_" & strFileName
    
    ' Save the binary data to the file system
    objUpload("File1").SaveAs strPath

    ' Release upload object from memory
    Set objUpload = Nothing

    dateadded = ""
    if dbType = "MySQL" then
        dateadded = MySQLDate(now())
    else
        dateadded = now()
    end if
    lastupdatedby = Request.Cookies("UID")
    if lastupdatedby = "" then
        response.write("Your session expired during data entry. Sorry about that, couldn't be helped.  You should now log in again.")
        response.end
    end if

    set oConn = Server.CreateObject("ADODB.Connection")
    oConn.Open strConnect

    sqlStr = "INSERT INTO files(objectid, authlevel, timestamp, userid, filename) VALUES(" & id & ",'" & defUType & "'," & delim & dateadded & delim & "," & lastupdatedby & ",'" & id & "_" & replace(strFileName,"'","''") & "')"
    Response.Write("<br/>" & sqlStr)
    oConn.Execute(sqlStr)
    
    set RS = Server.CreateObject("ADODB.RecordSet")
    sqlStr = "SELECT * FROM tobject WHERE ID=" & id
    RS.Open sqlStr, oConn, 1, 1
    arryObject = RS.GetRows()
    RS.Close
    sqlStr = "SELECT * FROM chgnotify WHERE ID=" & id
    'set RS = oConn.Execute(sqlStr)
    RS.Open sqlStr,oConn,1,1
      
    while RS.EOF <> true
         call sendMail(RS("UserID"), "A file was added to an entry you wanted to watch at " & coName & " " & appName & ":<br /><strong>Title</strong>: " & arryObject(1,0) & "<br /><strong>File Name</strong>: " & strFileName & "<br /><strong>Current Status</strong>: " & arryObject(4,0) & "<br /><strong>Tentative Due Date</strong>: " & arryObject(21,0) & "<p>You can view the full request at: <a href=" & chr(34) & siteAddy & "/newrequest.asp?id=" & id & chr(34) & ">Click Here</a>", "Change Notification for " & id)
         RS.MoveNext
    wend
      
    RS.Close
    set RS = nothing
    
    call writeLog(id, 5,"Added")

    'Upload Complete, close the upload dialog
    Response.Write("<script type='text/javascript'>window.opener.document.location.reload();</script>")
    Response.Write("<script type='text/javascript'>window.close();</script>")
    
end if
%>
<html>
<head>
        <title>Upload File</title>
    <script type="text/javascript">
        function submitform() {
            if(document.addcomm.File1.value == '') {
                alert('You must select a file before uploading');
                return false;
            }
            document.addcomm.butt.value = 'Uploading...';
            document.addcomm.butt.disabled = true;
            document.addcomm.submit();
        }
    </script>
        <link rel="stylesheet" type="text/css" href="demon.css">
</head>
<body onunload="">
<div style="width:900px;">
<!-- #include file="inc_header.asp" -->

<div style="">
    <form action="addfile.asp" method="post" encType="multipart/form-data" name="addcomm">
    <table border=0 cellpadding="0" cellspacing="0" width="500">
        <tr><td colspan="2"><hr style="color:lightsteelblue;"></td></tr>
        <tr><td><b><%=reqtitle%></b></td></tr>
        <tr><td colspan=2>&nbsp;</td></tr>
        <tr><td><input type="file" name="File1"></td></tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr><td align="right"><input type="button" onclick="submitform();" value="Add File" class="butt" name="butt"></td></tr>
        <tr><td colspan="2"><hr style="color:lightsteelblue;"></td></tr>
        <tr><td><%=comments%></td></tr>
    </table>
    </form>
</div>
</div>
</body>
</html>