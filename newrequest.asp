<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<!-- #include file="inc_FieldDef.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2014, Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

id = Request.QueryString("id")
action = Request.QueryString("action")
if action = "" then
   action = 0
end if
%>
<!-- #include file="inc_checklogin.asp" -->
<%
auth = Request.Cookies("Auth")

set oConn = Server.CreateObject("ADODB.Connection")
oConn.Open strConnect
priority = 1

if id <> "" then 'load existing values
    Response.Cookies("tobject") = id

    sqlStr = "SELECT * FROM tobject WHERE ID=" & id
    set RS = Server.CreateObject("ADODB.Recordset")
   
    RS.Open sqlStr,oConn,1,1
    'set RS = oConn.Execute(sqlStr)
   
   summary = RS("Summary")
   description = RS("Description")
   dateentered = RS("DateEntered")
   status = RS("Status")
   changetype = RS("ChangeType")
   product = RS("Product")
   dateclosed = RS("DateClosed")
   comments = RS("Comments")
   commented = RS("Commented")
   enteredby = RS("EnteredBy")
   assignedto = RS("AssignedTo")
   lastupdated = RS("LastUpdated")
   lastupdatedby = RS("LastUpdatedBy")
   internalcomments = RS("InternalComments")
   weightingfactor = RS("WeightingFactor")
   changeduration = RS("ChangeDuration")
   completedby = RS("CompletedBy")
   version = RS("Version")
   files = RS("AffFiles")
   fixrelease = RS("FixRelease")
   duedate = RS("DueDate")
   priority = cint(RS("Priority"))
   
   RS.Close
   
   sqlStr = "SELECT * FROM chgnotify WHERE UserID='" & userid & "' AND ID=" & id
   RS.Open sqlStr,oConn,1,1
   
   if RS.EOF = true then
      watch = false
   else
      watch = true
   end if
   RS.Close
      
   'get time tracking information if logged in user
   if Request.Cookies("UID") <> "" then
      sqlStr = "SELECT * FROM timetracking WHERE UserID=" & Request.Cookies("UID") & " AND TaskID=" & id & " AND EndTime IS NULL"
   
      RS.Open sqlStr,oConn,1,1
   
      opentime = false
      if not RS.EOF then
         opentime = true
         begintime = RS("BeginTime")
      end if
   
      RS.Close
      'get the total time for this task (all users)
      sqlStr = "SELECT SUM(TotalMins) AS TheTotal FROM timetracking WHERE TaskID=" & id
      RS.Open sqlStr,oConn,1,1
   
      tottime = RS("TheTotal")
   end if
   'log the view
   call writeLog(id, 1,"Viewed")
end if

'get priorities
sqlStr = "SELECT * FROM priorities ORDER BY escalationpath"
set RS = Server.CreateObject("ADODB.RecordSet")
RS.Open sqlStr,oConn,1,1
arryPriority = RS.GetRows()
RS.Close

optionsPriority = ""
for i=0 to ubound(arryPriority,2)
    optionsPriority = optionsPriority & "<option value=" & chr(34) & arryPriority(0,i) & chr(34) & " "
    if priority = cint(arryPriority(0,i)) then
        optionsPriority = optionsPriority & "selected"
    end if
    optionsPriority = optionsPriority & ">" & arryPriority(1,i) & "</option>" & chr(10) & chr(13)
next

pagetitle = "Task Detail"
%>

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/header.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/header.html" -->
<% end if %>

<% if watch = true then %>
   &nbsp;You are <b>Watching</b> this item.  <a href="deletewatch.asp?id=<%=id%>&pn=newrequest.asp"><img src="/themes/<%=theme%>/images/watch_remove.png" align="absmiddle" border="0" alt="Delete Watch" title="Delete Watch" height="30"></a>
<% else %>
   <% if id <> "" then %>
      &nbsp;You are <b>Not Watching</b> this item. <a href="addwatch.asp?id=<%=id%>&pn=newrequest.asp"><img src="/themes/<%=theme%>/images/watch.png" align="absmiddle" border="0" alt="Watch Item" title="Watch Item" height="30"></a>
   <% end if %>
<% end if %>
&nbsp; <% if action=3 then Response.Write("&nbsp;&nbsp;<font color=blue><strong>Item Saved<strong></font>") end if %>
<br /><br />

<script type="text/javascript">
function Cancel() {
  this.location = document.req.start.value;
}
function Search() {
    if(document.search.term.value == '') {
        alert('You did not enter any search criteria.');
        return false;
    }
    document.search.submit();
}
function submitform() {
   if(document.req.duedate.value!= '' && !isDate(document.req.duedate.value)) {
       alert('Invalid <%=arryFields(15)%> Specified.');
       return false;
   }
  if (document.req.description.value == "") {
     alert("You must specify a value for <%=arryFields(5)%>.");
     return 1;
  }
  if (document.req.summary.value == "") {
     alert("You must specify a value for <%=arryFields(4)%>.");
     return 1;
  }
  var sSum = document.req.summary.value;
  if (sSum.length > 250) {
     alert("The Summary is too long.  <%=arryFields(4)%> must be less than 250 characters.");
     return 1;
  }
  
  if (document.req.auth.value == 'Owner' || document.req.auth.value == 'Developer') {
     /* These are the Admin checks. */
     if (document.req.status.value == 'Closed' && document.req.admincomments.value == '') {
        alert("When <%=arryFields(0)%> is \'Closed\' then <%=arryFields(14)%> may not be blank.");
        return 1;
     }
     if (document.req.status.value == 'Complete' && (document.req.changeduration.value == '0' || document.req.completedby.value == '')) {
        alert("When <%=arryFields(0)%> is \'Complete\' then <%=arryFields(9)%> and <%=arryFields(10)%> must be filled out.");
        return 1;
     }
  }
  document.req.status.disabled = false;
  document.req.submit();
}
function isDate(val) {
   var d = new Date(val);
   return !isNaN(d.valueOf());
}
function AddComments() {
  if (document.req.auth.value == 'Owner' || document.req.auth.value == 'Developer') {
     if (confirm("Do you wish to save first?  If you don\'t, any changes will be lost.")) {
        document.req.saveonly.value == '1';
        document.req.submit();
     }
  }// Else a Standard User, no save necessary.
  newwin = window.open('addcomment.asp?id=' + document.req.id.value, 'addcomment', 'width=650,height=425,toolbar=no,scrollbars,menubar=0');   
  newwin.focus();
}
function Copy() {
  document.req.completedby.value = document.req.assignedto.value;
}
function Info() {
  var link = 'external.asp';
  var site = document.req.product.value;
  newwin = window.open(link + '?site=' + site,'lookup','width=300,height=200,toolbar=no,scrollbars=0,menubar=0');
  newwin.focus();
}
function FullScreen() {
  newwin = window.open('showfull.asp?id=<%=id%>','FullScreen','width=600,height=400,toolbar=no,scrollbars,menubar=1');
  newwin.focus();
}
function Upload() {
   newwin = window.open('addfile.asp?id=' + document.req.id.value, 'addfile', 'width=650,height=225,toolbar=no,scrollbars=no,menubar=0');   
   newwin.focus();
}

/* pop action result */
switch(<%=action%>) {
  case 0:
     break;
  case 1:
     alert('StopWatch Started');
     break;
  case 2:
     alert('StopWatch Stopped');
     break;
  case 3:
     //alert('Item Saved');
     break;
  default:
     break;
}
</script>
<center>
<form action="saverequest.asp" method="POST" name="req">
<table border="0" cellspacing="0" cellpadding="0" width="800" class="task_user">
   <tr><th class="task_user_ul_round">ID</th><td class="task_user">&nbsp;<font color=blue><b><%=id%></b></font></td><th class="task_user"><%=arryFields(0)%></td><td class="task_user_ur_round"><select name="status" size="1" <%if auth = "Standard" then Response.Write "disabled" end if%>>
      <%
         sqlStr = "SELECT * FROM statuses ORDER BY StatusID"
         set RS = oConn.Execute(sqlStr)
         while RS.EOF <> true
            Response.Write("<option value=" & chr(34) & RS("StatusDesc") & chr(34))
            if RS("StatusDesc") = status then
               Response.Write(" selected")
            end if
            Response.Write(">" & RS("StatusDesc") & "</option>")
            RS.MoveNext
         wend
      %>
      </select></td></tr>
   <tr><th class="task_user"><%=arryFields(1)%></th><td class="task_user"><select name="chgtype" size="1">
      <%
         sqlStr = "SELECT * FROM changetypes ORDER BY ChangeTypeDesc"
         set RS = oConn.Execute(sqlStr)
         while RS.EOF <> true
            Response.Write("<option value=" & chr(34) & RS("ChangeTypeDesc") & chr(34))
            if RS("ChangeTypeDesc") = changetype then
               Response.Write(" selected")
            end if
            Response.Write(">" & RS("ChangeTypeDesc") & "</option>")
            RS.MoveNext
         wend
      %>
      </select></td><th class="task_user"><%=arryFields(2)%></th><td class="task_user"><select name="product" size="1">
      <%
         sqlStr = "SELECT * FROM products ORDER BY SortOrder"
         set RS = oConn.Execute(sqlStr)
         while RS.EOF <> true
            Response.Write("<option value=" & chr(34) & RS("ProductName") & chr(34))
            if RS("ProductName") = product then
               Response.Write(" selected")
            end if
            if id = "" and RS("ProductID") = 0 then 'make product 0 the default on new items
               Response.Write(" selected")
            end if
            Response.Write(">" & RS("ProductName") & "</option>")
            RS.MoveNext
         wend
         if id <> "" then
            sqlStr = "SELECT DateAdded,UserID,Comments as C FROM addcomments WHERE ID=" & Request.QueryString("id") & " ORDER BY DateAdded DESC"
            'sqlStr = "SELECT * FROM AddComments WHERE ID=" & id & " ORDER BY DateAdded DESC"
            set RS = oConn.Execute(sqlStr)
         end if
      %>
      </select>&nbsp;
      <%
      if externalLookup = true then
         'If you have an external information database for your products, you can enable this
         Response.Write("<a href=" & chr(34) & "javascript:Info();" & chr(34) & ">")
         Response.Write("<img src=" & chr(34) & "/themes/" & theme & "/images/information.png" & chr(34) & " border=" & chr(34) & "0" & chr(34) & ">")
         Response.Write("</a>")
      end if
      %>
      </td></tr>
   <tr><th class="task_user"><%=arryFields(16)%></th><td class="task_user"><select name="priority"><%=optionsPriority%></select></td><th class="task_user"><%=arryFields(15)%></th><td class="task_user"><input type="text" name="duedate" value="<%=Response.Write(duedate)%>" <%if auth = "Standard" then Response.Write("readonly") end if%>"></td></tr>
   <tr><th class="task_user"><%=arryFields(3)%></th><td class="task_user"><input type="text" size="20" name="version" value="<%=version%>"></td><th class="task_user">&nbsp;</th><td class="task_user">&nbsp;</td></tr>
   <tr><th class="task_user"><%=arryFields(4)%></th><td class="task_user" colspan="10"><textarea cols="90" rows="2" name="summary"><%=summary%></textarea></td></tr>
   <tr><th class="task_user"><%=arryFields(5)%>&nbsp;<% if id <> "" then%><a href="javascript:FullScreen();"><img src="/themes/<%=theme%>/images/blog.gif" border="0" alt="Full Screen" title="Full Screen"></a><%end if%></th><td class="task_user" colspan="10"><textarea cols="90" rows="6" name="description"><%=description%></textarea></td></tr>
   <tr><th class="task_user" colspan=10 style="text-align:center;border-bottom-style:solid;border-bottom-color:DarkGrey;border-bottom-width:1px;"><input type="checkbox" name="notify" value="ON" checked <%if id <> "" then%>disabled<%end if%>> Notify me of changes related to this Request.</th></tr>
   <% if id <> "" then 
   'Prep file list
   sqlStr = "SELECT * FROM files WHERE objectid=" & id & " AND active=1"
   filelist = ""
   set oRS = oConn.Execute(sqlStr)
   while oRS.EOF <> true 
       filelist = filelist & "<li class='upload'><a href='" & siteAddy & "/" & uploadPath & "/" & oRS("filename") & "'>" & oRS("filename") & "</a></li>"
       oRS.MoveNext
   wend
   set oRS = nothing
   %>
   <tr><th class="task_user">Date Entered</th><td class="task_user"><%=dateentered%></td><th class="task_user">Entered By</td><td class="task_user"><%=enteredby%></td></tr>
   <tr><th class="task_user">Last Updated</th><td class="task_user"><%=lastupdated%></td><th class="task_user">Last Updated By</td><td class="task_user"><%=lastupdatedby%></td></tr>
   <tr><th class="task_user">Date Closed</th><td class="task_user">&nbsp;<span style="color:red;"><%=dateclosed%></span></td><th class="task_user">&nbsp;</th><td class="task_user">&nbsp;</td></tr>
   <tr><th class="task_user" valign="top">Files</th>
       <td class="task_user" colspan="3">
           <table border="0" cellpadding="0" cellspacing="0">
                <tr><td style="width:670px;"><%
                                                if filelist <> "" then                            
                                                    Response.Write("<ul>" & filelist & "</ul>")
                                                end if

                                             %>
                    </td>
                    <td style="width:30px;"><%if (id <> "") and ((auth = "Owner" or auth = "Developer") or (enteredby = userid)) then %><input type="button" value="+" class="butt" onclick="Upload();" /><%else Response.Write("&nbsp;") end if %></td>
                </tr>
           </table>
       </td></tr>
   <tr><th class="task_user"><%=arryFields(6)%></th><td colspan="10" class="task_user"><%if comments <> "" then %><hr style="color:LightSteelBlue"><%=commented%><%=comments%><hr style="color:LightSteelBlue"><br><% end if %>
      <%
      while RS.EOF <> true
         Response.Write("<span style='font-size:10pt;'><font color='LightSteelBlue'>" & RS("DateAdded") & " - " & RS("UserID") & "</font><br>" & RS("C") & "</span><p>") 
         RS.MoveNext
      wend
      %>
      </td></tr>
   <tr><th class="task_user" colspan=10 style="text-align:center;border-bottom-style:solid;border-bottom-color:DarkGrey;border-bottom-width:1px;"><a href="javascript:AddComments();">Add New Public Comments</a></td></tr>
   <input type="hidden" name="id" value="<%=id%>">
   <% end if %>
</table>

<p><p>

<% if (auth = "Owner" or auth = "Developer") and id <> "" then %>
<table cellspacing="0" cellpadding="5" width="800" class="task_admin">
   <tr><td colspan="10" align="center" class="task_admin_head">Administrative Information (<a href="newrequest.asp?parentid=<%=id%>">Add Child Task</a>)</td></tr>
   <tr><th class="task_admin"><%=arryFields(14)%></th><td colspan=10 class="task_admin"><textarea cols="90" rows="3" name="admincomments" ><%=comments%></textarea></td></tr>
   <tr><th class="task_admin"><%=arryFields(7)%></th><td class="task_admin">&nbsp;<select name="assignedto" size="1"><option value = "">
      <%
         sqlStr = "SELECT UserID FROM users WHERE UserType='Owner' OR UserType='Developer'"
         set RS = oConn.Execute(sqlStr)
         while RS.EOF <> true
            Response.Write("<option value=" & chr(34) & RS("UserID") & chr(34))
            if RS("UserID") = assignedto then
               Response.Write(" selected")
            end if
            Response.Write(">" & RS("UserID") & "</option>")
            RS.MoveNext
         wend
      %>
      </select></td><th class="task_admin"><%=arryFields(8)%></th><td class="task_admin"><input type="text" name="weightfactor" size="30" value="<%=weightingfactor%>"></td></tr>
   <tr><td colspan="10" align="center" class="task_admin_head">Work Information</td></tr>
   <tr><th class="task_admin"><%=arryFields(9)%></th>
       <td class="task_admin"><input type="text" name="changeduration" size="5" value="<%=changeduration%>">&nbsp;(hours)&nbsp;&nbsp;<span style="font-size:9px;color:blue;">StopWatch: <%if tottime <> "" then Response.write(round((clng(tottime)/60),3)) else Response.write("0") end if%></span></td>
       <th class="task_admin">StopWatch</th>
       <td align="center" class="task_admin">
          <%
          if opentime then
             Response.Write("<img border='0' title='Timer Running' src=" & chr(34) & "/themes/" & theme & "/images/running.png" & chr(34) & ">&nbsp; <a href=" & chr(34) & "timestop.asp?id=" & id & chr(34) & "><img border='0' title='Stop Timer' src=" & chr(34) & "/themes/" & theme & "/images/stop.png" & chr(34) & "></a>&nbsp; <a href=" & chr(34) & "timeadd.asp?id=" & id & chr(34) & "><img border='0' title='Manage Time' alt=" & chr(34) & "Manual Time Entry" & chr(34) & " src=" & chr(34) & "/themes/" & theme & "/images/manualtimes.png" & chr(34) & "></a>&nbsp; <a href=" & chr(34) & "timeview.asp?id=" & id & chr(34) & "><img border='0' title='View Time' alt=" & chr(34) & "View Your Time Entries" & chr(34) & " src=" & chr(34) & "/themes/" & theme & "/images/viewtimes.png" & chr(34) & "></a>")
          else
                Response.Write("<a href=" & chr(34) & "timestart.asp?id=" & id & chr(34) & "><img border='0' title='Start Timer' alt=" & chr(34) & "Start Time" & chr(34) & " src=" & chr(34) & "/themes/" & theme & "/images/start.png" & chr(34) & ">&nbsp; <a href=" & chr(34) & "timeadd.asp?id=" & id & chr(34) & "><img border='0' title='Manual Time Entry' alt=" & chr(34) & "Manual Time Entry" & chr(34) & " src=" & chr(34) & "/themes/" & theme & "/images/manualtimes.png" & chr(34) & "></a>&nbsp; <a href=" & chr(34) & "timeview.asp?id=" & id & chr(34) & "><img border='0' title='View Time Entries' alt=" & chr(34) & "View Your Time Entries" & chr(34) & " src=" & chr(34) & "/themes/" & theme & "/images/viewtimes.png" & chr(34) & "></a>")
          end if             

          %>
       </td>
   </tr>
   <tr><th class="task_admin"><%=arryFields(11)%></th><td class="task_admin"><input type=text name="fixrelease" size="30" value="<%=fixrelease%>"></td><th class="task_admin"><%=arryFields(10)%></th><td align="center" class="task_admin"><input type=text name="completedby" value="<%=completedby%>" size="30"><br><span style="font-size:10px;"><a href="javascript:Copy();">( Copy Assigned To )</a></span></td></tr>
   <tr><td colspan=10 class="task_admin"><%=internalcomments%></td></tr>
   <tr><th class="task_admin"><%=arryFields(12)%></th><td colspan=10 class="task_admin"><textarea cols="90" rows="3" name="intcomments"></textarea></td></tr>
   <tr><th class="task_admin"><%=arryFields(13)%></th><td colspan=10 class="task_admin"><textarea cols="90" rows="2" name="files"><%=files%></textarea></td></tr>
   <tr><td colspan="10" align="center" class="task_admin" style="border-bottom-style:solid;border-bottom-color:black;border-bottom-width:1px;"><input type="checkbox"" name="sendnotifications" value="ON" checked> Send Notifications</td></tr>
</table>

<% end if %>
<% if (id = "") or (auth = "Owner" or auth = "Developer") or (enteredby = userid) then %>
<table border="0" cellpadding="0" cellspacing="0" width="900">
   <tr><td style="width:50px;text-align:left;padding-left:50px;">
   		   <% if id <> "" and allowStatusReq = true then %>
   		      <input type="button" value="Request Update" class="butt">
   		   <% end if %>
   	   </td>
   	   <td style="width:900px;text-align:right;padding-right:50px;"><input type="button" onclick="submitform();" class="butt" value="Save Request">&nbsp;&nbsp;&nbsp;<input type="button" onclick="Cancel();" class="butt" value="Cancel"></td></tr>
</table>
    <% if request.item("parentid") <> "" then%>
        <input type="hidden" name="parentid" value="<%=request.item("parentid")%>">
    <% end if %>
<% end if %>
<input type="hidden" name="auth" value="<%=auth%>">
<input type="hidden" name="saveonly" value="0">
<input type="hidden" name="owner" value="<%if enteredby = userid then Response.Write("1") end if%>">
<input type="hidden" name="start" value="<%=startpage%>">
</form>

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/footer.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/footer.html" -->
<% end if %>