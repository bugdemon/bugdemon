<%
'/************************************************************************************************************************************/
'/*  Usable variables: [ID], [OLD_STATUS], [NEW_STATUS], [USER_NAME], [COMMENTS], [DUPLICATE_ID], [TITLE], [DUPLICATE_TITLE], [DUE_DATE]
'/*                     [LOGIN_NAME]



%>

<%
function doReplacements(id, title, old_status, new_status, due_date, user_name, login_name, comments, dup_id, dup_title)
    msg = ""
    select case msgid
        case 1 'signup
            msg = "[USER_NAME],<br />Your account has been created"
        case 2 'change
        
        case 3 'more info request
        
        case 4 'new
        
        case 5 'duplicate
            msg = "[USER_NAME],<br />The work item you entered at " & coName & " has been flagged as a duplicate task and closed. You may continue to follow developments by "
        case else 'anything else
            msg = "[USER_NAME],<br />There has been a status change on an entry you wanted to watch ([TITLE]) at " & coName & " " & appName & ".<br>You can view the request at: <a href=" & chr(34) & siteAddy & "/newrequest.asp?id=" & id & chr(34) & ">Click Here</a>"
    end select

    msg = replace(msg, "[USER_NAME]", user_name)
    msg = replace(msg, "[ID]", id)
    msg = replace(msg, "[TITLE]", title)
    msg = replace(msg, "[OLD_STATUS]", old_status)
    msg = replace(msg, "[NEW_STATUS]", new_status)
    msg = replace(msg, "[DUE_DATE]", due_date)
    msg = replace(msg, "[LOGIN_NAME]", login_name)
    msg = replace(msg, "[COMMENTS]", comments)
    msg = replace(msg, "[DUPLICATE_ID]", dup_id)
    msg = replace(msg, "[DUPLICATE_TITLE", dup_title)

end function
%>