<%@ Language=VBScript %>
<!-- #include file="include.asp" -->
<%
'********************************************************************************************
'*   Copyright 2003-2014 - Quijano Business Services ("QBS"), All rights reserved.
'*   This is not shareware or freeware. There is no "demo" period for this software. 
'*   If you did not license this software you may not use it.
'*   If properly licensed, you may modify this software, however you may not distribute
'*   your modifications without purchasing one license per copy distributed from QBS.
'*   --------------   http://www.qbstech.com   -------------------------------------
'********************************************************************************************
Response.ExpiresAbsolute = #1/1/1980#
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache, private, must-revalidate"

%>
<!-- #include file="inc_checklogin.asp" -->
<%
comp = Request.Cookies("ShowWatchComp") 'Allow the completed items to be hidden
sqlStr = "SELECT * FROM chgnotify AS N LEFT JOIN tobject AS O ON N.ID=O.ID WHERE N.UserID='" & userid & "'"
if comp = "1" then
   sqlStr = sqlStr & " AND (O.Status <> 'Duplicate' AND O.Status <> 'Complete' AND O.Status <> 'Closed')"
end if
sqlStr = sqlStr & " ORDER BY O.Status, O.ID"

set oConn = server.CreateObject("ADODB.Connection")
set oRS = server.CreateObject("ADODB.Recordset")

oConn.Open strConnect

'Response.Write(sqlStr)

set oRS = oConn.Execute(sqlStr)
pagetitle = "My Watches"
%>

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/header.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/header.html" -->
<% end if %>

<script type="text/javascript">
   function DoComp() {
       var now = new Date();
       var aYearLater = new Date(now.getTime() + 1000 * 60 * 60 * 24 * 365);
       var aDayAgo = new Date(now.getTime() + 1000 * 60 * 60 * 24 * -1);
       if (document.options.hidecomp.checked == true) {
           document.getElementById('comp').value = '1';
           document.cookie = "ShowWatchComp=1;expires=" + aYearLater.toGMTString();
       } else {
           document.getElementById('comp').value = '';
           document.cookie = "ShowWatchComp=1;expires=" + aDayAgo.toGMTString();
       }
       this.location = 'watches.asp';
   }
   function Search() {
       if(document.search.term.value == '') {
           alert('You did not enter any search criteria.');
           return;
       }
       document.search.submit();
   }
</script>
<center>
<form name="options">
   <tr>
        <td style="padding-top:10px;font-size:12px;"><input type=checkbox name=hidecomp onclick="DoComp()" <%if comp = "1" then%>checked<%end if%>> Hide Completed Requests </td>
   </tr>
</form>
<table border="1" cellpadding="0" cellspacing="0" bordercolor="silver">
   <tr><th align=left>&nbsp;&nbsp;ID&nbsp;&nbsp;</th><th align=left>&nbsp;Status&nbsp;</th><th align=left>&nbsp;Summary</th><th align=left>&nbsp;&nbsp;Action&nbsp;&nbsp;</th></tr>
   <% if oRS.EOF = true then %>
      <tr><td colspan=5>&nbsp;You are not watching any change requests, or they are all complete.&nbsp;</td></tr>
   <% end if %>
   <% while oRS.EOF <> true %>
      <tr><td align="center"><a href="newrequest.asp?id=<%=oRS("ID")%>"><%=oRS("ID")%></a></td><td>&nbsp;&nbsp;<%=oRS("Status")%>&nbsp;&nbsp;</td><td>&nbsp;<%=oRS("Summary")%>&nbsp;</td><td align="center"><a href="deletewatch.asp?id=<%=oRS("ID")%>&pn=watches.asp"><img src='/themes/<%=theme%>/images/trash.gif' border="0" height="23" width="23" alt="Delete Watch" title="Delete Watch"></a></td></tr>
      <%=oRS.MoveNext%>
   <% wend %>
</table>
<input type="hidden" name="comp" id="comp" value="<%=comp%>">
</center>
<p>

<%if theme = "2003" then %>
    <!-- #include file="themes/2003/footer.html" -->
<% elseif theme = "2013" then %>
    <!-- #include file="themes/2013/footer.html" -->
<% end if %>

<% oRS.Close
   set oRS = nothing
   oConn.Close
   set oConn = nothing
%>
