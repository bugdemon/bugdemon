<%
'**************** Configuration Variables **********************
dbType      = "MSSQL"			'MSSQL, Access,MySQL
dbServer    = "localhost        'domain or IP address of your database
dbPort      = "1433"			'port your database listens on.  Standards:  3306 (MySQL), 1433 (MSSQL)
dbName      = "bugdemon"		'name of your database
dbUser      = "bugdemon_login"	'login id for your database
dbPass      = "BugZy2"   		'password for your database
coName      = ""				'Define your Company or Project Name
appName     = "BugDemon"	    'Define the name of the application - normally BugDemon
siteAddy    = "http://site.bugdemon.com"	'Specify the address to the root of this application ("home" destination)
defUType    = "Standard"		'The default UserType - must match definition in DB
uploadPath  = "Uploads"			'Path to where you want file uploads placed.
persistDays = 1				    'The number of days a User's login should last if they select 'remember me'
showNew     = true				'Show NEWly entered items in the main list immediately.
showComp    = true				'Show completed (or better) requests in main list
maxShow     = 20				'The maximum number of records to show on the main list.
openSystem  = true			    'true = anyone can create an account.
theme       = "2003"            '2013 or 2003
externalLookup = true		    'are you using an external information source?  If true, edit external.asp
enableMail  = true			    'Should e-mail be sent where applicable?
mailType    = 1				    '0=CDONTS 1=Persists MailSender 2=
fromAddy    = "bugdemon@qbstech.com"	'Return e-mail address
adminNotify = "jesse@qbstech.com" 'E-mail address(es) to notify of new request. Separate addys by ;
mailHost    = "outbound.mailhop.org"			'The name of the outgoing mail server
mailLogin   = "cdli"           'email login ID
mailPassword = "cdliadmin1"    'email password
modText = ""
'The above will be displayed to all users greater than standard on the main task overview.

'******** No changes past this point ****************************
if dbType = "" then
   Response.Write("You must specify a valid database type")
   Response.End
end if

if dbType <> "MySQL" and dbType <> "MSSQL" and dbType <> "Access" then
   Response.Write("You must specify a valid database type")
   Response.End
end if

if dbType = "MySQL" then
   strConnect = "Driver={MySQL ODBC 3.51 Driver};Server=" & dbServer & ";Port=" & dbPort & ";Option=131072;Stmt=;Database=" & dbName & ";Uid=" & dbUser & ";Pwd=" & dbPass &";"
   delim = "'"
end if

if dbType = "MSSQL" then
   'strConnect = "Driver={SQL Server}; Server=" & dbServer & "," & dbPort & "; UID=" & dbUser & "; PWD=" & dbPass & "; Database=" & dbName & ";"
   strConnect = "Provider=SQLOLEDB; Data Source=" & dbServer & "; Initial Catalog=" & dbName & "; User Id=" & dbUser & "; Password=" & dbPass & ";"
   delim = "'"
end if

function MySQLDate(thedate)
   mins = datepart("n",thedate)
   if len(mins) = 1 then
      mins = "0" & mins
   end if
   secs = datepart("s",thedate)
   if len(secs) = 1 then
      secs = "0" & secs
   end if
   MySQLDate = datepart("yyyy",thedate) & "-" & datepart("m",thedate) & "-" & datepart("d",thedate) & " " & datepart("h",thedate) & ":" & mins & ":" & secs
end function

sub sendMail(toAddy, mBody, mSubject)
   
   if enableMail <> true then
      exit sub
   end if
   
   select case (mailType)
      case 0
         Set oMail = Server.CreateObject("CDONTS.NewMail")
         oMail.Value("Keywords") = coName
         oMail.From = fromAddy
         oMail.To = toAddy
         oMail.Subject = mSubject
   
         oMail.Importance = 1
         oMail.BodyFormat = 0 '0 HTML, 1 Text
         oMail.MailFormat = 0 '0 MIME, 1 Text
   
         oMail.Body = mBody
         oMail.Send
         
         set oMail = nothing
         
      case 1
         Set oMail = Server.CreateObject("Persits.MailSender")
         
         oMail.From = fromAddy 
         'Mail.FromName = "" ' Specify sender's text name 
         oMail.AddAddress toAddy   
         oMail.IsHTML = true
         oMail.Subject = mSubject
         oMail.Body = mBody
         
         oMail.Host = mailHost
         oMail.Username = mailLogin
         oMail.Password = mailPassword
         
         oMail.Send
         
         set oMail = nothing
   end select
end sub
sub writeLog(objid, action,comment)
    set cnn = Server.CreateObject("ADODB.Connection")
    cnn.Open strConnect
    timestamp = now()
    if dbType = "MySQL" then
        timestamp = MySQLDate(timestamp)
    end if
    sql = "INSERT INTO history(objectid,userid,comment,activitytype,timestamp,ipaddress)VALUES("
    sql = sql & objid & ",'" & Request.Cookies("UID") & "','" & comment & "'," & action & "," & delim & timestamp & delim & ",'" & Request.ServerVariables("REMOTE_ADDR") & "')"
    cnn.Execute(sql)
    cnn.Close
end sub

%>